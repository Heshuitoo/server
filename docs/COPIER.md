## Copier
Copy every fields of database struct into response struct is bored and need time.
So, there is a library to copy the struct values to another struct.

## How to use
- import `gitlab.com/kirafan/sparkle/server/util`
- use `util.Copy()` to copy struct values
  - Example:
    - `util.Copy(&dst, &src)`
    - `util.Copy(&ResponseModel, &DatabaseModel)`

### Notes
- It has chance to make problem when passing pointer. Be careful at uses.
- It can not resolve array model like user_adv_ids_model.go, so you need to parse as number array and set manually.
  - Manual set and util.Copy() can be used together.

## References
- https://github.com/jinzhu/copier
- https://go.dev/play/p/AmMybE0mL-c