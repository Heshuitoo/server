## Air

The development server needs to rebuild every time when test a things.
But it is lazy to re-run build every time by hand. So we have [air](https://github.com/cosmtrek/air)'s hot reload.

## How to use

- Make a terminal at the root of this project
- Run `go install github.com/cosmtrek/air@latest` (If you haven't ever installed)
- Run `air`
- Now you can access to development server with hot-reload

## Notes
- It doesn't work well with windows docker container mount. (not verified)
  - Windows users can use VSCode devcontainer instead.

## References
- [Go言語のホットリロードツール『Air』でコードの修正を即時反映させる](https://nishinatoshiharu.com/install-go-air/)
- [DockerコンテナでgolangをホットリロードするAirを導入](https://zenn.dev/ajapa/articles/bc399c7e4c0def)
