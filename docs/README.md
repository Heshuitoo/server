# Docs

This folder includes development tool infos and guides.

<br>

## Guides

### How to install golang / Example projects of DDD

[Link](REFERENCES.md)

### Basic coding rules at this project

[Link](CODING_RULES.md)

### Priority labels of issues at this project

[Link](ISSUE_PRIORITY.md)

<br>
  
## Tools

### Git (Version control command)

[Link](GIT.md)

### Testing (Check the code behavior without running the server)

[Link](TESTING.md)

### Air (Rebuild development server at code change)

[Link](AIR.md)

### Copier (Copy the struct values to another struct)

[Link](COPIER.md)

### Boilerplate (Create DDD dependency templates in a time)

[Link](BOILERPLATE.md)

### Wire (Fulfill DDD dependencies automatically)

[Link](WIRE.md)
