## Issue priorities

This repository uses gitlab issues as issue tracker.
The priority of the issue is shown as below labels.

- must
  - Highest priority
- should
  - Middle priority
- could
  - Low priority

### The priority rules

- must

  - Critical problems like game stuck or data doesn't get saved.
  - User can't do the main feature by this issue.

- should

  - Major problem, but there is alternative way to avoid the problem.
  - User can't do the sub feature by this issue.

- could

  - Really minor problems like wrong display value
  - User can continue play by avoid things.
  - User can't do the minor feature by this issue.
