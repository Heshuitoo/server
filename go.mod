module gitlab.com/kirafan/sparkle/server

go 1.20

require (
	github.com/google/wire v0.5.0
	github.com/gorilla/mux v1.8.0
	github.com/jinzhu/copier v0.3.5
	github.com/swaggo/http-swagger v1.3.4
	github.com/urfave/cli v1.22.14
	golang.org/x/exp v0.0.0-20230321023759-10a507213a29
	gorm.io/gorm v1.25.3
)

require (
	github.com/KyleBanks/depth v1.2.1 // indirect
	github.com/cpuguy83/go-md2man/v2 v2.0.2 // indirect
	github.com/go-openapi/jsonpointer v0.19.6 // indirect
	github.com/go-openapi/jsonreference v0.20.2 // indirect
	github.com/go-openapi/spec v0.20.8 // indirect
	github.com/go-openapi/swag v0.22.3 // indirect
	github.com/go-sql-driver/mysql v1.7.0 // indirect
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/jinzhu/now v1.1.5 // indirect
	github.com/josharian/intern v1.0.0 // indirect
	github.com/mailru/easyjson v0.7.7 // indirect
	github.com/mattn/go-sqlite3 v1.14.16 // indirect
	github.com/rogpeppe/go-internal v1.10.0 // indirect
	github.com/russross/blackfriday/v2 v2.1.0 // indirect
	github.com/swaggo/files v1.0.1 // indirect
	github.com/swaggo/swag v1.8.12 // indirect
	golang.org/x/net v0.8.0 // indirect
	golang.org/x/sys v0.6.0 // indirect
	golang.org/x/tools v0.7.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)

require (
	github.com/google/go-cmp v0.5.9
	github.com/google/uuid v1.3.0
	github.com/joho/godotenv v1.5.1
	github.com/lithammer/shortuuid/v3 v3.0.7
	github.com/sirupsen/logrus v1.9.0
	github.com/thomas-tacquet/gormv2-logrus v1.2.2
	golang.org/x/crypto v0.6.0
	golang.org/x/text v0.8.0
	gorm.io/datatypes v1.1.1
	gorm.io/driver/mysql v1.5.1
	gorm.io/driver/sqlite v1.4.4
)
