package ctx_func

import (
	"context"
	"errors"
)

type key int

// CtxUserId is context key for getting userId
const CtxUserId key = 1

// GetUserID gets a requested user's id from context
func GetUserID(ctx context.Context) (uint64, error) {
	v := ctx.Value(CtxUserId)
	userId, ok := v.(uint64)
	if !ok {
		return 0, errors.New("could not parse user id header")
	}
	return uint64(userId), nil
}
