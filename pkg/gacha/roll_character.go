package gacha

import (
	"errors"
	"math/rand"
	"time"
)

type GachaCharacterHandler struct {
	randomGenerator *rand.Rand
}

func NewGachaCharacterHandler(randSource *rand.Source) GachaCharacterHandler {
	if randSource != nil {
		return GachaCharacterHandler{
			randomGenerator: rand.New(*randSource),
		}
	}
	return GachaCharacterHandler{
		randomGenerator: rand.New(rand.NewSource(time.Now().UnixNano())),
	}
}

func (h *GachaCharacterHandler) Roll(
	rarity GachaCharacterRarity,
	drops GachaCharacterDrops,
	pickUps GachaCharacterDrops,
) ([]uint32, error) {
	// validate params
	if rarity == nil {
		return nil, errors.New("rarity must not be nil")
	}
	for _, r := range rarity {
		if r != Rarity5 && r != Rarity4 && r != Rarity3 {
			return nil, errors.New("rarity must contain only 5, 4, 3")
		}
	}
	if drops == nil {
		return nil, errors.New("drops must not be nil")
	}
	for _, r := range []Rarity{Rarity5, Rarity4, Rarity3} {
		_, ok := drops[r]
		if !ok {
			return nil, errors.New("drops must contain all rarity")
		}
	}
	if pickUps != nil {
		for _, r := range []Rarity{Rarity5, Rarity4, Rarity3} {
			_, ok := pickUps[r]
			if !ok {
				return nil, errors.New("pickUps must contain all rarity")
			}
		}
	}
	results := make([]uint32, len(rarity))
	// gacha without pickup
	if pickUps == nil {
		for i := range rarity {
			characterIDs := drops[rarity[i]]
			index := h.randomGenerator.Intn(len(characterIDs))
			results[i] = characterIDs[index]
		}
		return results, nil
	}
	// gacha with pickup
	for i := range rarity {
		var characterIDs []uint32
		// 50% chance to use pickup
		shouldUsePickUps := h.randomGenerator.Intn(2) == 0
		if shouldUsePickUps && len(pickUps[rarity[i]]) > 0 {
			characterIDs = pickUps[rarity[i]]
		} else {
			characterIDs = drops[rarity[i]]
		}
		index := h.randomGenerator.Intn(len(characterIDs))
		results[i] = characterIDs[index]
	}
	return results, nil
}
