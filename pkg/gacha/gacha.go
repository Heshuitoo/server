package gacha

import "math/rand"

type GachaHandler struct {
	rarityHandler    GachaRarityHandler
	characterHandler GachaCharacterHandler
}

func NewGachaHandler(randSource *rand.Source) GachaHandler {
	return GachaHandler{
		rarityHandler:    NewGachaRarityHandler(randSource),
		characterHandler: NewGachaCharacterHandler(randSource),
	}
}

func (h *GachaHandler) Roll(
	times int,
	isChanceUp bool,
	drops GachaCharacterDrops,
	pickUps GachaCharacterDrops,
) ([]uint32, error) {
	rarities, err := h.rarityHandler.Roll(times, isChanceUp)
	if err != nil {
		return nil, err
	}
	characters, err := h.characterHandler.Roll(rarities, drops, pickUps)
	if err != nil {
		return nil, err
	}
	return characters, nil
}
