package upgrade

import (
	"math/rand"
	"time"
)

type CharacterUpgradeProbability uint8

const (
	CharacterUpgradeGoodProb    CharacterUpgradeProbability = 80
	CharacterUpgradeGreatProb   CharacterUpgradeProbability = 15
	CharacterUpgradePerfectProb CharacterUpgradeProbability = 5
)

type CharacterUpgradeResult uint8

const (
	CharacterUpgradeResultGood CharacterUpgradeResult = iota
	CharacterUpgradeResultGreat
	CharacterUpgradeResultPerfect
)

type UpgradeCharacterHandler struct {
	randomGenerator *rand.Rand
}

func NewUpgradeCharacterHandler(randSource *rand.Source) UpgradeCharacterHandler {
	if randSource != nil {
		return UpgradeCharacterHandler{
			randomGenerator: rand.New(*randSource),
		}
	}
	return UpgradeCharacterHandler{
		randomGenerator: rand.New(rand.NewSource(time.Now().UnixNano())),
	}
}

func (h UpgradeCharacterHandler) Roll() CharacterUpgradeResult {
	r := CharacterUpgradeProbability(h.randomGenerator.Intn(100))
	if r < CharacterUpgradePerfectProb {
		return CharacterUpgradeResultPerfect
	}
	if r < CharacterUpgradePerfectProb+CharacterUpgradeGreatProb {
		return CharacterUpgradeResultGreat
	}
	return CharacterUpgradeResultGood
}
