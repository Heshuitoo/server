package upgrade

import (
	"math/rand"
	"time"
)

type UpgradeWeaponHandler struct {
	randomGenerator *rand.Rand
}

func NewUpgradeWeaponHandler(randSource *rand.Source) UpgradeWeaponHandler {
	if randSource != nil {
		return UpgradeWeaponHandler{
			randomGenerator: rand.New(*randSource),
		}
	}
	return UpgradeWeaponHandler{
		randomGenerator: rand.New(rand.NewSource(time.Now().UnixNano())),
	}
}

func (h UpgradeWeaponHandler) Roll() WeaponUpgradeResult {
	r := WeaponUpgradeProbability(h.randomGenerator.Intn(100))
	if r < WeaponUpgradePerfectProb {
		return WeaponUpgradeResultPerfect
	}
	if r < WeaponUpgradePerfectProb+WeaponUpgradeGreatProb {
		return WeaponUpgradeResultGreat
	}
	return WeaponUpgradeResultGood
}
