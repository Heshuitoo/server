package env

import (
	"fmt"
	"os"

	"github.com/joho/godotenv"
)

type ScheduleEndpoint string

func LoadScheduleEndpoint() ScheduleEndpoint {
	if envFilePath := os.Getenv("GO_ENV"); envFilePath != "" {
		if err := godotenv.Load(envFilePath); err != nil {
			fmt.Print("envFile " + envFilePath + "could not be loaded")
		}
	} else {
		_ = godotenv.Load(".env")
	}
	key := ScheduleEndpoint(os.Getenv("SCHEDULE_ENDPOINT"))
	return key
}
