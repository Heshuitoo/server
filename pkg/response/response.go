package response

type BaseResponse struct {
	NewAchievementCount int32  `json:"newAchievementCount"`
	ResultCode          int32  `json:"resultCode"`
	ResultMessage       string `json:"resultMessage"`
	ServerTime          string `json:"serverTime"`
	ServerVersion       int32  `json:"serverVersion"`
	// Below values used if RESULT_INVALID_AUTH_VOIDED and login request
	Subject *string `json:"subject,omitempty"`
	Message *string `json:"message,omitempty"`
}

func NewSuccessResponse() BaseResponse {
	return BaseResponse{
		ServerTime:          NewSparkleTime(),
		ResultCode:          int32(RESULT_SUCCESS),
		ResultMessage:       "",
		NewAchievementCount: 0,
		ServerVersion:       SERVER_VERSION,
	}
}

func NewErrorResponse(code ResultCode) BaseResponse {
	return BaseResponse{
		ServerTime:          NewSparkleTime(),
		ResultCode:          int32(code),
		ResultMessage:       "",
		NewAchievementCount: 0,
		ServerVersion:       SERVER_VERSION,
	}
}

func NewErrorResponseWithMessage(subject string, message string) BaseResponse {
	return BaseResponse{
		ServerTime:          NewSparkleTime(),
		ResultCode:          int32(RESULT_INVALID_AUTH_VOIDED),
		ResultMessage:       "",
		NewAchievementCount: 0,
		ServerVersion:       SERVER_VERSION,
		Subject:             &subject,
		Message:             &message,
	}
}

func NewMaintenanceResponseWithMessage(message string) BaseResponse {
	return BaseResponse{
		ServerTime:          NewSparkleTime(),
		ResultCode:          int32(RESULT_MAINTENANCE),
		ResultMessage:       "",
		NewAchievementCount: 0,
		ServerVersion:       SERVER_VERSION,
		Message:             &message,
	}
}
