package parser

import (
	"strconv"
	"strings"
)

type SkillResult struct {
	Skill1 uint64
	Skill2 uint64
	Skill3 uint64
}

func parseUintOrDefault(str string) uint {
	val, err := strconv.ParseUint(str, 10, 32)
	if err != nil {
		return 0
	}
	return uint(val)
}

func ParseCharacterExpString(requestString string) map[uint64]SkillResult {
	skillMap := make(map[uint64]SkillResult)
	skillStrings := strings.Split(requestString, ",")
	for _, skillString := range skillStrings {
		skillParts := strings.Split(skillString, ":")
		if skillParts[0] == "" || len(skillParts) < 4 {
			return skillMap
		}
		managedCharacterId, _ := strconv.ParseUint(skillParts[0], 10, 64)
		skillResult := SkillResult{
			Skill1: uint64(parseUintOrDefault(skillParts[1])),
			Skill2: uint64(parseUintOrDefault(skillParts[2])),
			Skill3: uint64(parseUintOrDefault(skillParts[3])),
		}
		skillMap[managedCharacterId] = skillResult
	}
	return skillMap
}

func ParseWeaponExpString(requestString string) map[uint64]SkillResult {
	skillMap := make(map[uint64]SkillResult)
	skillStrings := strings.Split(requestString, ",")
	for _, skillString := range skillStrings {
		skillParts := strings.Split(skillString, ":")
		if skillParts[0] == "" || len(skillParts) < 2 {
			return skillMap
		}
		weaponID, _ := strconv.ParseUint(skillParts[0], 10, 64)
		skillResult := SkillResult{
			Skill1: uint64(parseUintOrDefault(skillParts[1])),
			Skill2: 0,
			Skill3: 0,
		}
		skillMap[weaponID] = skillResult
	}
	return skillMap
}
