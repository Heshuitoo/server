package encrypt

import (
	"reflect"
	"testing"
)

func Test_compress(t *testing.T) {
	type args struct {
		str []byte
	}
	tests := []struct {
		name string
		args args
		want []byte
	}{
		{
			name: "compress success",
			args: args{
				str: []byte("test"),
			},
			want: []byte{120, 156, 43, 73, 45, 46, 1, 0, 4, 93, 1, 193},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := compress(tt.args.str); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("compress() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_decompress(t *testing.T) {
	type args struct {
		b []byte
	}
	tests := []struct {
		name string
		args args
		want []byte
	}{
		{
			name: "compress success",
			args: args{
				b: []byte{120, 156, 42, 73, 45, 46, 1, 4, 0, 0, 255, 255, 4, 93, 1, 193},
			},
			want: []byte("test"),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := decompress(tt.args.b)
			if err != nil {
				t.Errorf("decompress() = %v, want %v", got, err)
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("decompress() = %v, want %v", got, tt.want)
			}
		})
	}
}
