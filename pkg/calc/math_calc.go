package calc

import (
	"github.com/jinzhu/copier"
	"golang.org/x/exp/constraints"
)

func Contains[T comparable](elems []T, v T) bool {
	for _, s := range elems {
		if v == s {
			return true
		}
	}
	return false
}

func Max[T constraints.Ordered](a, b T) T {
	if a > b {
		return a
	}
	return b
}

func Min[T constraints.Ordered](a, b T) T {
	if a < b {
		return a
	}
	return b
}

func ToPtr[T constraints.Ordered](v T) *T {
	return &v
}

func Copy(dst interface{}, src interface{}) error {
	err := copier.Copy(dst, src)
	return err
}
