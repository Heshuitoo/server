### Assets

This folder is placeholder for database seed files.
Currently, below files are supported.

- quests.json
- event_quests.json
- quest_stamina_reductions.json
- quest_periods.json
- character_quests.json
- chapters.json
