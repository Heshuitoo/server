package main

import (
	"log"
	"os"
	"time"

	"github.com/urfave/cli"
	batch_repository "gitlab.com/kirafan/sparkle/server/cmd/batch/repository"
	"gitlab.com/kirafan/sparkle/server/cmd/batch/task"
	"gitlab.com/kirafan/sparkle/server/internal/application/service"
	"gitlab.com/kirafan/sparkle/server/internal/application/usecase"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/database"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/persistence"
)

func runTasks(tasks []batch_repository.TaskRepository) {
	for _, task := range tasks {
		log.Println("started task: " + task.GetName())
		err := task.Run()
		if err != nil {
			log.Println(err)
		}
		log.Println("finished task: " + task.GetName())
	}
}

func main() {
	logger := database.InitLogger()
	loggerRepo := database.InitLoggerRepo(logger)
	db := database.InitDatabase(loggerRepo, logger)

	service := service.InitializeUserGachaService(loggerRepo, db)
	userRepo := persistence.NewUserRepositoryImpl(db)
	userUsecase := usecase.NewUserUsecase(userRepo, loggerRepo)
	refreshUserGachaTask := task.NewTaskRefreshUserGacha(db, userUsecase, service)

	tasks := []batch_repository.TaskRepository{
		refreshUserGachaTask,
	}

	app := cli.NewApp()
	app.Name = "sparkle-api batch"
	app.Usage = "run batch process"
	app.Flags = []cli.Flag{
		cli.BoolFlag{
			Name:  "force",
			Usage: "force immediate execution",
		},
	}

	app.Action = func(c *cli.Context) error {
		log.Println("batch process started.")
		if c.Bool("force") {
			log.Println("running task (force)...")
			runTasks(tasks)
		}
		// 毎日午前0時にデータを更新する
		for {
			until := time.Date(time.Now().Year(), time.Now().Month(), time.Now().Day()+1, 0, 0, 0, 0, time.Local)
			time.Sleep(time.Until(until))
			log.Println("running task...")
			runTasks(tasks)
		}
	}
	app.Run(os.Args)
}
