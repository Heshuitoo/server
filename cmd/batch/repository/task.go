package repository

type TaskRepository interface {
	Run() error
	GetName() string
}
