package middleware

import (
	"bytes"
	"io"
	"net/http"

	"gitlab.com/kirafan/sparkle/server/pkg/encrypt"
)

func NewAppDecryptionMiddleware(ignoreRoutes *[]string, key *encrypt.Key, padding *encrypt.Pad) func(next http.HandlerFunc) http.HandlerFunc {
	cipher := encrypt.NewSparkleCipher(*key, *padding)
	return func(next http.HandlerFunc) http.HandlerFunc {
		return func(w http.ResponseWriter, r *http.Request) {
			if r.Header.Get("PznxhHrYEDRrfARyBgRp") == "1" {
				body, err := io.ReadAll(r.Body)
				if err != nil {
					http.Error(w, err.Error(), http.StatusInternalServerError)
					return
				}
				decryptedBody, err := cipher.DecryptAsRequestBytes(r.URL.Path, string(body))
				if err != nil {
					http.Error(w, err.Error(), http.StatusInternalServerError)
					return
				}
				r.Body = io.NopCloser(bytes.NewReader(decryptedBody))
				r.ContentLength = int64(len(decryptedBody))
			}
			next(w, r)
		}
	}
}
