package middleware

import (
	"context"
	"net/http"

	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	gen "gitlab.com/kirafan/sparkle/server/internal/interfaces"
	"gitlab.com/kirafan/sparkle/server/pkg/calc"
	"gitlab.com/kirafan/sparkle/server/pkg/ctx_func"
	"gitlab.com/kirafan/sparkle/server/pkg/response"
)

func NewAppSessionMiddleware(sessionIgnoreRoutes []string, sessionService repository.SessionRepository) func(next http.HandlerFunc) http.HandlerFunc {
	return func(next http.HandlerFunc) http.HandlerFunc {
		return func(w http.ResponseWriter, r *http.Request) {
			if !calc.Contains(sessionIgnoreRoutes, r.URL.Path) {
				session := r.Header.Get("nVJuf6NNHtyez3xTRKix")
				userId, err := sessionService.Validate(session)
				if err != nil {
					resp := gen.Response(http.StatusOK, response.NewErrorResponse(response.RESULT_PLAYER_SESSION_EXPIRED))
					gen.EncodeJSONResponse(resp.Body, &resp.Code, w)
					return
				}
				ctx := context.WithValue(r.Context(), ctx_func.CtxUserId, uint(userId))
				r = r.WithContext(ctx)
			}
			next(w, r)
		}
	}
}
