//go:build wireinject
// +build wireinject

package wires

import (
	"github.com/google/wire"
	"github.com/gorilla/mux"
	"gitlab.com/kirafan/sparkle/server/internal/application/service"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/middleware"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/persistence"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/wires_providers"
	"gitlab.com/kirafan/sparkle/server/pkg/encrypt"
	"gitlab.com/kirafan/sparkle/server/pkg/env"
	"gorm.io/gorm"
)

func InitializeMiddlewares(logger repository.LoggerRepository, db *gorm.DB) middleware.Middlewares {
	wire.Build(
		encrypt.LoadKey,
		encrypt.LoadPad,
		persistence.NewSessionRepositoryImpl,
		middleware.GetMiddlewares,
	)
	return middleware.Middlewares{}
}

func InitializeRouter(logger repository.LoggerRepository, db *gorm.DB) *mux.Router {
	wire.Build(
		wires_providers.UserUsecaseSet,
		wires_providers.VersionUsecaseSet,
		wires_providers.ChapterUsecaseSet,
		wires_providers.QuestUsecaseSet,
		wires_providers.MissionUsecaseSet,
		wires_providers.PresentUsecaseSet,
		wires_providers.QuestWaveUsecaseSet,
		wires_providers.GachaUsecaseSet,
		wires_providers.NamedTypeUsecaseSet,
		wires_providers.CharacterUsecaseSet,
		wires_providers.ItemUsecaseSet,
		wires_providers.ScheduleUsecaseSet,
		wires_providers.LoginBonusUsecaseSet,
		wires_providers.ExpTableWeaponUsecaseSet,
		wires_providers.ExpTableCharacterUsecaseSet,
		wires_providers.ExpTableSkillUsecaseSet,
		wires_providers.ExpTableRankUsecaseSet,
		wires_providers.ExpTableFriendshipUsecaseSet,
		wires_providers.EvoTableLimitBreakUsecaseSet,
		wires_providers.EvoTableEvolutionUsecaseSet,
		wires_providers.RoomObjectUsecaseSet,
		wires_providers.TownFacilityUsecaseSet,
		wires_providers.LevelTableTownFacilityUsecaseSet,
		wires_providers.ItemTableTownFacilityUsecaseSet,
		wires_providers.WeaponCharacterTableUsecaseSet,
		wires_providers.WeaponTableUsecaseSet,
		wires_providers.WeaponRecipeUsecaseSet,
		wires_providers.EvoTableWeaponUsecaseSet,
		// Service (DDD)
		service.NewAppVersionService,
		service.NewUserQuestService,
		service.NewPlayerGachaService,
		service.NewUserPresentService,
		service.NewLoginService,
		service.NewScheduleService,
		service.NewUserTownFacilityService,
		service.NewUserRoomObjectService,
		service.NewUserRoomService,
		service.NewCharacterService,
		service.NewPlayerAgeService,
		service.NewPlayerBattlePartyService,
		service.NewPlayerBadgeService,
		service.NewPlayerAbilityService,
		service.NewEventBannerService,
		service.NewInformationService,
		service.NewAchievementService,
		service.NewPlayerAdvService,
		service.NewPlayerExchangeShopService,
		service.NewPlayerFavoriteMemberService,
		service.NewPlayerFieldPartyService,
		service.NewPlayerFriendService,
		service.NewPlayerChestService,
		service.NewPlayerContentRoomService,
		service.NewPlayerGeneralFlagSaveService,
		service.NewPlayerSetService,
		service.NewPlayerImportService,
		service.NewPlayerWeaponService,
		service.NewPlayerPushNotificationService,
		service.NewPlayerPushTokenService,
		// Service (router)
		ApiServicerSet,
		// Middleware (router)
		InitializeMiddlewares,
		// Injectable (router)
		InjectableControllerSet,
		// Registerable (router)
		infrastructure.NewRouter,
		// value loader
		env.LoadScheduleEndpoint,
	)
	// Below line is unused, just for passing lint check
	return &mux.Router{}
}
