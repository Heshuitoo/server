package seed

import "gorm.io/gorm"

func AutoSeed(db *gorm.DB) {
	if db.Dialector.Name() == "sqlite" {
		if res := db.Exec("PRAGMA synchronous=OFF;PRAGMA journal_mode=MEMORY;", nil); res.Error != nil {
			panic(res.Error)
		}
	}
	SeedVersions(db)
	SeedQuests(db)
	SeedQuestWaves(db)
	SeedQuestWaveDrops(db)
	SeedQuestWaveRandoms(db)
	SeedChapters(db)
	SeedEventQuests(db)
	SeedQuestStaminaReductions(db)
	SeedQuestPeriods(db)
	SeedCharacterQuests(db)
	SeedMissions(db)
	SeedItems(db)
	SeedRoomObjects(db)
	SeedTownFacilities(db)
	SeedItemTownFacilities(db)
	SeedLevelTownFacilities(db)
	SeedCharacters(db)
	SeedNamedTypes(db)
	SeedPresents(db)
	SeedOffers(db)
	SeedGachas(db)
	// SeedGachaTables(db)
	SeedUsers(db)
	SeedExpTableRank(db)
	SeedExpTableCharacter(db)
	SeedExpTableWeapon(db)
	SeedExpTableFriendship(db)
	SeedExpTableSkill(db)
	SeedEvoTableLimitBreak(db)
	SeedEvoTableEvolution(db)
	SeedEvoTableWeapon(db)
	SeedLoginBonuses(db)
	SeedWeaponCharacterTables(db)
	SeedWeapons(db)
	SeedWeaponRecipes(db)
	if db.Dialector.Name() == "sqlite" {
		if res := db.Exec("PRAGMA synchronous=NORMAL;PRAGMA journal_mode=DELETE;", nil); res.Error != nil {
			panic(res.Error)
		}
	}
}
