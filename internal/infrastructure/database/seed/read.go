package seed

import (
	"errors"
	"fmt"
	"io"
	"net/http"
	"os"

	"github.com/joho/godotenv"
)

func readRemoteFile(endpoint string, path string) (*[]byte, error) {
	resp, err := http.Get(endpoint + path)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	bytes, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	return &bytes, nil
}

func Read(filename string) ([]byte, error) {
	_ = godotenv.Load(".env")
	switch os.Getenv("SEED_SOURCE") {
	case "remote":
		endpoint := os.Getenv("SEED_ENDPOINT")
		if endpoint == "" {
			fmt.Print("[WARN] Seed file endpoint was not specified\n")
			return nil, errors.New("endpoint was not specified")
		}
		fmt.Printf("[INFO] Reading seed file from remote\n")
		file, err := readRemoteFile(endpoint, fmt.Sprintf("/%v.json", filename))
		// Read the JSON file into a byte slice
		if err != nil {
			fmt.Printf("[WARN] Seed file %v was not found\n", filename)
			return nil, err
		}
		return *file, nil
	case "local":
		fallthrough
	default:
		fmt.Printf("[INFO] Reading seed file from local")
		file, err := os.ReadFile(fmt.Sprintf("assets/%v.json", filename))
		// Read the JSON file into a byte slice
		if err != nil {
			fmt.Printf("[WARN] Seed file %v was not found\n", filename)
			return nil, err
		}
		return file, nil
	}
}
