package seed

import (
	"encoding/json"

	model_quest "gitlab.com/kirafan/sparkle/server/internal/domain/model/quest"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

func SeedQuestPeriods(db *gorm.DB) {
	questPeriodsFile, err := Read("quest_periods")
	if err != nil {
		return
	}
	var questPeriods []model_quest.EventQuestPeriod
	err = json.Unmarshal(questPeriodsFile, &questPeriods)
	if err != nil {
		panic(err)
	}
	result := db.Clauses(clause.OnConflict{
		DoNothing: true,
	}).CreateInBatches(questPeriods, 50)
	if result.Error != nil {
		panic(result.Error)
	}
}
