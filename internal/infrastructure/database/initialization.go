package database

import (
	"github.com/sirupsen/logrus"
	gormv2logrus "github.com/thomas-tacquet/gormv2-logrus"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/persistence"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
)

// Make a gorm config with inserting logrus logger
func GetGormConfig(l *logrus.Logger) gorm.Config {
	gormLogger := gormv2logrus.NewGormlog(gormv2logrus.WithLogrus(l))
	logLevel := l.GetLevel()
	switch logLevel {
	case logrus.TraceLevel:
	case logrus.DebugLevel:
	case logrus.InfoLevel:
		gormLogger.LogMode(logger.Info)
	case logrus.WarnLevel:
		gormLogger.LogMode(logger.Warn)
	case logrus.ErrorLevel:
	default:
		gormLogger.LogMode(logger.Error)
	}
	gormConfig := gorm.Config{
		Logger:          gormLogger,
		CreateBatchSize: 150,
	}
	return gormConfig
}

// Get a gorm database instance with specified database config
func GetDatabase(dbConfig ConfigList, logger *logrus.Logger) *gorm.DB {
	if !dbConfig.DatabaseVerbose {
		logger.Level = logrus.InfoLevel
	}
	gormConfig := GetGormConfig(logger)
	switch dbConfig.DatabaseType {
	case DatabaseTypeSqliteInMemory:
		return NewSQLiteInMemoryDatabase(dbConfig, gormConfig)
	case DatabaseTypeSqlite:
		return NewSQLiteDatabase(dbConfig, gormConfig)
	case DatabaseTypeMysql:
		return NewMySQLDatabase(dbConfig, gormConfig)
	default:
		panic("Unknown database type")
	}
}

func InitLogger() *logrus.Logger {
	logger := logrus.New()
	logger.SetLevel(logrus.DebugLevel)
	return logger
}

func InitLoggerRepo(logger *logrus.Logger) repository.LoggerRepository {
	loggerRepo := persistence.NewLoggerRepository(logger, persistence.DebugLevel)
	return loggerRepo
}

func InitDatabase(loggerRepo repository.LoggerRepository, logger *logrus.Logger) *gorm.DB {
	db := GetDatabase(GetConfig(loggerRepo), logger)
	return db
}
