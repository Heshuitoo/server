package persistence

import (
	model_weapon "gitlab.com/kirafan/sparkle/server/internal/domain/model/weapon"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	value_character "gitlab.com/kirafan/sparkle/server/internal/domain/value/character"

	"gorm.io/gorm"
)

type weaponCharacterTableRepositoryImpl struct {
	Conn *gorm.DB
}

func NewWeaponCharacterTableRepositoryImpl(conn *gorm.DB) repository.WeaponCharacterTableRepository {
	return &weaponCharacterTableRepositoryImpl{Conn: conn}
}

func (rp *weaponCharacterTableRepositoryImpl) FindByCharacterId(characterId value_character.CharacterId) (*model_weapon.WeaponCharacterTable, error) {
	var data *model_weapon.WeaponCharacterTable
	result := rp.Conn.Where("character_id = ?", characterId).First(&data)
	if result.Error != nil {
		return nil, result.Error
	}
	return data, nil
}
