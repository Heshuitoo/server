package persistence

import (
	model_present "gitlab.com/kirafan/sparkle/server/internal/domain/model/present"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"

	"gorm.io/gorm"
)

type presentRepositoryImpl struct {
	Conn *gorm.DB
}

func NewPresentRepositoryImpl(conn *gorm.DB) repository.PresentRepository {
	return &presentRepositoryImpl{Conn: conn}
}

func (rp *presentRepositoryImpl) FindPresents(query *model_present.Present, criteria map[string]interface{}, associations *[]string) ([]*model_present.Present, error) {
	var datas []*model_present.Present
	var result *gorm.DB
	chain := rp.Conn
	if associations != nil {
		for _, association := range *associations {
			chain = chain.Preload(association)
		}
	}
	if query != nil {
		result = chain.Where(query).Find(&datas)
	} else {
		result = chain.Where(criteria).Find(&datas)
	}
	if result.Error != nil {
		return nil, result.Error
	}
	return datas, nil
}

func (rp *presentRepositoryImpl) FindPresent(query *model_present.Present, criteria map[string]interface{}, associations *[]string) (*model_present.Present, error) {
	var data *model_present.Present
	var result *gorm.DB
	chain := rp.Conn
	if associations != nil {
		for _, association := range *associations {
			chain = chain.Preload(association)
		}
	}
	if query != nil {
		result = chain.Where(&query).First(&data)
	} else {
		result = chain.Where(criteria).First(&data)
	}
	if result.Error != nil {
		return nil, result.Error
	}
	return data, nil
}
