package persistence

import (
	model_evo_table "gitlab.com/kirafan/sparkle/server/internal/domain/model/evo_table"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"

	"gorm.io/gorm"
)

type evoTableLimitBreakRepositoryImpl struct {
	Conn *gorm.DB
}

func NewEvoTableLimitBreakRepositoryImpl(conn *gorm.DB) repository.EvoTableLimitBreakRepository {
	return &evoTableLimitBreakRepositoryImpl{Conn: conn}
}

func (rp *evoTableLimitBreakRepositoryImpl) FindByRecipeId(recipeId uint) (*model_evo_table.EvoTableLimitBreak, error) {
	var data *model_evo_table.EvoTableLimitBreak
	if result := rp.Conn.Where(&model_evo_table.EvoTableLimitBreak{
		RecipeId: recipeId,
	}).Find(&data); result.Error != nil {
		return nil, result.Error
	}
	return data, nil
}
