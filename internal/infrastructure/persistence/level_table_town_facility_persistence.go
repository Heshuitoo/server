package persistence

import (
	model_level_table "gitlab.com/kirafan/sparkle/server/internal/domain/model/level_table"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"

	"gorm.io/gorm"
)

type levelTableTownFacilityRepositoryImpl struct {
	Conn *gorm.DB
}

func NewLevelTableTownFacilityRepositoryImpl(conn *gorm.DB) repository.LevelTableTownFacilityRepository {
	return &levelTableTownFacilityRepositoryImpl{Conn: conn}
}

func (rp *levelTableTownFacilityRepositoryImpl) FindLevelTableTownFacility(query *model_level_table.LevelTableTownFacility) (*model_level_table.LevelTableTownFacility, error) {
	var data *model_level_table.LevelTableTownFacility
	if result := rp.Conn.Preload("FieldItemDropProbabilities").Preload("FieldItemDropIds").Where(query).First(&data); result.Error != nil {
		return nil, result.Error
	}
	return data, nil
}
