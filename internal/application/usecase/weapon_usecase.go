package usecase

import (
	model_weapon "gitlab.com/kirafan/sparkle/server/internal/domain/model/weapon"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	value_weapon "gitlab.com/kirafan/sparkle/server/internal/domain/value/weapon"
)

type WeaponUsecase interface {
	GetWeaponById(weaponId value_weapon.WeaponId) (*model_weapon.Weapon, error)
}

type weaponUsecase struct {
	rp     repository.WeaponRepository
	logger repository.LoggerRepository
}

func NewWeaponUsecase(rp repository.WeaponRepository, logger repository.LoggerRepository) WeaponUsecase {
	return &weaponUsecase{rp, logger}
}

func (uc *weaponUsecase) GetWeaponById(weaponId value_weapon.WeaponId) (*model_weapon.Weapon, error) {
	weapon, err := uc.rp.FindByWeaponId(weaponId)
	if err != nil {
		return nil, err
	}
	return weapon, nil
}
