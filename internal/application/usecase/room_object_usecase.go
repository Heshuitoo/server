package usecase

import (
	model_room_object "gitlab.com/kirafan/sparkle/server/internal/domain/model/room_object"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
)

type RoomObjectUsecase interface {
	GetAll() ([]*model_room_object.RoomObject, error)
	GetRoomObject(id uint32) (*model_room_object.RoomObject, error)
}

type roomObjectUsecase struct {
	ro     repository.RoomObjectRepository
	logger repository.LoggerRepository
}

func NewRoomObjectUsecase(ro repository.RoomObjectRepository, logger repository.LoggerRepository) RoomObjectUsecase {
	return &roomObjectUsecase{ro, logger}
}

func (cu *roomObjectUsecase) GetAll() ([]*model_room_object.RoomObject, error) {
	roomObjects, err := cu.ro.FindRoomObjects(nil, nil, nil)
	if err != nil {
		return nil, err
	}
	return roomObjects, nil
}

func (cu *roomObjectUsecase) GetRoomObject(id uint32) (*model_room_object.RoomObject, error) {
	roomObject, err := cu.ro.FindRoomObject(uint(id))
	if err != nil {
		return nil, err
	}
	return roomObject, nil
}
