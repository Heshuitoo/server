package usecase

import (
	model_exp_table "gitlab.com/kirafan/sparkle/server/internal/domain/model/exp_table"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
)

type ExpTableCharacterUsecase interface {
	GetNextExpTableCharacter(currentExp uint64) (*model_exp_table.ExpTableCharacter, error)
	GetRequiredCoinsForUpgrade(currentLevel uint8) (value_user.Gold, error)
}

type expTableCharacterUsecase struct {
	rp     repository.ExpTableCharacterRepository
	logger repository.LoggerRepository
}

func NewExpTableCharacterUsecase(rp repository.ExpTableCharacterRepository, logger repository.LoggerRepository) ExpTableCharacterUsecase {
	return &expTableCharacterUsecase{rp, logger}
}

func (uc *expTableCharacterUsecase) GetNextExpTableCharacter(currentExp uint64) (*model_exp_table.ExpTableCharacter, error) {
	criteria := map[string]interface{}{
		"total_exp > ?": currentExp,
	}
	expTableRank, err := uc.rp.FindExpTableCharacter(nil, criteria)
	if err != nil {
		return nil, err
	}
	return expTableRank, nil
}

func (uc *expTableCharacterUsecase) GetRequiredCoinsForUpgrade(currentLevel uint8) (value_user.Gold, error) {
	coins, err := uc.rp.GetRequiredCoinsForUpgrade(currentLevel)
	if err != nil {
		return 0, err
	}
	golds := value_user.NewGold(uint64(coins))
	return golds, nil
}
