package usecase

import (
	"errors"

	model_schedule "gitlab.com/kirafan/sparkle/server/internal/domain/model/schedule"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	value_character "gitlab.com/kirafan/sparkle/server/internal/domain/value/character"
)

type ScheduleUsecase interface {
	CreateSchedule(characterIds []value_character.CharacterId, gridData string) ([]*model_schedule.Schedule, error)
	RefreshSchedule(characterSchedules []string) ([]string, error)
}

var ErrRefreshScheduleInvalidParamLength = errors.New("characterIds and schedules must have same length")

type scheduleUsecase struct {
	sr     repository.ScheduleRepository
	logger repository.LoggerRepository
}

func NewScheduleUsecase(
	sr repository.ScheduleRepository,
	logger repository.LoggerRepository,
) ScheduleUsecase {
	return &scheduleUsecase{sr, logger}
}

func (u *scheduleUsecase) CreateSchedule(characterIds []value_character.CharacterId, gridData string) ([]*model_schedule.Schedule, error) {
	schedules, err := u.sr.Create(characterIds, gridData)
	if err != nil {
		return nil, err
	}
	return schedules, nil
}

func (u *scheduleUsecase) RefreshSchedule(characterSchedules []string) ([]string, error) {
	refreshedSchedules, err := u.sr.Refresh(characterSchedules)
	if err != nil {
		return nil, err
	}
	return refreshedSchedules, nil
}
