package usecase

import (
	"errors"

	model_item "gitlab.com/kirafan/sparkle/server/internal/domain/model/item"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	value_character "gitlab.com/kirafan/sparkle/server/internal/domain/value/character"
	value_exp "gitlab.com/kirafan/sparkle/server/internal/domain/value/exp"
	value_item "gitlab.com/kirafan/sparkle/server/internal/domain/value/item"
)

type ItemUsecase interface {
	GetAllItems() ([]*model_item.Item, error)
	GetItemsByCategory(category value_item.ItemCategory) ([]*model_item.Item, error)
	GetWeaponUpgradeAmount(itemId int64) (value_exp.WeaponExp, error)
	GetCharacterUpgradeAmount(itemId int64) (value_character.ClassType, value_exp.CharacterExp, error)
}

type itemUsecase struct {
	rp     repository.ItemRepository
	logger repository.LoggerRepository
}

func NewItemUsecase(rp repository.ItemRepository, logger repository.LoggerRepository) ItemUsecase {
	return &itemUsecase{rp, logger}
}

func (uc *itemUsecase) GetAllItems() ([]*model_item.Item, error) {
	items, err := uc.rp.FindItems(nil, nil, nil)
	if err != nil {
		return nil, err
	}
	return items, nil
}

func (uc *itemUsecase) GetItemsByCategory(category value_item.ItemCategory) ([]*model_item.Item, error) {
	items, err := uc.rp.FindItems(&model_item.Item{Category: category}, nil, nil)
	if err != nil {
		return nil, err
	}
	return items, nil
}

func (uc *itemUsecase) GetWeaponUpgradeAmount(itemId int64) (value_exp.WeaponExp, error) {
	amount, err := uc.rp.GetWeaponUpgradeAmount(itemId)
	if err != nil {
		return 0, err
	}
	exp, err := value_exp.NewWeaponExp(uint32(amount))
	if err != nil {
		return 0, errors.New("invalid weapon exp from db")
	}
	return exp, nil
}

func (uc *itemUsecase) GetCharacterUpgradeAmount(itemId int64) (value_character.ClassType, value_exp.CharacterExp, error) {
	classRaw, expRaw, err := uc.rp.GetCharacterUpgradeAmount(itemId)
	if err != nil {
		return 0, 0, err
	}
	classType, err := value_character.NewClassType(uint8(classRaw))
	if err != nil {
		return 0, 0, errors.New("invalid class type from db")
	}
	exp, err := value_exp.NewCharacterExp(uint32(expRaw))
	if err != nil {
		return 0, 0, errors.New("invalid character exp from db")
	}
	return classType, exp, nil
}
