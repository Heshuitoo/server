package usecase

import (
	model_present "gitlab.com/kirafan/sparkle/server/internal/domain/model/present"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	value_present "gitlab.com/kirafan/sparkle/server/internal/domain/value/present"
)

type PresentUsecase interface {
	GetTutorialPresents() ([]*model_present.Present, error)
}

type presentUsecase struct {
	rp     repository.PresentRepository
	logger repository.LoggerRepository
}

func NewPresentUsecase(rp repository.PresentRepository, logger repository.LoggerRepository) PresentUsecase {
	return &presentUsecase{rp, logger}
}

func (pu *presentUsecase) GetTutorialPresents() ([]*model_present.Present, error) {
	criteria := map[string]interface{}{
		"present_insert_type": value_present.PresentInsertTypeTutorial,
	}
	presents, err := pu.rp.FindPresents(nil, criteria, nil)
	if err != nil {
		return nil, err
	}
	return presents, nil
}
