package usecase

import (
	"errors"

	model_version "gitlab.com/kirafan/sparkle/server/internal/domain/model/version"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	value_version "gitlab.com/kirafan/sparkle/server/internal/domain/value/version"
)

type VersionUsecase interface {
	FindByPlatformAndVersion(platform value_version.Platform, version value_version.Version) (*model_version.Version, error)
}

type versionUsecase struct {
	versionRepo repository.VersionRepository
	logger      repository.LoggerRepository
}

func NewVersionUsecase(versionRepo repository.VersionRepository, logger repository.LoggerRepository) VersionUsecase {
	return &versionUsecase{versionRepo: versionRepo, logger: logger}
}

func (vu *versionUsecase) FindByPlatformAndVersion(platform value_version.Platform, version value_version.Version) (*model_version.Version, error) {
	existedVersion, err := vu.versionRepo.FindByPlatformAndVersion(platform, version)
	if err != nil {
		return nil, errors.New("version not supported")
	}
	return existedVersion, nil
}
