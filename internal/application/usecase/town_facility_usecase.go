package usecase

import (
	model_town_facility "gitlab.com/kirafan/sparkle/server/internal/domain/model/town_facility"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
)

type TownFacilityUsecase interface {
	GetTownFacilityById(facilityId uint32) (*model_town_facility.TownFacility, error)
}

type townFacilityUsecase struct {
	rp     repository.TownFacilityRepository
	logger repository.LoggerRepository
}

func NewTownFacilityUsecase(rp repository.TownFacilityRepository, logger repository.LoggerRepository) TownFacilityUsecase {
	return &townFacilityUsecase{rp, logger}
}

func (uc *townFacilityUsecase) GetTownFacilityById(facilityId uint32) (*model_town_facility.TownFacility, error) {
	townFacility, err := uc.rp.FindTownFacility(facilityId)
	if err != nil {
		return nil, err
	}
	return townFacility, nil
}
