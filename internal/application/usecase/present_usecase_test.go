package usecase

import (
	"testing"

	"github.com/google/go-cmp/cmp"
	"github.com/google/go-cmp/cmp/cmpopts"
	model_present "gitlab.com/kirafan/sparkle/server/internal/domain/model/present"
	value_present "gitlab.com/kirafan/sparkle/server/internal/domain/value/present"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/persistence"
)

func Test_presentUsecase_GetTutorialPresents(t *testing.T) {
	presentRepository := persistence.NewPresentRepositoryImpl(db)
	presentUsecase := NewPresentUsecase(presentRepository, logRepo)

	tests := []struct {
		name         string
		err          error
		expectIndex0 model_present.Present
	}{
		{
			name: "GetTutorialPresents success",
			err:  nil,
			expectIndex0: model_present.Present{
				PresentId:         1,
				DeadlineFlag:      false,
				Title:             "新規プレイヤー応援キャンペーン！",
				Message:           "スタミナ回復アイテム　大 × 20プレゼント！",
				ObjectId:          1002,
				Amount:            20,
				Options:           nil,
				Source:            0,
				Type:              value_present.PresentTypeItem,
				PresentInsertType: value_present.PresentInsertTypeTutorial,
			},
		},
	}

	opts := []cmp.Option{
		cmpopts.IgnoreFields(model_present.Present{}, "CreatedAt"),
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := presentUsecase.GetTutorialPresents()
			if err != nil {
				t.Errorf("presentUsecase.GetTutorialPresents() error = %v, wantErr nil", err)
				return
			}
			if cmp.Equal(&tt.expectIndex0, got[0], opts...) != true {
				t.Errorf("presentUsecase.GetTutorialPresents() Diff = %+v", cmp.Diff(tt.expectIndex0, got[0]))
				return
			}
		})
	}
}
