package usecase

import (
	model_login_bonus "gitlab.com/kirafan/sparkle/server/internal/domain/model/login_bonus"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
)

type LoginBonusUsecase interface {
	GetAllLoginBonuses() ([]*model_login_bonus.LoginBonus, error)
}

type loginBonusUsecase struct {
	rp     repository.LoginBonusRepository
	logger repository.LoggerRepository
}

func NewLoginBonusUsecase(rp repository.LoginBonusRepository, logger repository.LoggerRepository) LoginBonusUsecase {
	return &loginBonusUsecase{rp, logger}
}

func (uc *loginBonusUsecase) GetAllLoginBonuses() ([]*model_login_bonus.LoginBonus, error) {
	loginBonuses, err := uc.rp.FindAvailableLoginBonuses()
	if err != nil {
		return nil, err
	}
	return loginBonuses, nil
}
