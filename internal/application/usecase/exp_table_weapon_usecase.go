package usecase

import (
	model_exp_table "gitlab.com/kirafan/sparkle/server/internal/domain/model/exp_table"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	value_exp "gitlab.com/kirafan/sparkle/server/internal/domain/value/exp"
	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
)

type ExpTableWeaponUsecase interface {
	GetNextExpTableWeapon(currentExp value_exp.WeaponExp, weaponType uint8) (*model_exp_table.ExpTableWeapon, error)
	GetRequiredCoinsForUpgrade(currentLevel uint8) (value_user.Gold, error)
}

type expTableWeaponUsecase struct {
	rp     repository.ExpTableWeaponRepository
	logger repository.LoggerRepository
}

func NewExpTableWeaponUsecase(rp repository.ExpTableWeaponRepository, logger repository.LoggerRepository) ExpTableWeaponUsecase {
	return &expTableWeaponUsecase{rp, logger}
}

func (uc *expTableWeaponUsecase) GetNextExpTableWeapon(currentExp value_exp.WeaponExp, weaponType uint8) (*model_exp_table.ExpTableWeapon, error) {
	criteria := map[string]interface{}{
		"total_exp > ?":   currentExp,
		"weapon_type = ?": weaponType,
	}
	expTableWeapon, err := uc.rp.FindExpTableWeapon(nil, criteria)
	if err != nil {
		return nil, err
	}
	return expTableWeapon, nil
}

func (uc *expTableWeaponUsecase) GetRequiredCoinsForUpgrade(currentLevel uint8) (value_user.Gold, error) {
	coins, err := uc.rp.GetRequiredCoinsForUpgrade(currentLevel)
	if err != nil {
		return 0, err
	}
	golds := value_user.NewGold(uint64(coins))
	return golds, nil
}
