package schema_weapon

import (
	model_user "gitlab.com/kirafan/sparkle/server/internal/domain/model/user"
	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
	"gitlab.com/kirafan/sparkle/server/pkg/upgrade"
)

type UpgradeWeaponResponseSchema struct {
	ManagedWeapon model_user.ManagedWeapon
	ItemSummary   []model_user.ItemSummary
	UpgradeResult upgrade.WeaponUpgradeResult
	Gold          value_user.Gold
}
