package schema_weapon

import (
	model_user "gitlab.com/kirafan/sparkle/server/internal/domain/model/user"
)

type ReceiveWeaponResponseSchema struct {
	NewWeapons  []model_user.ManagedWeapon
	WeaponLimit uint16
}
