package schema_weapon

type ConsumeItem struct {
	ItemId int32
	Count  uint16
}
