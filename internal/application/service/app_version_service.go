package service

import (
	"gitlab.com/kirafan/sparkle/server/internal/application/usecase"
	model_version "gitlab.com/kirafan/sparkle/server/internal/domain/model/version"
	value_version "gitlab.com/kirafan/sparkle/server/internal/domain/value/version"
)

type AppVersionService interface {
	GetAppVersion(platform value_version.Platform, version value_version.Version) (*model_version.Version, error)
}

func NewAppVersionService(
	vu usecase.VersionUsecase,
) AppVersionService {
	return &appVersionService{vu}
}

type appVersionService struct {
	vu usecase.VersionUsecase
}

func (s *appVersionService) GetAppVersion(platform value_version.Platform, version value_version.Version) (*model_version.Version, error) {
	versionInfo, err := s.vu.FindByPlatformAndVersion(platform, version)
	return versionInfo, err
}
