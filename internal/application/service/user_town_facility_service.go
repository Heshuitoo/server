package service

import (
	"errors"

	"gitlab.com/kirafan/sparkle/server/internal/application/usecase"
	model_user "gitlab.com/kirafan/sparkle/server/internal/domain/model/user"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	value_town_facility "gitlab.com/kirafan/sparkle/server/internal/domain/value/town_facility"
)

type UserTownFacilityService interface {
	SaleTownFacility(
		internalUserId uint,
		managedTownFacilityId uint,
	) (*model_user.User, error)
	BuyTownFacility(
		internalUserId uint,
		actionTime uint64,
		buildTime uint64,
		facilityId uint32,
		nextLevel uint8,
		openState int64,
		buildPointIndex int64,
	) (*model_user.User, uint, error)
	LevelUpTownFacility(
		internalUserId uint,
		managedTownFacilityId uint,
		nextLevel uint8,
		actionTime uint64,
		openState int64,
	) (*model_user.User, error)
	OpenUpTownFacility(
		internalUserId uint,
		managedTownFacilityId uint,
		nextLevel uint8,
		buildTime uint64,
		openState int64,
	) (*model_user.User, error)
	SetTownFacilityBuildPoint(
		internalUserId uint,
		managedTownFacilityId uint,
		buildPointIndex int32,
		buildTime uint64,
		openState int64,
	) (*model_user.User, error)
	AddPlayerTownFacilityLimit(
		internalUserId uint,
	) (*model_user.User, error)
	GemLevelUpPlayerTownFacility(
		internalUserId uint,
		managedTownFacilityId uint,
		actionTime uint64,
		nextLevel uint8,
		openState int64,
		remainingTime uint64,
	) (*model_user.User, error)
	ItemUpPlayerTownFacility(
		internalUserId uint,
		managedTownFacilityId uint,
		actionTime int64,
		amount int64,
		itemNo int64,
	) (*model_user.User, error)
}

type userTownFacilityService struct {
	uu  usecase.UserUsecase
	fu  usecase.TownFacilityUsecase
	flu usecase.LevelTableTownFacilityUsecase
	fiu usecase.ItemTableTownFacilityUsecase
}

func NewUserTownFacilityService(
	uu usecase.UserUsecase,
	fu usecase.TownFacilityUsecase,
	flu usecase.LevelTableTownFacilityUsecase,
	fiu usecase.ItemTableTownFacilityUsecase,
) UserTownFacilityService {
	return &userTownFacilityService{uu, fu, flu, fiu}
}

var ErrUserTownFacilityInvalidManagedFacilityId = errors.New("invalid managed facility id")
var ErrUserTownFacilityInvalidFacilityId = errors.New("invalid facility id")
var ErrUserTownFacilityInvalidLevel = errors.New("invalid level")
var ErrUserTownFacilityNotEnoughGoldOrKirara = errors.New("not enough gold or kirara")
var ErrUserTownFacilityNotEnoughGem = errors.New("not enough gem")

func (s *userTownFacilityService) BuyTownFacility(
	internalUserId uint,
	actionTime uint64,
	buildTime uint64,
	facilityId uint32,
	nextLevel uint8,
	openState int64,
	buildPointIndex int64,
) (*model_user.User, uint, error) {
	user, err := s.uu.GetUserByInternalId(internalUserId, repository.UserRepositoryParam{ManagedFacilities: true})
	if err != nil {
		return nil, 0, err
	}
	// Get facility
	infoFacility, err := s.fu.GetTownFacilityById(facilityId)
	if err != nil {
		return nil, 0, ErrUserTownFacilityInvalidFacilityId
	}
	levelFacility, err := s.flu.GetCurrentLevelTableTownFacility(infoFacility.LevelUpListId, nextLevel)
	if err != nil {
		return nil, 0, ErrUserTownFacilityInvalidLevel
	}
	if consumed := user.ConsumeTownFacilityCosts(levelFacility); !consumed {
		return nil, 0, ErrUserTownFacilityNotEnoughGoldOrKirara
	}

	err = user.AddManagedFacility(
		facilityId,
		int32(buildPointIndex),
		uint8(nextLevel),
		uint64(actionTime),
		uint64(buildTime),
		uint8(openState),
	)
	if err != nil {
		return nil, 0, err
	}
	// Update user
	user, err = s.uu.UpdateUser(user, repository.UserRepositoryParam{ManagedFacilities: true})
	if err != nil {
		return nil, 0, err
	}
	// NOTE: IDK why but the client wants the added facility id(s) as response
	lastManagedFacilityId := user.ManagedFacilities[len(user.ManagedFacilities)-1].ManagedTownFacilityId
	return user, lastManagedFacilityId, nil
}

func (s *userTownFacilityService) LevelUpTownFacility(
	internalUserId uint,
	managedTownFacilityId uint,
	nextLevel uint8,
	actionTime uint64,
	openState int64,
) (*model_user.User, error) {
	newOpenState, err := value_town_facility.NewTownFacilityOpenState(uint8(openState))
	if err != nil {
		return nil, err
	}
	user, err := s.uu.GetUserByInternalId(internalUserId, repository.UserRepositoryParam{ManagedFacilities: true})
	if err != nil {
		return nil, err
	}

	// TODO: Maybe there is a chance to level up without consume costs?
	if newOpenState != value_town_facility.TownFacilityOpenStateOpen {
		// Get nextLevel town facility info
		facility, err := user.GetManagedFacility(uint32(managedTownFacilityId))
		if err != nil {
			return nil, ErrUserTownFacilityInvalidFacilityId
		}
		infoFacility, err := s.fu.GetTownFacilityById(facility.FacilityId)
		if err != nil {
			return nil, ErrUserTownFacilityInvalidManagedFacilityId
		}
		levelFacility, err := s.flu.GetNextLevelTableTownFacility(infoFacility.LevelUpListId, nextLevel)
		if err != nil {
			return nil, ErrUserTownFacilityInvalidLevel
		}
		if consumed := user.ConsumeTownFacilityCosts(levelFacility); !consumed {
			return nil, ErrUserTownFacilityNotEnoughGoldOrKirara
		}
	}

	err = user.LevelUpManagedFacility(
		uint32(managedTownFacilityId),
		uint8(nextLevel),
		actionTime,
		newOpenState,
	)
	if err != nil {
		return nil, err
	}

	// Update user
	user, err = s.uu.UpdateUser(user, repository.UserRepositoryParam{ManagedFacilities: true})
	if err != nil {
		return nil, err
	}
	return user, nil
}

func (s *userTownFacilityService) OpenUpTownFacility(
	internalUserId uint,
	managedTownFacilityId uint,
	nextLevel uint8,
	buildTime uint64,
	openState int64,
) (*model_user.User, error) {
	newOpenState, err := value_town_facility.NewTownFacilityOpenState(uint8(openState))
	if err != nil {
		return nil, err
	}
	user, err := s.uu.GetUserByInternalId(internalUserId, repository.UserRepositoryParam{ManagedFacilities: true})
	if err != nil {
		return nil, err
	}

	// Get nextLevel town facility info
	facility, err := user.GetManagedFacility(uint32(managedTownFacilityId))
	if err != nil {
		return nil, ErrUserTownFacilityInvalidFacilityId
	}
	infoFacility, err := s.fu.GetTownFacilityById(facility.FacilityId)
	if err != nil {
		return nil, ErrUserTownFacilityInvalidManagedFacilityId
	}
	levelFacility, err := s.flu.GetCurrentLevelTableTownFacility(infoFacility.LevelUpListId, nextLevel)
	if err != nil {
		return nil, ErrUserTownFacilityInvalidLevel
	}
	if consumed := user.ConsumeTownFacilityCosts(levelFacility); !consumed {
		return nil, ErrUserTownFacilityNotEnoughGoldOrKirara
	}

	err = user.OpenUpManagedFacility(
		uint32(managedTownFacilityId),
		uint8(nextLevel),
		buildTime,
		newOpenState,
	)
	if err != nil {
		return nil, err
	}

	// Update user
	user, err = s.uu.UpdateUser(user, repository.UserRepositoryParam{ManagedFacilities: true})
	if err != nil {
		return nil, err
	}
	return user, nil
}

func (s *userTownFacilityService) SetTownFacilityBuildPoint(
	internalUserId uint,
	managedTownFacilityId uint,
	buildPointIndex int32,
	buildTime uint64,
	openState int64,
) (*model_user.User, error) {
	newOpenState, err := value_town_facility.NewTownFacilityOpenState(uint8(openState))
	if err != nil {
		return nil, err
	}
	newBuildIndex, err := value_town_facility.NewBuildPointIndex(buildPointIndex)
	if err != nil {
		return nil, err
	}
	user, err := s.uu.GetUserByInternalId(internalUserId, repository.UserRepositoryParam{ManagedFacilities: true})
	if err != nil {
		return nil, err
	}
	err = user.MoveManagedTownFacility(
		uint32(managedTownFacilityId),
		newBuildIndex,
		buildTime,
		newOpenState,
	)
	if err != nil {
		return nil, err
	}

	// Update user
	user, err = s.uu.UpdateUser(user, repository.UserRepositoryParam{ManagedFacilities: true})
	if err != nil {
		return nil, err
	}
	return user, nil
}

func (s *userTownFacilityService) SaleTownFacility(
	internalUserId uint,
	managedTownFacilityId uint,
) (*model_user.User, error) {
	user, err := s.uu.GetUserByInternalId(internalUserId, repository.UserRepositoryParam{ManagedFacilities: true})
	if err != nil {
		return nil, err
	}

	managedFacility, err := user.GetManagedFacility(uint32(managedTownFacilityId))
	if err != nil {
		return nil, ErrUserTownFacilityInvalidFacilityId
	}
	infoFacility, err := s.fu.GetTownFacilityById(managedFacility.FacilityId)
	if err != nil {
		return nil, ErrUserTownFacilityInvalidManagedFacilityId
	}
	levelFacility, err := s.flu.GetCurrentLevelTableTownFacility(infoFacility.LevelUpListId, managedFacility.Level)
	if err != nil {
		return nil, ErrUserTownFacilityInvalidLevel
	}

	user.AddGold(uint64(levelFacility.GoldAmountSell))
	user.AddKirara(levelFacility.KiraraPointAmountSell)
	err = user.RemoveManagedFacility(uint32(managedTownFacilityId))
	if err != nil {
		return nil, err
	}

	// Update user
	user, err = s.uu.UpdateUser(user, repository.UserRepositoryParam{ManagedFacilities: true})
	if err != nil {
		return nil, err
	}
	return user, nil
}

func (s *userTownFacilityService) AddPlayerTownFacilityLimit(
	internalUserId uint,
) (*model_user.User, error) {
	user, err := s.uu.GetUserByInternalId(internalUserId, repository.UserRepositoryParam{})
	if err != nil {
		return nil, err
	}

	// TODO: Move this static value to config
	if consumed := user.ConsumeGem(2); !consumed {
		return nil, ErrUserTownFacilityNotEnoughGem
	}
	user.ExtendManagedFacilityLimit()

	// Update user
	user, err = s.uu.UpdateUser(user, repository.UserRepositoryParam{})
	if err != nil {
		return nil, err
	}
	return user, nil
}

func calculateRequiredGemsForLevelUp(remainingMilliseconds uint64) uint32 {
	remainMinutes := remainingMilliseconds / 60000
	requiredGems := (remainMinutes / 30) * 10
	if remainMinutes%30 != 0 {
		requiredGems += 10
	}
	return uint32(requiredGems)
}

func (s *userTownFacilityService) GemLevelUpPlayerTownFacility(
	internalUserId uint,
	managedTownFacilityId uint,
	actionTime uint64,
	nextLevel uint8,
	openState int64,
	remainingTime uint64,
) (*model_user.User, error) {
	newOpenState, err := value_town_facility.NewTownFacilityOpenState(uint8(openState))
	if err != nil {
		return nil, err
	}
	user, err := s.uu.GetUserByInternalId(internalUserId, repository.UserRepositoryParam{ManagedFacilities: true})
	if err != nil {
		return nil, err
	}

	requiredGems := calculateRequiredGemsForLevelUp(remainingTime)
	if consumed := user.ConsumeGem(requiredGems); !consumed {
		return nil, ErrUserTownFacilityNotEnoughGem
	}

	err = user.LevelUpManagedFacility(uint32(managedTownFacilityId), nextLevel, actionTime, newOpenState)
	if err != nil {
		return nil, err
	}

	// Update user
	user, err = s.uu.UpdateUser(user, repository.UserRepositoryParam{ManagedFacilities: true})
	if err != nil {
		return nil, err
	}
	return user, nil
}

func (s *userTownFacilityService) ItemUpPlayerTownFacility(
	internalUserId uint,
	managedTownFacilityId uint,
	actionTime int64,
	amount int64,
	itemNo int64,
) (*model_user.User, error) {
	user, err := s.uu.GetUserByInternalId(internalUserId, repository.UserRepositoryParam{ManagedFacilities: true, ItemSummary: true})
	if err != nil {
		return nil, err
	}
	err = user.UpdateManagedFacilityActionTime(uint32(managedTownFacilityId), uint64(actionTime))
	if err != nil {
		return nil, err
	}

	itemTableTownFacilities, err := s.fiu.GetItemTableTownFacility(uint32(itemNo))
	if err != nil {
		return nil, err
	}

	// loop for the amount times
	for i := int64(0); i < amount; i++ {
		for _, itemTableTownFacility := range itemTableTownFacilities {
			switch itemTableTownFacility.Category {
			case value_town_facility.ItemTableTownFacilityCategoryCoin:
				user.AddGold(uint64(itemTableTownFacility.ObjectCount))
			case value_town_facility.ItemTableTownFacilityCategoryItem:
				user.AddItem(uint32(itemTableTownFacility.ObjectID), itemTableTownFacility.ObjectCount)
			case value_town_facility.ItemTableTownFacilityCategoryKirara:
				user.AddKirara(itemTableTownFacility.ObjectCount)
			case value_town_facility.ItemTableTownFacilityCategoryStamina:
				// user = user.AddStamina(uint32(itemTableTownFacility.ObjectCount))
				return nil, errors.New("unimplemented item table town facility category")
			case value_town_facility.ItemTableTownFacilityCategoryFriendship:
				// user = user.AddFriendship(uint32(itemTableTownFacility.ObjectCount))
				return nil, errors.New("unimplemented item table town facility category")
			case value_town_facility.ItemTableTownFacilityCategoryLimitedGem:
				user.AddLimitedGem(uint32(itemTableTownFacility.ObjectCount))
			}
		}
	}

	// Update user
	user, err = s.uu.UpdateUser(user, repository.UserRepositoryParam{ManagedFacilities: true, ItemSummary: true})
	if err != nil {
		return nil, err
	}
	return user, nil
}
