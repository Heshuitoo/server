package service

import (
	"github.com/google/uuid"
	schema_player_import "gitlab.com/kirafan/sparkle/server/internal/application/schemas/import_player"
	"gitlab.com/kirafan/sparkle/server/internal/application/usecase"
	model_mission "gitlab.com/kirafan/sparkle/server/internal/domain/model/mission"
	model_present "gitlab.com/kirafan/sparkle/server/internal/domain/model/present"
	model_user "gitlab.com/kirafan/sparkle/server/internal/domain/model/user"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
	"gitlab.com/kirafan/sparkle/server/pkg/calc"
)

type PlayerImportService interface {
	ImportPlayerOffline(data schema_player_import.ImportPlayerOfflineSchema) (string, string, error)
	ImportPlayerOnline(data schema_player_import.ImportPlayerOnlineSchema) (string, string, error)
}

type playerImportService struct {
	uu usecase.UserUsecase
	lu usecase.LoginBonusUsecase
	nu usecase.NamedTypeUsecase
}

func NewPlayerImportService(uu usecase.UserUsecase, lu usecase.LoginBonusUsecase, nu usecase.NamedTypeUsecase) PlayerImportService {
	return &playerImportService{uu, lu, nu}
}

func (s *playerImportService) ImportPlayerOffline(data schema_player_import.ImportPlayerOfflineSchema) (string, string, error) {
	// Create new user as same as normal flow
	loginBonuses, err := s.lu.GetAllLoginBonuses()
	if err != nil {
		return "", "", err
	}
	user, err := s.uu.CreateUser(
		uuid.New().String(),
		data.Name,
		[]*model_present.Present{},
		[]*model_mission.Mission{},
		loginBonuses,
	)
	if err != nil {
		return "", "", err
	}

	// Inject uploaded info
	calc.Copy(&user, &data)
	user.CurrentAchievementId = uint64(data.AchievementID)
	user.AdvIds = data.AdvIds
	for i := range user.AdvIds {
		user.AdvIds[i].UserId = user.Id
	}
	user.ManagedNamedTypes = data.ManagedNamedTypes
	for i := range user.ManagedNamedTypes {
		user.ManagedNamedTypes[i].UserId = user.Id
		namedType, err := s.nu.GetNamedTypeById(uint(user.ManagedNamedTypes[i].NamedType))
		if err != nil {
			return "", "", err
		}
		user.ManagedNamedTypes[i].TitleType = namedType.TitleType
	}
	user.ManagedCharacters = data.ManagedCharacters
	for i := range user.ManagedCharacters {
		user.ManagedCharacters[i].PlayerId = user.Id
	}
	user.StepCode = value_user.StepCodeAdvCreaDone

	// Inject transfer code
	movePass := calc.GetSparkleRandomString()
	moveCode, err := user.UpdateMoveCode(movePass)
	if err != nil {
		return "", "", err
	}
	// Inject random town data as stub
	user.ManagedTowns = []model_user.ManagedTown{
		{
			UserId:   user.Id,
			GridData: "eyJtX1ZlciI6MSwibV9EYXRhVHlwZSI6MCwibV9EYXRhIjoiZXlKdFgxUmhZbXhsSWpwYmV5SnRYMDFoYm1GblpVbEVJam81TlN3aWJWOUNkV2xzWkZCdmFXNTBTVzVrWlhnaU9qQXNJbTFmVDJKcVNVUWlPakFzSW0xZlRIWWlPakVzSW0xZlNYTlBjR1Z1SWpwMGNuVmxMQ0p0WDBKMWFXeGthVzVuVFhNaU9qRTJPRGd6TlRVeE9EUTRNamNzSW0xZlFXTjBhVzl1VkdsdFpTSTZNVFk0T0RNMU5URTRORGd5TjMwc2V5SnRYMDFoYm1GblpVbEVJam81Tml3aWJWOUNkV2xzWkZCdmFXNTBTVzVrWlhnaU9qSTJPRFF6TmpZMU5pd2liVjlQWW1wSlJDSTZNVEl3TUN3aWJWOU1kaUk2TVN3aWJWOUpjMDl3Wlc0aU9uUnlkV1VzSW0xZlFuVnBiR1JwYm1kTmN5STZNVFk0T0RNMU5URTRORGd5Tnl3aWJWOUJZM1JwYjI1VWFXMWxJam94TmpnNE16VTFNVGcwT0RJM2ZTeDdJbTFmVFdGdVlXZGxTVVFpT2prM0xDSnRYMEoxYVd4a1VHOXBiblJKYm1SbGVDSTZNalk0TkRNMk5qVTRMQ0p0WDA5aWFrbEVJam94TWpBeUxDSnRYMHgySWpveExDSnRYMGx6VDNCbGJpSTZkSEoxWlN3aWJWOUNkV2xzWkdsdVowMXpJam94TmpnNE16VTFNVGcwT0RJM0xDSnRYMEZqZEdsdmJsUnBiV1VpT2pFMk9EZ3pOVFV4T0RRNE1qZDlMSHNpYlY5TllXNWhaMlZKUkNJNk9UZ3NJbTFmUW5WcGJHUlFiMmx1ZEVsdVpHVjRJam95TmpnME16WTJOVGtzSW0xZlQySnFTVVFpT2pFeU1ETXNJbTFmVEhZaU9qRXNJbTFmU1hOUGNHVnVJanAwY25WbExDSnRYMEoxYVd4a2FXNW5UWE1pT2pFMk9EZ3pOVFV4T0RRNE1qY3NJbTFmUVdOMGFXOXVWR2x0WlNJNk1UWTRPRE0xTlRFNE5EZ3lOMzBzZXlKdFgwMWhibUZuWlVsRUlqbzVPU3dpYlY5Q2RXbHNaRkJ2YVc1MFNXNWtaWGdpT2pJMk9EUXpOalkyTUN3aWJWOVBZbXBKUkNJNk1USXdOQ3dpYlY5TWRpSTZNU3dpYlY5SmMwOXdaVzRpT25SeWRXVXNJbTFmUW5WcGJHUnBibWROY3lJNk1UWTRPRE0xTlRFNE5EZ3lOeXdpYlY5QlkzUnBiMjVVYVcxbElqb3hOamc0TXpVMU1UZzBPREkzZlN4N0ltMWZUV0Z1WVdkbFNVUWlPakV3TUN3aWJWOUNkV2xzWkZCdmFXNTBTVzVrWlhnaU9qSTJPRFF6TmpZMk1Td2liVjlQWW1wSlJDSTZNVEl3TlN3aWJWOU1kaUk2TVN3aWJWOUpjMDl3Wlc0aU9uUnlkV1VzSW0xZlFuVnBiR1JwYm1kTmN5STZNVFk0T0RNMU5URTRORGd5Tnl3aWJWOUJZM1JwYjI1VWFXMWxJam94TmpnNE16VTFNVGcwT0RJM2ZTeDdJbTFmVFdGdVlXZGxTVVFpT2pFeU5Td2liVjlDZFdsc1pGQnZhVzUwU1c1a1pYZ2lPaklzSW0xZlQySnFTVVFpT2pFc0ltMWZUSFlpT2pFc0ltMWZTWE5QY0dWdUlqcDBjblZsTENKdFgwSjFhV3hrYVc1blRYTWlPakUyT0Rnek5UY3lNak13TXpjc0ltMWZRV04wYVc5dVZHbHRaU0k2TVRZNE9ETTFOekl5TXpBek4zMHNleUp0WDAxaGJtRm5aVWxFSWpveE1qWXNJbTFmUW5WcGJHUlFiMmx1ZEVsdVpHVjRJam8xTXpZNE56QTVNVFFzSW0xZlQySnFTVVFpT2pFeE1ESXdNQ3dpYlY5TWRpSTZNU3dpYlY5SmMwOXdaVzRpT25SeWRXVXNJbTFmUW5WcGJHUnBibWROY3lJNk1UWTRPRE0xTnpJeU16QXpOeXdpYlY5QlkzUnBiMjVVYVcxbElqb3hOamc0TXpVM01qSXpNRE0zZlN4N0ltMWZUV0Z1WVdkbFNVUWlPakV5Tnl3aWJWOUNkV2xzWkZCdmFXNTBTVzVrWlhnaU9qRXdOek01TXpnME16UXNJbTFmVDJKcVNVUWlPakV6TURBd01Dd2liVjlNZGlJNk1Td2liVjlKYzA5d1pXNGlPblJ5ZFdVc0ltMWZRblZwYkdScGJtZE5jeUk2TVRZNE9ETTFOekl6TVRReE55d2liVjlCWTNScGIyNVVhVzFsSWpveE5qZzRNelUzTWpNMU1EWTVmU3g3SW0xZlRXRnVZV2RsU1VRaU9qRXlPQ3dpYlY5Q2RXbHNaRkJ2YVc1MFNXNWtaWGdpT2pFd056TTROekk0T1Rnc0ltMWZUMkpxU1VRaU9qRXpNREV3TUN3aWJWOU1kaUk2TVN3aWJWOUpjMDl3Wlc0aU9uUnlkV1VzSW0xZlFuVnBiR1JwYm1kTmN5STZNVFk0T0RNMU56SXpPVFUyTnl3aWJWOUJZM1JwYjI1VWFXMWxJam94TmpnNE16VTNNalF6TWpjNWZWMTkifQ==",
		},
	}

	// Save the user for next step
	user, err = s.uu.UpdateUser(user, repository.UserRepositoryParam{
		ManagedCharacters:        true,
		ManagedNamedTypes:        true,
		FavoriteMembers:          true,
		ManagedFieldPartyMembers: true,
		ManagedBattleParties:     true,
		AdvIds:                   true,
	})
	if err != nil {
		return "", "", err
	}

	// Finalize the user as same as normal flow
	user, err = s.uu.SetupUser(user.Id)
	if err != nil {
		return "", "", err
	}
	// Change the step code and make the import complete
	user.UpdateStepCode(value_user.StepCodeTutorialDone)
	if _, err := s.uu.UpdateUser(user, repository.UserRepositoryParam{}); err != nil {
		return "", "", err
	}

	return moveCode, movePass, nil
}

func (s *playerImportService) ImportPlayerOnline(data schema_player_import.ImportPlayerOnlineSchema) (string, string, error) {
	// STUB
	return "MoveCode", "MovePass", nil
}
