package service

import (
	"errors"
	"time"

	"gitlab.com/kirafan/sparkle/server/internal/application/usecase"
	model_friend "gitlab.com/kirafan/sparkle/server/internal/domain/model/friend"
	"gitlab.com/kirafan/sparkle/server/internal/domain/value"
	value_character "gitlab.com/kirafan/sparkle/server/internal/domain/value/character"
	value_friend "gitlab.com/kirafan/sparkle/server/internal/domain/value/friend"
)

type PlayerFriendService interface {
	// Add the friend relationship
	AcceptPlayerFriend(internalUserId uint, managedFriendId uint) error
	// Remove the friend relationship
	TerminatePlayerFriend(internalUserId uint, managedFriendId uint) error
	// Cancel a friend request to a player
	CancelPlayerFriend(internalUserId uint, managedFriendId uint) error
	// Deny a friend request from a player
	RefusePlayerFriend(internalUserId uint, managedFriendId uint) error
	// Send a friend request to a player
	ProposePlayerFriend(internalUserId uint, targetInternalUserId uint) (uint, error)
	// Find a player info by the player code
	SearchPlayerFriend(internalUserId uint, myCode string) (model_friend.Friend, error)
	// Get list of friends
	GetAllPlayerFriend(
		internalUserId uint,
		getType value_friend.FriendGetType,
		managedBattleParty uint,
		ignoreSupport value.BoolLikeUInt8,
		reload value.BoolLikeUInt8,
	) ([]model_friend.Friend, []model_friend.Friend, error)
}

func NewPlayerFriendService(
	uu usecase.UserUsecase,
) PlayerFriendService {
	return &playerFriendService{uu}
}

type playerFriendService struct {
	uu usecase.UserUsecase
}

func (s *playerFriendService) AcceptPlayerFriend(internalId uint, managedFriendId uint) error {
	return errors.New("this endpoint is not implemented yet")
}

func (s *playerFriendService) TerminatePlayerFriend(internalId uint, managedFriendId uint) error {
	return errors.New("this endpoint is not implemented yet")
}

func (s *playerFriendService) CancelPlayerFriend(internalId uint, managedFriendId uint) error {
	return errors.New("this endpoint is not implemented yet")
}

func (s *playerFriendService) RefusePlayerFriend(internalId uint, managedFriendId uint) error {
	return errors.New("this endpoint is not implemented yet")
}

func (s *playerFriendService) ProposePlayerFriend(internalUserId uint, targetInternalUserId uint) (uint, error) {
	return 0, errors.New("this endpoint is not implemented yet")
}

func (s *playerFriendService) SearchPlayerFriend(internalUserId uint, myCode string) (model_friend.Friend, error) {
	return model_friend.Friend{}, errors.New("this endpoint is not implemented yet")
}

func (s *playerFriendService) GetAllPlayerFriend(
	internalUserId uint,
	getType value_friend.FriendGetType,
	managedBattleParty uint,
	ignoreSupport value.BoolLikeUInt8,
	reload value.BoolLikeUInt8,
) ([]model_friend.Friend, []model_friend.Friend, error) {
	// STUB
	friends := []model_friend.Friend{
		{
			ManagedFriendId:      66,
			PlayerId:             66,
			State:                value_friend.FriendStateFriend,
			Direction:            value_friend.FriendDirectionFriend,
			Name:                 "星いろどりいし",
			MyCode:               "CREA",
			Comment:              "<size=35>す、す、すごかったです！\n次回もがんばりますっ！</size>",
			CurrentAchievementId: 133320201,
			Level:                66,
			LastLoginAt:          time.Date(2023, 2, 28, 17, 0, 0, 0, time.UTC),
			TotalExp:             0,
			SupportLimit:         8,
			NamedTypes:           nil,
			SupportName:          "きらきらふぁんたじあ",
			SupportCharacters: []model_friend.FriendSupportCharacter{
				{
					ManagedCharacterId: 1,
					CharacterId:        value_character.CharacterId(32022011),
					Level:              100,
					Exp:                0,
					LevelBreak:         4,
					SkillLevel1:        35,
					SkillLevel2:        25,
					SkillLevel3:        25,
					WeaponId:           21200,
					WeaponLevel:        20,
					WeaponSkillLevel:   30,
					WeaponSkillExp:     0,
					NamedLevel:         5,
					NamedExp:           0,
					DuplicatedCount:    5,
					ArousalLevel:       5,
					AbilityBoardId:     -1,
					EquipItemIds:       make([]uint32, 0),
				},
			},
			FirstFavoriteMember: model_friend.FriendSupportCharacter{
				ManagedCharacterId: 1,
				CharacterId:        value_character.CharacterId(32022011),
				Level:              100,
				Exp:                0,
				LevelBreak:         4,
				SkillLevel1:        35,
				SkillLevel2:        25,
				SkillLevel3:        25,
				WeaponId:           21200,
				WeaponLevel:        20,
				WeaponSkillLevel:   30,
				WeaponSkillExp:     0,
				NamedLevel:         5,
				NamedExp:           0,
				DuplicatedCount:    5,
				ArousalLevel:       5,
				AbilityBoardId:     -1,
				EquipItemIds:       make([]uint32, 0),
			},
		},
	}
	guests := []model_friend.Friend{
		{
			ManagedFriendId:      66,
			PlayerId:             66,
			State:                value_friend.FriendStateGuest,
			Direction:            value_friend.FriendDirectionGuest,
			Name:                 "惜しいろどりいし",
			MyCode:               "CREA",
			Comment:              "<size=35>次回もがんばります!\nまたいつでも来てくださいね!</size>",
			CurrentAchievementId: 131320201,
			Level:                66,
			LastLoginAt:          time.Date(2023, 2, 28, 17, 0, 0, 0, time.UTC),
			TotalExp:             0,
			SupportLimit:         8,
			NamedTypes:           nil,
			SupportName:          "きらきらふぁんたじあ",
			SupportCharacters: []model_friend.FriendSupportCharacter{
				{
					ManagedCharacterId: 1,
					CharacterId:        value_character.CharacterId(32022010),
					Level:              100,
					Exp:                0,
					LevelBreak:         4,
					SkillLevel1:        25,
					SkillLevel2:        15,
					SkillLevel3:        15,
					WeaponId:           21200,
					WeaponLevel:        20,
					WeaponSkillLevel:   30,
					WeaponSkillExp:     0,
					NamedLevel:         5,
					NamedExp:           0,
					DuplicatedCount:    5,
					ArousalLevel:       5,
					AbilityBoardId:     -1,
					EquipItemIds:       make([]uint32, 0),
				},
			},
			FirstFavoriteMember: model_friend.FriendSupportCharacter{
				ManagedCharacterId: 1,
				CharacterId:        value_character.CharacterId(32022010),
				Level:              100,
				Exp:                0,
				LevelBreak:         4,
				SkillLevel1:        25,
				SkillLevel2:        15,
				SkillLevel3:        15,
				WeaponId:           21200,
				WeaponLevel:        20,
				WeaponSkillLevel:   30,
				WeaponSkillExp:     0,
				NamedLevel:         5,
				NamedExp:           0,
				DuplicatedCount:    5,
				ArousalLevel:       5,
				AbilityBoardId:     -1,
				EquipItemIds:       make([]uint32, 0),
			},
		},
	}
	return friends, guests, nil
}
