package service

import (
	"errors"

	schema_room_object "gitlab.com/kirafan/sparkle/server/internal/application/schemas/room_object"
	"gitlab.com/kirafan/sparkle/server/internal/application/usecase"
	model_user "gitlab.com/kirafan/sparkle/server/internal/domain/model/user"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	"gitlab.com/kirafan/sparkle/server/pkg/calc"
	"gitlab.com/kirafan/sparkle/server/pkg/parser"
)

type UserRoomObjectService interface {
	SalePlayerRoomObject(
		internalUserId uint,
		managedRoomObjectId uint,
	) (*model_user.User, error)
	BuySetPlayerRoomObject(
		internalUserId uint,
		param schema_room_object.BuySetPlayerRoomObject,
	) (*model_user.User, *string, error)
	AddPlayerRoomObjectLimit(
		internalUserId uint,
	) (*model_user.User, error)
}

var ErrUserRoomObjectNotEnoughGem = errors.New("user room facility: not enough gem")
var ErrUserRoomObjectNotEnoughCoin = errors.New("user room facility: not enough coin")
var ErrUserRoomObjectRoomNotFound = errors.New("user room facility: room not found")

type userRoomObjectService struct {
	uu usecase.UserUsecase
	ru usecase.RoomObjectUsecase
}

func NewUserRoomObjectService(
	uu usecase.UserUsecase,
	ru usecase.RoomObjectUsecase,
) UserRoomObjectService {
	return &userRoomObjectService{uu, ru}
}

func (s *userRoomObjectService) SalePlayerRoomObject(
	internalUserId uint,
	managedRoomObjectId uint,
) (*model_user.User, error) {
	user, err := s.uu.GetUserByInternalId(internalUserId, repository.UserRepositoryParam{ManagedRoomObjects: true})
	if err != nil {
		return nil, err
	}

	managedRoomObject, err := user.GetRoomObject(managedRoomObjectId)
	if err != nil {
		return nil, err
	}
	roomObject, err := s.ru.GetRoomObject(managedRoomObject.RoomObjectId)
	if err != nil {
		return nil, err
	}

	// Add coin
	user.AddGold(uint64(roomObject.SaleAmount))
	user.RemoveRoomObject(int(managedRoomObject.RoomObjectId), uint32(managedRoomObject.ManagedRoomObjectId))

	// Update user
	user, err = s.uu.UpdateUser(user, repository.UserRepositoryParam{ManagedRoomObjects: true})
	if err != nil {
		return nil, err
	}
	return user, nil
}

func (s *userRoomObjectService) buyPlayerRoomObject(
	internalUserId uint,
	param schema_room_object.BuySetPlayerRoomObject,
) ([]int, error) {
	user, err := s.uu.GetUserByInternalId(
		internalUserId,
		repository.UserRepositoryParam{ManagedRoomObjects: true},
	)
	if err != nil {
		return nil, err
	}

	roomObject, err := s.ru.GetRoomObject(param.RoomObjectId)
	if err != nil {
		return nil, err
	}

	newManagedRoomObjectIds := make([]int, int(param.BuyAmount))
	for i := 0; i < int(param.BuyAmount); i++ {
		consumed := user.ConsumeGold(uint64(roomObject.BuyAmount))
		if !consumed {
			return nil, ErrUserRoomObjectNotEnoughCoin
		}
		user.AddRoomObject(param.RoomObjectId)
		user, err = s.uu.UpdateUser(user, repository.UserRepositoryParam{ManagedRoomObjects: true})
		if err != nil {
			return nil, err
		}
		newManagedRoomObjectIds[i] = *calc.ToPtr(user.ManagedRoomObjects[len(user.ManagedRoomObjects)-1].ManagedRoomObjectId)
	}
	return newManagedRoomObjectIds, nil
}

func (s *userRoomObjectService) setPlayerRoom(
	internalUserId uint,
	param schema_room_object.BuySetPlayerRoomObject,
	newIds []int,
) (*model_user.User, error) {
	user, err := s.uu.GetUserByInternalId(
		internalUserId,
		repository.UserRepositoryParam{ManagedRooms: true, ManagedRoomObjects: true},
	)
	if err != nil {
		return nil, err
	}

	for i := 0; i < int(param.BuyAmount); i++ {
		for i2 := 0; i2 < len(param.ArrangeData); i2++ {
			if param.ArrangeData[i2].ManagedRoomObjectId == -2 && param.ArrangeData[i2].RoomObjectId == param.RoomObjectId {
				param.ArrangeData[i2].ManagedRoomObjectId = newIds[i]
			}
		}
	}

	if err := user.UpdateRoom(param.ManagedRoomId, param.ArrangeData); err != nil {
		return nil, err
	}

	// Update user
	user, err = s.uu.UpdateUser(user, repository.UserRepositoryParam{ManagedRooms: true, ManagedRoomObjects: true})
	if err != nil {
		return nil, err
	}
	return user, nil
}

func (s *userRoomObjectService) BuySetPlayerRoomObject(
	internalUserId uint,
	param schema_room_object.BuySetPlayerRoomObject,
) (*model_user.User, *string, error) {
	newIds, err := s.buyPlayerRoomObject(internalUserId, param)
	if err != nil {
		return nil, nil, err
	}

	user, err := s.setPlayerRoom(internalUserId, param, newIds)
	if err != nil {
		return nil, nil, err
	}

	newManagedRoomObjectIds := parser.ParseToManagedRoomObjectIdsResponse(newIds)
	return user, &newManagedRoomObjectIds, nil
}

func (s *userRoomObjectService) AddPlayerRoomObjectLimit(
	internalUserId uint,
) (*model_user.User, error) {
	user, err := s.uu.GetUserByInternalId(internalUserId, repository.UserRepositoryParam{})
	if err != nil {
		return nil, err
	}

	// TODO: Move this static value to config
	if consumed := user.ConsumeGem(5); !consumed {
		return nil, ErrUserRoomObjectNotEnoughGem
	}
	user.ExtendManagedRoomObjectLimit()

	// Update user
	user, err = s.uu.UpdateUser(user, repository.UserRepositoryParam{})
	if err != nil {
		return nil, err
	}
	return user, nil
}
