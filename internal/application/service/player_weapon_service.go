package service

import (
	"errors"
	"fmt"

	schema_weapon "gitlab.com/kirafan/sparkle/server/internal/application/schemas/weapon"
	"gitlab.com/kirafan/sparkle/server/internal/application/usecase"
	model_user "gitlab.com/kirafan/sparkle/server/internal/domain/model/user"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	value_exp "gitlab.com/kirafan/sparkle/server/internal/domain/value/exp"
	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
	value_weapon "gitlab.com/kirafan/sparkle/server/internal/domain/value/weapon"
	"gitlab.com/kirafan/sparkle/server/pkg/upgrade"
)

type PlayerWeaponService interface {
	// Consume gems and extend the weapon inventory limit
	AddPlayerWeaponLimit(internalUserId uint) (*model_user.User, error)
	// Consume item and evolute specified weapon
	EvoluteWeapon(internalUserId uint, managedWeaponId value_user.ManagedWeaponId, weaponEvolutionId uint) (*schema_weapon.EvoluteWeaponResponseSchema, error)
	// Consume item and add weapon to the user
	MakeWeapon(internalUserId uint, recipeId uint32) (*model_user.User, error)
	// Find the specified managed character and insert a special weapon for the character
	ReceiveWeapon(internalUserId uint, managedCharacterIds []value_user.ManagedCharacterId) (*schema_weapon.ReceiveWeaponResponseSchema, error)
	// Sale weapon and add golds to the user
	SaleWeapon(internalUserId uint, managedWeaponIds []value_user.ManagedWeaponId) (*model_user.User, error)
	// Consume item and add exps to the weapon
	UpgradeWeapon(internalUserId uint, managedWeaponId value_user.ManagedWeaponId, items []schema_weapon.ConsumeItem) (*schema_weapon.UpgradeWeaponResponseSchema, error)
}

type playerWeaponService struct {
	uu  usecase.UserUsecase
	iu  usecase.ItemUsecase
	cu  usecase.CharacterUsecase
	wu  usecase.WeaponUsecase
	wru usecase.WeaponRecipeUsecase
	wcu usecase.WeaponCharacterTableUsecase
	eu  usecase.ExpTableWeaponUsecase
	evu usecase.EvoTableWeaponUsecase
	ch  upgrade.UpgradeWeaponHandler
}

func NewPlayerWeaponService(
	uu usecase.UserUsecase,
	iu usecase.ItemUsecase,
	cu usecase.CharacterUsecase,
	wu usecase.WeaponUsecase,
	wru usecase.WeaponRecipeUsecase,
	wcu usecase.WeaponCharacterTableUsecase,
	eu usecase.ExpTableWeaponUsecase,
	evu usecase.EvoTableWeaponUsecase,
) PlayerWeaponService {
	ch := upgrade.NewUpgradeWeaponHandler(nil)
	return &playerWeaponService{uu, iu, cu, wu, wru, wcu, eu, evu, ch}
}

func (s *playerWeaponService) AddPlayerWeaponLimit(internalUserId uint) (*model_user.User, error) {
	// Get user
	user, err := s.uu.GetUserByInternalId(internalUserId, repository.UserRepositoryParam{
		ManagedWeapons: true,
	})
	if err != nil {
		return nil, err
	}

	// TODO: Move this static value to config
	if consumed := user.ConsumeGem(5); !consumed {
		return nil, errors.New("not enough gems")
	}
	user.ExtendManagedWeaponLimit()

	// Update user
	user, err = s.uu.UpdateUser(user, repository.UserRepositoryParam{})
	if err != nil {
		return nil, err
	}
	return user, nil
}

func (s *playerWeaponService) EvoluteWeapon(internalUserId uint, managedWeaponId value_user.ManagedWeaponId, weaponEvolutionId uint) (*schema_weapon.EvoluteWeaponResponseSchema, error) {
	// Get user
	user, err := s.uu.GetUserByInternalId(internalUserId, repository.UserRepositoryParam{
		ManagedWeapons: true,
		ItemSummary:    true,
	})
	if err != nil {
		return nil, err
	}
	// Get managed weapon
	managedWeapon, err := user.GetManagedWeapon(managedWeaponId)
	if err != nil {
		return nil, err
	}
	// Get weapon and validate it can evolve
	weapon, err := s.wu.GetWeaponById(managedWeapon.WeaponId)
	if err != nil {
		return nil, err
	}
	if !weapon.CanEvolve() {
		return nil, errors.New("weapon can not evolve anymore")
	}

	// Get evolution recipe
	evolutionRecipe, err := s.evu.GetEvolutionRecipe(managedWeapon.WeaponId)
	if err != nil {
		return nil, err
	}
	// Consume golds
	requiredGolds := uint64(evolutionRecipe.RequiredCoin)
	if consumed := user.ConsumeGold(requiredGolds); !consumed {
		return nil, err
	}
	// Consume items
	for _, itemInfo := range evolutionRecipe.RecipeMaterials {
		if consumed := user.ConsumeItem(uint32(itemInfo.ItemId), uint32(itemInfo.Amount)); !consumed {
			return nil, err
		}
	}
	// Update weapon id
	managedWeapon.WeaponId = evolutionRecipe.DestWeaponId

	// Save the changes
	user, err = s.uu.UpdateUser(user, repository.UserRepositoryParam{
		ManagedWeapons: true,
		ItemSummary:    true,
	})
	if err != nil {
		return nil, err
	}

	// Return response
	resp := schema_weapon.EvoluteWeaponResponseSchema{
		ManagedWeapon: *managedWeapon,
		ItemSummary:   user.ItemSummary,
		Gold:          value_user.Gold(user.Gold),
	}
	return &resp, nil
}

func (s *playerWeaponService) MakeWeapon(internalUserId uint, recipeId uint32) (*model_user.User, error) {
	// Get user
	user, err := s.uu.GetUserByInternalId(internalUserId, repository.UserRepositoryParam{
		ManagedWeapons: true,
		ItemSummary:    true,
	})
	if err != nil {
		return nil, err
	}
	recipe, err := s.wru.GetWeaponRecipeById(recipeId)
	if err != nil {
		return nil, err
	}
	// Consume golds
	if consumed := user.ConsumeGold(uint64(recipe.BuyAmount)); !consumed {
		return nil, err
	}
	// Consume items
	for _, itemInfo := range recipe.RecipeMaterials {
		if consumed := user.ConsumeItem(itemInfo.ItemId, itemInfo.Amount); !consumed {
			return nil, err
		}
	}
	// Add weapon
	err = user.AddWeapon(recipe.WeaponId, false)
	if err != nil {
		return nil, err
	}
	// Save user
	user, err = s.uu.UpdateUser(user, repository.UserRepositoryParam{
		ItemSummary:    true,
		ManagedWeapons: true,
	})
	if err != nil {
		return nil, err
	}
	return user, nil
}

func (s *playerWeaponService) ReceiveWeapon(internalUserId uint, managedCharacterIds []value_user.ManagedCharacterId) (*schema_weapon.ReceiveWeaponResponseSchema, error) {
	// Get user
	user, err := s.uu.GetUserByInternalId(internalUserId, repository.UserRepositoryParam{
		ManagedWeapons:    true,
		ManagedCharacters: true,
	})
	if err != nil {
		return nil, err
	}

	// make a slice of weapon ids for get managed weapons later
	newWeaponIds := make([]value_weapon.WeaponId, len(managedCharacterIds))
	for i, managedCharacterId := range managedCharacterIds {
		// Get managed character
		managedCharacter, err := user.GetManagedCharacter(managedCharacterId)
		if err != nil {
			return nil, err
		}
		if managedCharacter.Level != 100 {
			return nil, errors.New("character level is not 100 : " + fmt.Sprint(managedCharacter.CharacterId))
		}
		// Get weaponID for the character
		weaponId, err := s.wcu.GetWeaponCharacterTableById(managedCharacter.CharacterId)
		if err != nil {
			return nil, err
		}
		newWeaponIds[i] = *weaponId
		if user.HasWeapon(*weaponId) {
			return nil, errors.New("user already has a weapon for the character : " + fmt.Sprint(managedCharacter.CharacterId))
		}
		// Insert weapon
		user.AddWeapon(*weaponId, true)
	}

	user, err = s.uu.UpdateUser(user, repository.UserRepositoryParam{
		ManagedWeapons: true,
	})
	if err != nil {
		return nil, err
	}

	// Create new weapons slice for response
	// NOTE: This endpoint wants "only" new weapons for unknown reason (tedious)
	newManagedWeapons := make([]model_user.ManagedWeapon, len(newWeaponIds))
	for i := range newWeaponIds {
		managedWeapon, err := user.GetManagedWeaponByWeaponId(newWeaponIds[i])
		if err != nil {
			return nil, err
		}
		newManagedWeapons[i] = *managedWeapon
	}
	resp := schema_weapon.ReceiveWeaponResponseSchema{
		NewWeapons:  newManagedWeapons,
		WeaponLimit: user.WeaponLimit,
	}

	return &resp, nil
}

func (s *playerWeaponService) SaleWeapon(internalUserId uint, managedWeaponIds []value_user.ManagedWeaponId) (*model_user.User, error) {
	// Get user
	user, err := s.uu.GetUserByInternalId(internalUserId, repository.UserRepositoryParam{
		ManagedWeapons: true,
	})
	if err != nil {
		return nil, err
	}

	for _, managedWeaponId := range managedWeaponIds {
		// Validate weapon exists
		managedWeapon, err := user.GetManagedWeapon(managedWeaponId)
		if err != nil {
			return nil, err
		}
		// Get weapon
		weapon, err := s.wu.GetWeaponById(managedWeapon.WeaponId)
		if err != nil {
			return nil, err
		}
		// Calculate sale price
		salePrice := weapon.SaleAmount
		// Add golds
		user.AddGold(uint64(salePrice))
		// Remove weapon
		if removed := user.RemoveWeapon(managedWeaponId); !removed {
			return nil, errors.New("failed to remove weapon")
		}
	}

	user, err = s.uu.UpdateUser(user, repository.UserRepositoryParam{
		ManagedWeapons: true,
	})
	if err != nil {
		return nil, err
	}

	return user, nil
}

func (s *playerWeaponService) UpgradeWeapon(internalUserId uint, managedWeaponId value_user.ManagedWeaponId, items []schema_weapon.ConsumeItem) (*schema_weapon.UpgradeWeaponResponseSchema, error) {
	// Get user
	user, err := s.uu.GetUserByInternalId(internalUserId, repository.UserRepositoryParam{
		ManagedWeapons: true,
		ItemSummary:    true,
	})
	if err != nil {
		return nil, err
	}

	// Validate weapon exists
	managedWeapon, err := user.GetManagedWeapon(managedWeaponId)
	if err != nil {
		return nil, err
	}

	// Consume items and calculate required coins
	baseRequiredGolds, err := s.eu.GetRequiredCoinsForUpgrade(managedWeapon.Level)
	requiredGolds := uint64(0)
	for _, item := range items {
		if consumed := user.ConsumeItem(uint32(item.ItemId), uint32(item.Count)); !consumed {
			return nil, err
		}
		requiredGolds += uint64(item.Count) * baseRequiredGolds.ToValue()
	}
	if consumed := user.ConsumeGold(requiredGolds); !consumed {
		return nil, err
	}

	weapon, err := s.wu.GetWeaponById(managedWeapon.WeaponId)
	if err != nil {
		return nil, err
	}

	increaseExps, _ := value_exp.NewWeaponExp(0)
	for _, item := range items {
		amount, err := s.iu.GetWeaponUpgradeAmount(int64(item.ItemId))
		if err != nil {
			return nil, err
		}
		// Add class bonus
		if isBonus := weapon.IsUpgradeBonusItem(uint32(item.ItemId)); isBonus {
			// FIXME: Move this constant to something else
			// Source: https://kirarabbs.com/index.cgi?read=2670
			amount += value_exp.WeaponExp(uint32(float32(amount) * 0.2))
		}
		increaseExps += value_exp.WeaponExp(uint32(amount) * uint32(item.Count))
	}

	// Roll bonus
	// Source: https://kirarabbs.com/index.cgi?read=1201#bbsform
	bonus := s.ch.Roll()
	switch bonus {
	case upgrade.WeaponUpgradeResultPerfect:
		// FIXME: Move this constant to something else
		increaseExps += increaseExps
	case upgrade.WeaponUpgradeResultGreat:
		// FIXME: Move this constant to something else
		increaseExps += value_exp.WeaponExp(float32(increaseExps) * 0.5)
	}

	// Increase weapon exp
	managedWeapon.AddWeaponExp(increaseExps)
	// Recalculate weapon level
	nextWeaponLevel, err := s.eu.GetNextExpTableWeapon(managedWeapon.Exp, weapon.ExpTableId)
	if err != nil {
		return nil, err
	}

	// Limit display level (it doesn't affect the actual total exps)
	// FIXME: This code has chance to make over limit of total exps
	newLv := uint8(nextWeaponLevel.Level)
	if newLv > weapon.LimitLv {
		newLv = weapon.LimitLv
	}
	managedWeapon.UpdateWeaponLevel(newLv)

	user, err = s.uu.UpdateUser(user, repository.UserRepositoryParam{
		ItemSummary:    true,
		ManagedWeapons: true,
	})
	if err != nil {
		return nil, err
	}

	resp := schema_weapon.UpgradeWeaponResponseSchema{
		ManagedWeapon: *managedWeapon,
		ItemSummary:   user.ItemSummary,
		Gold:          value_user.Gold(user.Gold),
		UpgradeResult: bonus,
	}
	return &resp, nil
}
