package service

import (
	"gitlab.com/kirafan/sparkle/server/internal/application/usecase"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
)

type PlayerAgeService interface {
	SetPlayerAge(internalUserId uint, age value_user.Age) error
}

type playerAgeService struct {
	uu usecase.UserUsecase
}

func NewPlayerAgeService(uu usecase.UserUsecase) PlayerAgeService {
	return &playerAgeService{uu}
}

func (s *playerAgeService) SetPlayerAge(internalUserId uint, age value_user.Age) error {
	user, err := s.uu.GetUserByInternalId(internalUserId, repository.UserRepositoryParam{})
	if err != nil {
		return err
	}
	user.Age = age
	if _, err := s.uu.UpdateUser(user, repository.UserRepositoryParam{}); err != nil {
		return err
	}
	return nil
}
