package service

import (
	"encoding/json"
	"errors"

	schema_quest "gitlab.com/kirafan/sparkle/server/internal/application/schemas/quest"
	"gitlab.com/kirafan/sparkle/server/internal/application/usecase"
	model_quest "gitlab.com/kirafan/sparkle/server/internal/domain/model/quest"
	model_user "gitlab.com/kirafan/sparkle/server/internal/domain/model/user"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	"gitlab.com/kirafan/sparkle/server/internal/domain/value"
	value_exp "gitlab.com/kirafan/sparkle/server/internal/domain/value/exp"
	value_quest "gitlab.com/kirafan/sparkle/server/internal/domain/value/quest"
	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
	value_weapon "gitlab.com/kirafan/sparkle/server/internal/domain/value/weapon"
	"gitlab.com/kirafan/sparkle/server/pkg/calc"
	"gitlab.com/kirafan/sparkle/server/pkg/parser"
)

type UserQuestService interface {
	StartQuest(
		userId uint, questId uint, managedBattlePartyId uint,
		supportCharacterId int64, questNpcId int64,
	) (*model_user.User, *[][]*model_quest.QuestWave, *[][][]model_quest.QuestWaveDrop, error)
	SaveQuest(userId uint, orderReceiveId int, questData string) error
	CompleteQuest(
		userId uint, orderReceiveId uint,
		state uint8, clearRank value_quest.ClearRank,
		skillExps string, weaponSkillExps string,
		friendUseNum uint8, masterSkillUseNum uint8, uniqueSkillUseNum uint8,
		stepCode value_user.StepCode,
	) (*model_user.User, bool, error)
	ListQuest(
		userId uint,
	) (*schema_quest.AllQuestInfoWithClearRanksSchema, error)
}

type userQuestService struct {
	uu  usecase.UserUsecase
	qu  usecase.QuestUsecase
	cu  usecase.CharacterUsecase
	ecu usecase.ExpTableCharacterUsecase
	efu usecase.ExpTableFriendshipUsecase
	eru usecase.ExpTableRankUsecase
	esu usecase.ExpTableSkillUsecase
	qwu usecase.QuestWaveUsecase
}

func NewUserQuestService(
	uu usecase.UserUsecase,
	qu usecase.QuestUsecase,
	cu usecase.CharacterUsecase,
	ecu usecase.ExpTableCharacterUsecase,
	efu usecase.ExpTableFriendshipUsecase,
	eru usecase.ExpTableRankUsecase,
	esu usecase.ExpTableSkillUsecase,
	qwu usecase.QuestWaveUsecase,
) UserQuestService {
	return &userQuestService{uu, qu, cu, ecu, efu, eru, esu, qwu}
}

var ErrUserQuestNotEnoughStamina = errors.New("not enough stamina")
var ErrUserQuestNotEnoughItem = errors.New("not enough item")
var ErrUserQuestInvalidManagedBattlePartyId = errors.New("not valid managed battle party id")
var ErrUserQuestOrderReceiveIdMismatch = errors.New("orderReceiveId mismatch")
var ErrUserQuestLogNotFound = errors.New("quest log not found")
var ErrUserQuestQuestsNotFound = errors.New("quests weren't found")

func (s *userQuestService) ListQuest(
	userId uint,
) (*schema_quest.AllQuestInfoWithClearRanksSchema, error) {
	questInfo, err := s.qu.GetAll(userId)
	if err != nil {
		return nil, ErrUserQuestQuestsNotFound
	}
	userClearRanks, err := s.uu.GetUserClearRanks(userId)
	if err != nil {
		return nil, ErrUserQuestLogNotFound
	}
	user, err := s.uu.GetUserByInternalId(userId, repository.UserRepositoryParam{})
	if err != nil {
		return nil, ErrUserQuestLogNotFound
	}

	return &schema_quest.AllQuestInfoWithClearRanksSchema{
		AllQuestInfo:                  questInfo,
		ClearRanks:                    userClearRanks,
		LastPlayedChapterQuestIdPart1: user.LastPlayedPart1ChapterQuestId,
		LastPlayedChapterQuestIdPart2: user.LastPlayedPart2ChapterQuestId,
		PlayedOpenChapterIdPart1:      user.LastOpenedPart1ChapterId,
		PlayedOpenChapterIdPart2:      user.LastOpenedPart2ChapterId,
	}, nil
}

func (s *userQuestService) StartQuest(
	// required ids
	userId uint,
	questId uint,
	// optional ids for make a quest-log data
	managedBattlePartyId uint,
	supportCharacterId int64,
	questNpcId int64,
) (*model_user.User, *[][]*model_quest.QuestWave, *[][][]model_quest.QuestWaveDrop, error) {
	// Get user
	user, err := s.uu.GetUserByInternalId(userId, repository.UserRepositoryParam{
		ManagedBattleParties: true,
		ItemSummary:          true,
	})
	if err != nil {
		return nil, nil, nil, err
	}

	user.RefreshStamina()

	// Get quest
	quest, err := s.qu.GetQuest(questId)
	if err != nil {
		return nil, nil, nil, err
	}

	if s.qu.IsPart1Quest(questId) {
		user.LastPlayedPart1ChapterQuestId = int32(questId)
	} else if s.qu.IsPart2Quest(questId) {
		user.LastPlayedPart2ChapterQuestId = int32(questId)
	}

	if quest.AdvOnly == value.BoolLikeUIntTrue {
		// Create quest log
		questLog := model_user.NewQuestLogForAdvOnly(*user, questId)
		// Insert quest log
		err = s.uu.InsertUserQuestLog(user.Id, questLog)
		if err != nil {
			return nil, nil, nil, err
		}
		return user, &[][]*model_quest.QuestWave{}, &[][][]model_quest.QuestWaveDrop{}, nil
	}

	// Validate managed battle party Id
	var userManagedBattlePartyIds []int64
	for _, v := range user.ManagedBattleParties {
		userManagedBattlePartyIds = append(userManagedBattlePartyIds, int64(v.ManagedBattlePartyId))
	}
	if !calc.Contains(userManagedBattlePartyIds, int64(managedBattlePartyId)) {
		return nil, nil, nil, ErrUserQuestInvalidManagedBattlePartyId
	}

	// Validate user stamina or item and consumes
	var consumed bool
	if quest.Stamina != 0 && user.StepCode == value_user.StepCodeTutorialDone {
		consumed = user.ConsumeStamina(uint32(quest.Stamina))
		if !consumed {
			return nil, nil, nil, ErrUserQuestNotEnoughStamina
		}
	}
	if quest.ExId2 != -1 && user.StepCode == value_user.StepCodeTutorialDone {
		if consumed := user.ConsumeItem(uint32(quest.ExId2), uint32(quest.Ex2Amount)); !consumed {
			return nil, nil, nil, ErrUserQuestNotEnoughItem
		}
	}

	// Create quest waves and drop items
	var questWaves [][]*model_quest.QuestWave
	var questDropItems [][][]model_quest.QuestWaveDrop
	if quest.WaveId1 != 0 {
		questWaveInfo, dropItem, err := s.qwu.GetQuestWave(quest.WaveId1)
		if err != nil {
			return nil, nil, nil, err
		}
		questWaves = append(questWaves, questWaveInfo)
		questDropItems = append(questDropItems, dropItem)
	}
	if quest.WaveId2 != 0 {
		questWaveInfo, dropItem, err := s.qwu.GetQuestWave(quest.WaveId2)
		if err != nil {
			return nil, nil, nil, err
		}
		questWaves = append(questWaves, questWaveInfo)
		questDropItems = append(questDropItems, dropItem)
	}
	if quest.WaveId3 != 0 {
		questWaveInfo, dropItem, err := s.qwu.GetQuestWave(quest.WaveId3)
		if err != nil {
			return nil, nil, nil, err
		}
		questWaves = append(questWaves, questWaveInfo)
		questDropItems = append(questDropItems, dropItem)
	}
	if quest.WaveId4 != 0 {
		questWaveInfo, dropItem, err := s.qwu.GetQuestWave(quest.WaveId4)
		if err != nil {
			return nil, nil, nil, err
		}
		questWaves = append(questWaves, questWaveInfo)
		questDropItems = append(questDropItems, dropItem)
	}
	if quest.WaveId5 != 0 {
		questWaveInfo, dropItem, err := s.qwu.GetQuestWave(quest.WaveId5)
		if err != nil {
			return nil, nil, nil, err
		}
		questWaves = append(questWaves, questWaveInfo)
		questDropItems = append(questDropItems, dropItem)
	}

	// Convert waves and drop items to json strings for use later
	questWaveJsonBytes, err := json.Marshal(questWaves)
	if err != nil {
		return nil, nil, nil, err
	}
	questWaveJsonStrings := string(questWaveJsonBytes)
	questDropItemBytes, err := json.Marshal(questDropItems)
	if err != nil {
		return nil, nil, nil, err
	}
	questDropItemJsonStrings := string(questDropItemBytes)

	// Create quest log
	questLog, err := model_user.NewQuestLog(
		*user,
		managedBattlePartyId,
		questId,
		"",
		&questWaveJsonStrings,
		&questDropItemJsonStrings,
		uint8(len(questWaves)),
	)
	if err != nil {
		return nil, nil, nil, err
	}

	// Insert quest log
	err = s.uu.InsertUserQuestLog(user.Id, questLog)
	if err != nil {
		return nil, nil, nil, err
	}

	// Re-get Latest OrderID
	latestQuest, err := s.uu.GetLatestUserQuestLog(userId)
	if err != nil {
		return nil, nil, nil, err
	}
	// Update user latest quest log id
	user.LatestQuestLogID = latestQuest.QuestLogId

	// Save the user
	user, err = s.uu.UpdateUser(user, repository.UserRepositoryParam{ItemSummary: true})
	if err != nil {
		return nil, nil, nil, err
	}

	return user, &questWaves, &questDropItems, nil
}

func (s *userQuestService) SaveQuest(
	// required ids
	userId uint,
	orderReceiveId int,
	questData string,
) error {
	// Get user
	user, err := s.uu.GetUserByInternalId(userId, repository.UserRepositoryParam{})
	if err != nil {
		return err
	}

	// Validate orderReceiveId
	if user.LatestQuestLogID != orderReceiveId {
		return ErrUserQuestOrderReceiveIdMismatch
	}

	questLog, err := s.uu.GetLatestUserQuestLog(userId)
	if err != nil {
		return err
	}

	// Update quest log
	questLog.CurrentWave += 1
	questLog.QuestData = questData

	// Save the user questLog
	err = s.uu.UpdateUserQuestLog(*questLog)
	if err != nil {
		return err
	}

	return nil
}

func (s *userQuestService) updateUserRank(user *model_user.User) error {
	nextRank, err := s.eru.GetNextExpTableRank(user.TotalExp)
	if err != nil {
		return err
	}
	// LevelExp means differences between current user's exp and previous level's required exp
	// FIXME: Please refactor this line, no one can understand this
	user.LevelExp = user.TotalExp - (uint64(nextRank.TotalExp) - nextRank.NextExp)

	oldLevel := user.Level
	newLevel := uint16(nextRank.Rank)
	if oldLevel != newLevel {
		user.AddStaminaGold()
		user.StaminaMax = uint32(nextRank.Stamina)
		user.PartyCost = uint16(nextRank.BattlePartyCost)
		user.FriendLimit = nextRank.FriendLimit
		user.UpdateRank(newLevel)
	}
	return nil
}

func (s *userQuestService) updateManagedNamedTypeLevels(user *model_user.User) error {
	for i, namedType := range user.ManagedNamedTypes {
		nextNamedTypeLevel, err := s.efu.GetNextExpTableFriendship(uint64(namedType.Exp))
		if err != nil {
			return err
		}
		user.ManagedNamedTypes[i].UpdateLevel(uint8(nextNamedTypeLevel.Level))
	}
	return nil
}

func (s *userQuestService) updateManagedCharacterLevels(user *model_user.User) error {
	for i, character := range user.ManagedCharacters {
		nextCharacterLevel, err := s.ecu.GetNextExpTableCharacter(character.Exp)
		if err != nil {
			return err
		}
		nextCharacterSkillLevel1, err := s.esu.GetNextExpTableSkill(character.SkillExp1, value_exp.ExpTableSkillTypeSpecial)
		if err != nil {
			return err
		}
		nextCharacterSkillLevel2, err := s.esu.GetNextExpTableSkill(character.SkillExp2, value_exp.ExpTableSkillTypeNormal)
		if err != nil {
			return err
		}
		nextCharacterSkillLevel3, err := s.esu.GetNextExpTableSkill(character.SkillExp3, value_exp.ExpTableSkillTypeNormal)
		if err != nil {
			return err
		}
		user.ManagedCharacters[i].UpdateLevel(uint8(nextCharacterLevel.Level))
		user.ManagedCharacters[i].UpdateSkillLevel(nextCharacterSkillLevel1.Level, model_user.SkillType1)
		user.ManagedCharacters[i].UpdateSkillLevel(nextCharacterSkillLevel2.Level, model_user.SkillType2)
		user.ManagedCharacters[i].UpdateSkillLevel(nextCharacterSkillLevel3.Level, model_user.SkillType3)
	}
	return nil
}

func (s *userQuestService) updateManagedWeaponLevels(user *model_user.User) error {
	for i, weapon := range user.ManagedWeapons {
		nextWeaponSkillLevel, err := s.esu.GetNextExpTableSkill(uint32(weapon.SkillExp), value_exp.ExpTableSkillTypeWeapon)
		if err != nil {
			return err
		}
		user.ManagedWeapons[i].UpdateWeaponSkillLevel(nextWeaponSkillLevel.Level)
	}
	return nil
}

func (s *userQuestService) receiveDropItems(user *model_user.User, questDropItemsRawJson string) error {
	// Read item infos
	var questDropItems [][][]model_quest.QuestWaveDrop
	if err := json.Unmarshal([]byte(questDropItemsRawJson), &questDropItems); err != nil {
		return err
	}
	var droppedItems []model_quest.QuestWaveDrop
	for i := 0; i < len(questDropItems); i++ {
		for j := 0; j < len(questDropItems[i]); j++ {
			for k := 0; k < len(questDropItems[i][j]); k++ {
				droppedItems = append(droppedItems, questDropItems[i][j][k])
			}
		}
	}
	// Update item summaries
	for i := range droppedItems {
		user.AddItem(droppedItems[i].DropItemId, droppedItems[i].DropItemAmount)
	}
	return nil
}

func (s *userQuestService) receiveFirstClearRewards(user *model_user.User, advId uint64, quest *model_quest.Quest) {
	if user.HasClearedAdv(advId) {
		return
	}
	if quest.QuestFirstClearReward.ItemId1 != -1 {
		user.AddItem(uint32(quest.QuestFirstClearReward.ItemId1), uint32(quest.QuestFirstClearReward.Amount1))
	}
	if quest.QuestFirstClearReward.ItemId2 != -1 {
		user.AddItem(uint32(quest.QuestFirstClearReward.ItemId2), uint32(quest.QuestFirstClearReward.Amount2))
	}
	if quest.QuestFirstClearReward.ItemId3 != -1 {
		user.AddItem(uint32(quest.QuestFirstClearReward.ItemId3), uint32(quest.QuestFirstClearReward.Amount3))
	}
	user.AddLimitedGem(uint32(quest.QuestFirstClearReward.Gem))
	user.AddGold(uint64(quest.QuestFirstClearReward.Gold))
	if quest.QuestFirstClearReward.WeaponId != -1 && quest.QuestFirstClearReward.WeaponAmount != -1 {
		for i := 0; i < int(quest.QuestFirstClearReward.WeaponAmount); i++ {
			weaponId := value_weapon.NewWeaponId(quest.QuestFirstClearReward.WeaponId)
			user.AddWeapon(weaponId, true)
		}
	}
	if quest.QuestFirstClearReward.RoomObjectId != -1 && quest.QuestFirstClearReward.RoomObjectAmount != -1 {
		for i := 0; i < int(quest.QuestFirstClearReward.RoomObjectAmount); i++ {
			user.AddRoomObject(uint32(quest.QuestFirstClearReward.RoomObjectId))
		}
	}
}

func (s *userQuestService) addNamedTypeExps(user *model_user.User, quest *model_quest.Quest, managedCharacterIds []uint64) {
	// TODO: This code can make nil pointer error if client send invalid request
	addedNamedTypes := []uint16{999, 999, 999, 999, 999}
	for i := range managedCharacterIds {
		// FIXME: Remove this force type assertion
		characterId, err := user.GetCharacterIdByManagedCharacterId(value_user.ManagedCharacterId(managedCharacterIds[i]))
		if err != nil {
			continue
		}
		character, err := s.cu.GetCharacterById(*characterId)
		if err != nil {
			continue
		}
		if !calc.Contains(addedNamedTypes, character.NamedType) {
			user.AddNamedTypeExp(character.NamedType, uint32(quest.RewardFriendshipExp))
			addedNamedTypes[i] = character.NamedType
		}
	}
}

func (s *userQuestService) CompleteQuest(
	userId uint,
	orderReceiveId uint,
	// Cleared: 2 / Failed: 1
	state uint8,
	clearRank value_quest.ClearRank,
	characterSkillExps string,
	weaponSkillExps string,
	friendUseNum uint8,
	// kirara skill
	masterSkillUseNum uint8,
	// とっておき
	uniqueSkillUseNum uint8,
	stepCode value_user.StepCode,
) (*model_user.User, bool, error) {
	// Get user
	user, err := s.uu.GetUserByInternalId(userId, repository.UserRepositoryParam{
		ManagedNamedTypes: true,
		ManagedCharacters: true,
		ManagedWeapons:    true,
		ItemSummary:       true,
		AdvIds:            true,
	})
	if err != nil {
		return nil, false, err
	}
	// Validate orderReceiveId
	if user.LatestQuestLogID != int(orderReceiveId) {
		return nil, false, ErrUserQuestOrderReceiveIdMismatch
	}
	// Get quest log
	questLog, err := s.uu.GetLatestUserQuestLog(userId)
	if err != nil {
		return nil, false, err
	}

	// Refresh server side stamina
	user.RefreshStamina()

	// Update questLog
	questLog.CurrentWave = questLog.TotalWave
	questLog.ClearRank = clearRank
	// Save the user questLog
	err = s.uu.UpdateUserQuestLog(*questLog)
	if err != nil {
		return nil, false, err
	}

	// Update user info
	user.LatestQuestLogID = -1
	// Finalize if quest is failed
	if clearRank == value_quest.ClearRankNone {
		// Save the user
		user, err = s.uu.UpdateUser(user, repository.UserRepositoryParam{})
		if err != nil {
			return nil, false, err
		}
		return user, false, nil
	}

	// Read quest infos
	quest, err := s.qu.GetQuest(questLog.QuestId)
	if err != nil {
		return nil, false, err
	}

	if quest.AdvOnly == value.BoolLikeUIntTrue {
		s.receiveFirstClearRewards(user, uint64(quest.AdvId1), quest)
		user.AddClearedAdv(uint64(quest.Id))
		user, err = s.uu.UpdateUser(user, repository.UserRepositoryParam{
			ManagedWeapons: true,
			ItemSummary:    true,
			AdvIds:         true,
		})
		if err != nil {
			return nil, false, err
		}
		return user, false, nil
	}

	if err := s.receiveDropItems(user, *questLog.DropData); err != nil {
		return nil, false, err
	}
	s.receiveFirstClearRewards(user, uint64(quest.AdvId2), quest)

	user.AddGold(quest.RewardMoney)

	// Add exps
	parsedCharacterSkillExps := parser.ParseCharacterExpString(characterSkillExps)
	managedCharacterIds := make([]uint64, 0, len(parsedCharacterSkillExps))
	for characterId := range parsedCharacterSkillExps {
		managedCharacterIds = append(managedCharacterIds, characterId)
	}
	s.addNamedTypeExps(user, quest, managedCharacterIds)
	user.AddCharacterExps(quest.RewardCharacterExp, managedCharacterIds)
	user.AddCharacterSkillExps(parsedCharacterSkillExps)
	user.AddWeaponSkillExps(parser.ParseWeaponExpString(weaponSkillExps))
	user.AddRankExp(quest.RewardUserExp)

	// Update levels
	if err := s.updateUserRank(user); err != nil {
		return nil, false, err
	}
	if err := s.updateManagedCharacterLevels(user); err != nil {
		return nil, false, err
	}
	if err := s.updateManagedWeaponLevels(user); err != nil {
		return nil, false, err
	}
	if err := s.updateManagedNamedTypeLevels(user); err != nil {
		return nil, false, err
	}

	// Update stepCode
	if user.StepCode == value_user.StepCodeFirstQuestStart {
		user.StepCode = value_user.StepCodeFirstQuestDone
	}
	isFirstClear := !user.HasClearedAdv(uint64(quest.Id))
	user.AddClearedAdv(uint64(quest.Id))

	// Save the user
	user, err = s.uu.UpdateUser(user, repository.UserRepositoryParam{
		ManagedNamedTypes: true,
		ManagedCharacters: true,
		ManagedWeapons:    true,
		ItemSummary:       true,
		AdvIds:            true,
	})
	if err != nil {
		return nil, false, err
	}

	return user, isFirstClear, nil
}
