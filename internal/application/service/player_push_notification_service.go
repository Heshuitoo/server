package service

import (
	"gitlab.com/kirafan/sparkle/server/internal/application/usecase"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	"gitlab.com/kirafan/sparkle/server/internal/domain/value"
)

type PlayerPushNotificationService interface {
	SetPlayerPushNotification(internalUserId uint, flagPush value.BoolLikeUInt8, flagUi value.BoolLikeUInt8, flagStamina value.BoolLikeUInt8) error
}

type playerPushNotificationService struct {
	uu usecase.UserUsecase
}

func NewPlayerPushNotificationService(uu usecase.UserUsecase) PlayerPushNotificationService {
	return &playerPushNotificationService{uu}
}

func (s *playerPushNotificationService) SetPlayerPushNotification(internalUserId uint, flagPush value.BoolLikeUInt8, flagUi value.BoolLikeUInt8, flagStamina value.BoolLikeUInt8) error {
	user, err := s.uu.GetUserByInternalId(internalUserId, repository.UserRepositoryParam{})
	if err != nil {
		return err
	}

	user.FlagPush = flagPush
	user.FlagUi = flagUi
	user.FlagStamina = flagStamina

	if _, err := s.uu.UpdateUser(user, repository.UserRepositoryParam{}); err != nil {
		return err
	}
	return nil
}
