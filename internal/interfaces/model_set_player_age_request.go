/*
 * SparkleAPI
 *
 * \"Our tomorrow is always a prologue\"  This doc is API Reference and template to generate krr-prd.star-api mirror.
 *
 * API version: 1.0
 * Contact: contact@sparklefantasia.com
 * Generated by: OpenAPI Generator (https://openapi-generator.tech)
 */

package interfaces

type SetPlayerAgeRequest struct {
	Age int64 `json:"age"`
}

// AssertSetPlayerAgeRequestRequired checks if the required fields are not zero-ed
func AssertSetPlayerAgeRequestRequired(obj SetPlayerAgeRequest) error {
	elements := map[string]interface{}{
		"age": obj.Age,
	}
	for name, el := range elements {
		if isZero := IsZeroValue(el); isZero {
			return &RequiredError{Field: name}
		}
	}

	return nil
}

// AssertRecurseSetPlayerAgeRequestRequired recursively checks if required fields are not zero-ed in a nested slice.
// Accepts only nested slice of SetPlayerAgeRequest (e.g. [][]SetPlayerAgeRequest), otherwise ErrTypeAssertionError is thrown.
func AssertRecurseSetPlayerAgeRequestRequired(objSlice interface{}) error {
	return AssertRecurseInterfaceRequired(objSlice, func(obj interface{}) error {
		aSetPlayerAgeRequest, ok := obj.(SetPlayerAgeRequest)
		if !ok {
			return ErrTypeAssertionError
		}
		return AssertSetPlayerAgeRequestRequired(aSetPlayerAgeRequest)
	})
}
