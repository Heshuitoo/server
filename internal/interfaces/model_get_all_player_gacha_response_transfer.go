package interfaces

import (
	model_user "gitlab.com/kirafan/sparkle/server/internal/domain/model/user"
	"gitlab.com/kirafan/sparkle/server/pkg/calc"
	"gitlab.com/kirafan/sparkle/server/pkg/response"
)

func toGetAllPlayerGachaResponse(gachas []model_user.UserGacha, success *response.BaseResponse) *GetAllPlayerGachaResponse {
	outGachas := make([]GachasArrayObject, len(gachas))
	calc.Copy(&outGachas, &gachas)
	for i := range outGachas {
		// Fill sub model fields
		calc.Copy(&outGachas[i].Gacha, (gachas)[i].Gacha)

		// File date time fields
		outGachas[i].Gacha.DispStartAt = response.ToSparkleTime((gachas)[i].Gacha.DispStartAt)
		outGachas[i].Gacha.DispEndAt = response.ToSparkleTime((gachas)[i].Gacha.DispEndAt)
		outGachas[i].Gacha.StartAt = response.ToSparkleTime((gachas)[i].Gacha.StartAt)
		outGachas[i].Gacha.EndAt = response.ToSparkleTime((gachas)[i].Gacha.EndAt)
		outGachas[i].Gacha.Id = int64((gachas)[i].Gacha.GachaId)

		// Fill sub model array fields
		selectionCharacterIds := make([]int64, 0)
		for _, id := range (gachas)[i].Gacha.SelectionCharacterIds {
			selectionCharacterIds = append(selectionCharacterIds, int64(id.CharacterId))
		}
		outGachas[i].SelectionCharacterIds = selectionCharacterIds

		drawPoints := make([]interface{}, 0)
		for _, point := range (gachas)[i].Gacha.DrawPoints {
			drawPoints = append(drawPoints, map[string]interface{}{
				"count": point.CharacterId,
				"point": point.Point,
			})
		}
		outGachas[i].DrawPoints = drawPoints

		gachaSteps := make([]interface{}, 0)
		for _, step := range (gachas)[i].Gacha.GachaSteps {
			var gachaBonusItems []map[string]interface{}
			for _, item := range step.GachaBonusItems {
				gachaBonusItems = append(gachaBonusItems, map[string]interface{}{
					"id":     item.Id,
					"amount": item.Amount,
				})
			}
			gachaSteps = append(gachaSteps, map[string]interface{}{
				"step":            step.Step,
				"gachaBonusItems": gachaBonusItems,
			})
		}
		outGachas[i].Gacha.GachaSteps = gachaSteps
	}
	// STUB
	finishedGachas := make([]interface{}, 0)
	gachaDailyFrees := make([]interface{}, 0)
	gachaFreeNextRefreshAt := "2099-12-31T23:59:59"
	return &GetAllPlayerGachaResponse{
		FinishedGachas:         finishedGachas,
		GachaDailyFrees:        gachaDailyFrees,
		GachaFreeNextRefreshAt: gachaFreeNextRefreshAt,
		Gachas:                 outGachas,
		NewAchievementCount:    success.NewAchievementCount,
		ResultCode:             success.ResultCode,
		ResultMessage:          success.ResultMessage,
		ServerTime:             success.ServerTime,
		ServerVersion:          success.ServerVersion,
	}
}
