package interfaces

import (
	"context"
	"os"
	"testing"

	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/database"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/database/migrate"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/database/seed"
	"gitlab.com/kirafan/sparkle/server/pkg/ctx_func"
	"gorm.io/gorm"
)

var db *gorm.DB
var logRepo repository.LoggerRepository
var validUserId1Context = context.WithValue(context.TODO(), ctx_func.CtxUserId, uint(1))
var baseIgnoreFields = []string{
	"NewAchievementCount",
	"ResultCode",
	"ResultMessage",
	"ServerVersion",
	"ServerTime",
}

func TestMain(m *testing.M) {
	logger := database.InitLogger()
	logRepo = database.InitLoggerRepo(logger)
	db = database.InitDatabase(logRepo, logger)
	migrate.AutoMigrate(db)
	seed.AutoSeed(db)

	exitVal := m.Run()
	os.Exit(exitVal)
}
