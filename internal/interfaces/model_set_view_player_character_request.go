/*
 * SparkleAPI
 *
 * \"Our tomorrow is always a prologue\"  This doc is API Reference and template to generate krr-prd.star-api mirror.
 *
 * API version: 1.0
 * Contact: contact@sparklefantasia.com
 * Generated by: OpenAPI Generator (https://openapi-generator.tech)
 */

package interfaces

type SetViewPlayerCharacterRequest struct {
	ManagedCharacterIds []int64 `json:"managedCharacterIds"`

	ViewEvolutions []int64 `json:"viewEvolutions"`
}

// AssertSetViewPlayerCharacterRequestRequired checks if the required fields are not zero-ed
func AssertSetViewPlayerCharacterRequestRequired(obj SetViewPlayerCharacterRequest) error {
	elements := map[string]interface{}{
		"managedCharacterIds": obj.ManagedCharacterIds,
		"viewEvolutions":      obj.ViewEvolutions,
	}
	for name, el := range elements {
		if isZero := IsZeroValue(el); isZero {
			return &RequiredError{Field: name}
		}
	}

	return nil
}

// AssertRecurseSetViewPlayerCharacterRequestRequired recursively checks if required fields are not zero-ed in a nested slice.
// Accepts only nested slice of SetViewPlayerCharacterRequest (e.g. [][]SetViewPlayerCharacterRequest), otherwise ErrTypeAssertionError is thrown.
func AssertRecurseSetViewPlayerCharacterRequestRequired(objSlice interface{}) error {
	return AssertRecurseInterfaceRequired(objSlice, func(obj interface{}) error {
		aSetViewPlayerCharacterRequest, ok := obj.(SetViewPlayerCharacterRequest)
		if !ok {
			return ErrTypeAssertionError
		}
		return AssertSetViewPlayerCharacterRequestRequired(aSetViewPlayerCharacterRequest)
	})
}
