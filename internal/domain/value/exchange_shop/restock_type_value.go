package value_exchange_shop

type RestockType uint8

const (
	RestockTypeNone RestockType = iota
	RestockTypeDaily
	RestockTypeWeekly
	RestockTypeMonthly
)
