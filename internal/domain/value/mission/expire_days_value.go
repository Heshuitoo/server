package value_mission

type ExpireDays uint8

const (
	EXPIRE_TUTORIAL_MISSION ExpireDays = 31
	EXPIRE_TUTORIAL_PRESENT ExpireDays = 31
	EXPIRE_DAILY_MISSION    ExpireDays = 1
	EXPIRE_WEEKLY_MISSION   ExpireDays = 7
	EXPIRE_EVENT_MISSION    ExpireDays = 31
)
