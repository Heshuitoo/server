package value_stamina

import "errors"

type StaminaConsumeType uint8

const (
	StaminaConsumeTypeUnknown StaminaConsumeType = iota
	StaminaConsumeTypeGem
	StaminaConsumeTypeItem
)

var ErrInvalidStaminaConsumeType = errors.New("invalid stamina consume type")

func NewStaminaConsumeType(value uint8) (StaminaConsumeType, error) {
	if value > uint8(StaminaConsumeTypeItem) {
		return StaminaConsumeTypeUnknown, ErrInvalidStaminaConsumeType
	}
	return StaminaConsumeType(value), nil
}
