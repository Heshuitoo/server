package value_present

import "errors"

type PresentInsertType uint8

const (
	PresentInsertTypeTutorial PresentInsertType = iota
	PresentInsertTypeLoginBonus
	PresentInsertTypeLoginContinuousBonus
	PresentInsertTypeMissionReward
	PresentInsertTypeCharacterRelationship
	PresentInsertTypeCharacterFacility
	PresentInsertTypeTrade
	PresentInsertTypeCampaign
)

var ErrInvalidPresentInsertType = errors.New("invalid present insert type")

func NewPresentInsertType(value uint8) (PresentInsertType, error) {
	if value > uint8(PresentInsertTypeCampaign) {
		return PresentInsertType(0), ErrInvalidPresentInsertType
	}
	return PresentInsertType(value), nil
}
