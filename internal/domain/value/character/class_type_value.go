package value_character

import "errors"

type ClassType int8

const (
	ClassTypeNone ClassType = iota - 1
	ClassTypeFire
	ClassTypeWater
	ClassTypeEarth
	ClassTypeWind
	ClassTypeMoon
	ClassTypeSun
)

var ErrInvalidClassType = errors.New("invalid ClassType")

func NewClassType(v uint8) (ClassType, error) {
	if v > uint8(ClassTypeSun) {
		return ClassTypeNone, ErrInvalidClassType
	}
	return ClassType(v), nil
}
