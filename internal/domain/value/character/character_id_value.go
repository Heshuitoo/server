package value_character

import "errors"

type CharacterId uint32

func (c CharacterId) GetRarity() CharacterIdRarity {
	rarity := (c / 1000) % 10
	return CharacterIdRarity(rarity)
}

func (c CharacterId) GetContent() CharacterIdContent {
	shiftedValue := (c / 1000000) % 100
	return CharacterIdContent(shiftedValue)
}

func (c CharacterId) IsEvolved() bool {
	return (c/100000)%10 == 1
}

func (c CharacterId) SetEvoluteState(isEvolved bool) CharacterId {
	// Get upper 7 digits / Ex: 32202001 -> 32202000
	res := (uint32(c) / 10) * 10
	// Set evolve status at 8th digit
	if isEvolved {
		res += 1
	}
	return CharacterId(res)
}

func NewCharacterId(id uint32) (CharacterId, error) {
	// Check if the input value is 8 digits
	if id < 10000000 || id > 99999999 {
		return 0, errors.New("characterId must be 8 digits")
	}
	// 1st-2nd digit is content (10~47)
	content := (id / 1000000) % 100
	if content < 10 || content > 47 {
		return 0, errors.New("characterContent (1st-2nd digit) must be 10~47")
	}
	// 3rd-4th digit is character index (00~99 but limits to 30)
	characterIndex := (id / 10000) % 100
	if characterIndex > 30 {
		return 0, errors.New("characterIndex (3rd-4th digit) must be 00~30")
	}
	// 5th digit is the rarity (0~2)
	rarity := (id / 1000) % 10
	if rarity > 2 {
		return 0, errors.New("characterRarity (5th digit) must be 0, 1, or 2")
	}
	// 6th-7th digit is variation index (00~99 but limits to 20)
	variationIndex := (id / 10) % 100
	if variationIndex > 20 {
		return 0, errors.New("characterVariationIndex (6th-7th digit) must be 00~20")
	}
	// 8th digit is the evolution (0~1)
	isEvolution := (id / 100000) % 10
	if isEvolution > 1 {
		return 0, errors.New("characterEvolution (8th digit) must be 0, or 1")
	}
	return CharacterId(id), nil
}
