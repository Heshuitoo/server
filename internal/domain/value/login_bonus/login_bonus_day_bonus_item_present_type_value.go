package value_login_bonus

import "errors"

// Internally same as present type but limits to item and gem
type LoginBonusDayBonusItemPresentType uint8

const (
	LoginBonusDayPresentTypeItem       LoginBonusDayBonusItemPresentType = 2
	LoginBonusDayPresentTypeLimitedGem LoginBonusDayBonusItemPresentType = 10
)

var ErrInvalidLoginBonusDayBonusItemPresentType = errors.New("invalid LoginBonusDayBonusItemPresentType")

func NewLoginBonusDayBonusItemPresentType(v uint8) (LoginBonusDayBonusItemPresentType, error) {
	if v > uint8(LoginBonusDayPresentTypeLimitedGem) {
		return LoginBonusDayPresentTypeItem, ErrInvalidLoginBonusDayBonusItemPresentType
	}
	return LoginBonusDayBonusItemPresentType(v), nil
}
