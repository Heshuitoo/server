package value_room

type RoomObjectType uint8

const (
	RoomObjectTypeDesk RoomObjectType = iota
	RoomObjectTypeChair
	RoomObjectTypeStorage
	RoomObjectTypeBedding
	RoomObjectTypeAppliances
	RoomObjectTypeGoods
	RoomObjectTypeHobby
	RoomObjectTypeWallDecoration
	RoomObjectTypeCarpet
	RoomObjectTypeScreen
	RoomObjectTypeFloor
	RoomObjectTypeWall
	RoomObjectTypeBackground
	RoomObjectTypeMsgboard
	RoomObjectTypeNum
)
