package value_town_facility

import "errors"

// TownFacilityOpenState is a value for town facility /buy_all or /level_up endpoint
type TownFacilityOpenState uint8

var ErrTownFacilityOpenStateInvalid = errors.New("invalid town facility open state, it must be between 0~4")

const (
	TownFacilityOpenStateNone TownFacilityOpenState = iota
	TownFacilityOpenStateOpen
	TownFacilityOpenStateWait
	TownFacilityOpenStatePreWait
	TownFacilityOpenStateOnWait
)

func NewTownFacilityOpenState(townFacilityOpenState uint8) (TownFacilityOpenState, error) {
	if townFacilityOpenState > uint8(TownFacilityOpenStateOnWait) {
		return 0, ErrTownFacilityOpenStateInvalid
	}
	return TownFacilityOpenState(townFacilityOpenState), nil
}
