package value_town_facility

import "errors"

type FacilityBuildTimeType uint8

const (
	FacilityBuildTimeTypeNone    FacilityBuildTimeType = 0
	FacilityBuildTimeTypeFactory FacilityBuildTimeType = 3
)

var ErrInvalidFacilityBuildTimeType = errors.New("invalid FacilityBuildTimeType")

func NewFacilityBuildTimeType(v uint8) (FacilityBuildTimeType, error) {
	if v != uint8(FacilityBuildTimeTypeFactory) && v != uint8(FacilityBuildTimeTypeNone) {
		return FacilityBuildTimeTypeNone, ErrInvalidFacilityBuildTimeType
	}
	return FacilityBuildTimeType(v), nil
}
