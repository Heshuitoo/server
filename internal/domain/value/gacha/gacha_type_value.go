package value_gacha

import "errors"

type GachaType uint8

const (
	GachaTypeNormal GachaType = iota + 1
	GachaTypePickup
	GachaTypeUnused // Maybe dead at something version
	GachaTypeUnlimitedGem
	GachaTypeSelectable
	GachaTypeStepup
)

var ErrInvalidGachaType = errors.New("invalid GachaType")

func NewGachaType(v uint8) (GachaType, error) {
	if v < uint8(GachaTypeNormal) || v > uint8(GachaTypeStepup) {
		return GachaTypeNormal, ErrInvalidGachaType
	}
	return GachaType(v), nil
}
