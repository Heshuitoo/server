package value_quest

import "errors"

type QuestRetryGem uint8

const (
	QuestRetryGemFree    QuestRetryGem = 0
	QuestRetryGemDefault QuestRetryGem = 5
	QuestRetryGemMax     QuestRetryGem = 100
)

var ErrInvalidQuestRetryGem = errors.New("invalid quest retry gems")

func NewQuestRetryGem(value uint8) (QuestRetryGem, error) {
	if value > uint8(QuestRetryGemMax) {
		return QuestRetryGemFree, ErrInvalidQuestRetryGem
	}
	return QuestRetryGem(value), nil
}
