package value_user

type ManagedCharacterId int64

func (id *ManagedCharacterId) ToInt64() int64 {
	return int64(*id)
}

func NewManagedCharacterId(id int64) ManagedCharacterId {
	return ManagedCharacterId(id)
}
