package value_friend

type FriendState uint8

// TODO: Find out valid constant values
// These values are few from responses
const (
	FriendStateGuest  FriendState = 0
	FriendStateFriend FriendState = 2
)
