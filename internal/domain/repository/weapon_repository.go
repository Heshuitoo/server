package repository

import (
	model_weapon "gitlab.com/kirafan/sparkle/server/internal/domain/model/weapon"
	value_weapon "gitlab.com/kirafan/sparkle/server/internal/domain/value/weapon"
)

type WeaponRepository interface {
	FindByWeaponId(weaponId value_weapon.WeaponId) (*model_weapon.Weapon, error)
}
