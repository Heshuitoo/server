package repository

import model_character "gitlab.com/kirafan/sparkle/server/internal/domain/model/character"

type CharacterRepository interface {
	FindCharacter(query *model_character.Character, criteria map[string]interface{}, associations *[]string) (*model_character.Character, error)
	FindCharacters(query *model_character.Character, criteria map[string]interface{}, associations *[]string) ([]*model_character.Character, error)
}
