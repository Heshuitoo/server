package repository

import (
	model_version "gitlab.com/kirafan/sparkle/server/internal/domain/model/version"
	value_version "gitlab.com/kirafan/sparkle/server/internal/domain/value/version"
)

type VersionRepository interface {
	// Create a version info
	Create(version *model_version.Version) (*model_version.Version, error)
	// Update a version info
	Update(version *model_version.Version) (*model_version.Version, error)
	// Find a version info
	FindByPlatformAndVersion(platform value_version.Platform, version value_version.Version) (*model_version.Version, error)
}
