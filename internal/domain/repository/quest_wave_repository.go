package repository

import (
	model_quest "gitlab.com/kirafan/sparkle/server/internal/domain/model/quest"
)

type QuestWaveRepository interface {
	FindQuestWave(query *model_quest.QuestWave, criteria map[string]interface{}, associations *[]string) (*model_quest.QuestWave, error)
	FindQuestWaves(query *model_quest.QuestWave, criteria map[string]interface{}, associations *[]string) ([]*model_quest.QuestWave, error)
	FindQuestWaveRandoms(query *model_quest.QuestWaveRandom, criteria map[string]interface{}, associations *[]string) ([]*model_quest.QuestWaveRandom, error)
	FindQuestWaveDrops(query *model_quest.QuestWaveDrop, criteria map[string]interface{}, associations *[]string) ([]*model_quest.QuestWaveDrop, error)
}
