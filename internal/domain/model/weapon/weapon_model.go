package model_weapon

import (
	"gitlab.com/kirafan/sparkle/server/internal/domain/value"
	value_character "gitlab.com/kirafan/sparkle/server/internal/domain/value/character"
	value_item "gitlab.com/kirafan/sparkle/server/internal/domain/value/item"
	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
	value_weapon "gitlab.com/kirafan/sparkle/server/internal/domain/value/weapon"
)

type Weapon struct {
	WeaponId     value_weapon.WeaponId `gorm:"primaryKey"`
	WeaponName   string
	Rarity       value_item.ItemRare
	Class        value_character.ClassType
	Cost         uint8
	ExpTableId   uint8
	InitLv       uint8
	LimitLv      uint8
	EvolvedCount uint8
	MaxEvolution uint8
	// This ID sometimes can be -1
	EquipableCharacterId int32
	InitAtk              uint16
	InitMgc              uint16
	InitDef              uint16
	InitMDef             uint16
	MaxAtk               uint16
	MaxMgc               uint16
	MaxDef               uint16
	MaxMDef              uint16
	// This ID sometimes can be -1
	SkillId         int32
	SkillLimitLv    uint8
	SkillExpTableId uint8
	// This ID sometimes can be -1
	PassiveSkillId              int32
	SaleAmount                  value_user.Gold
	IsSellable                  value.BoolLikeUInt8
	IsDefault                   bool
	UpgradeBonusMaterialItemIDs []WeaponUpgradeBonusMaterial `gorm:"foreignKey:WeaponId"`
}

func (w *Weapon) IsUpgradeBonusItem(itemId uint32) bool {
	for _, bonus := range w.UpgradeBonusMaterialItemIDs {
		if bonus.ItemId == itemId {
			return true
		}
	}
	return false
}

func (w *Weapon) CanEvolve() bool {
	return w.MaxEvolution != w.EvolvedCount
}
