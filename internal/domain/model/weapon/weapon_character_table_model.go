package model_weapon

import (
	value_character "gitlab.com/kirafan/sparkle/server/internal/domain/value/character"
	value_weapon "gitlab.com/kirafan/sparkle/server/internal/domain/value/weapon"
)

type WeaponCharacterTable struct {
	Id          uint `gorm:"primaryKey"`
	CharacterId value_character.CharacterId
	WeaponId    value_weapon.WeaponId
	CondLevel   uint8
}
