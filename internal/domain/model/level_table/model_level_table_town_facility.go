package model_level_table

import (
	"gitlab.com/kirafan/sparkle/server/internal/domain/value"
	value_town_facility "gitlab.com/kirafan/sparkle/server/internal/domain/value/town_facility"
	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
)

type LevelTableTownFacility struct {
	LevelTableFacilityId  *uint `gorm:"primaryKey"`
	LevelUpListId         value_town_facility.LevelUpListId
	TargetLevel           uint8
	GoldAmountBuy         uint32
	KiraraPointAmountBuy  uint32
	GoldAmountSell        uint32
	KiraraPointAmountSell uint32
	ReqUserLv             uint8
	BuildTimeSeconds      uint16
	IntervalTimeSeconds   uint16
	MaxGenMinutesOrItems  uint16
	MaxKiraraPoint        uint32
	IsRoomFunc            value.BoolLikeUInt8
	// 0 or 2
	IsSubRoomFunc uint8
	IsSubRoomOpen value.BoolLikeUInt8
	IsTownFunc    value.BoolLikeUInt8
	FreeStepCode  value_user.StepCode
	// Maybe these values are unneeded? (calculated at client side)
	FieldItemDropProbabilities []LevelTableTownFacilityFieldItemDropProbability `gorm:"foreignKey:LevelTableFacilityId"`
	FieldItemDropIds           []LevelTableTownFacilityFieldItemDropId          `gorm:"foreignKey:LevelTableFacilityId"`
}
