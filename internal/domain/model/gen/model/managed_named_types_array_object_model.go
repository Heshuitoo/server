package model

type ManagedNamedTypesArrayObject struct {
	Exp int64

	FriendshipExpTableId int64

	Level int64

	ManagedNamedTypeId int64

	NamedType int64

	TitleType int64
}
