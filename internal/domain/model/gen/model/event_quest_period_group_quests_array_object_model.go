package model

type EventQuestPeriodGroupQuestsArrayObject struct {
	CondEventQuestIds []interface{}

	EndAt string

	EventQuestId int64

	QuestId int64

	StartAt string

	Unlocked bool
}
