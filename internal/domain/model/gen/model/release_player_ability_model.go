package model

type ReleasePlayerAbilityResponse struct {
	ItemSummary []ItemSummaryArrayObject

	PlayerAbilityBoards []PlayerAbilityBoardsArrayObject
}
