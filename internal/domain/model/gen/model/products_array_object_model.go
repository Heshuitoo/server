package model

type ProductsArrayObject struct {
	AlreadyShown bool

	Amount1 int64

	Amount2 int64

	Description string

	DirectSale *ProductsArrayObjectDirectSale

	DirectSaleId int64

	DispEndAt string

	DispStartAt string

	Env int64

	Id int64

	IsNew bool

	LimitNum int64

	LinkId int64

	Name string

	PassType int64

	Platform int64

	PrefabType int64

	Premium *interface{}

	PremiumId int64

	Price int64

	PurchaseCount int64

	Sku string

	StoreBonuses []interface{}

	TabId int64

	UiPriority int64

	UiType int64
}
