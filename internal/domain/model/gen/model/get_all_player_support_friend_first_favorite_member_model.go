package model

type GetAllPlayerResponseSupportFriendFirstFavoriteMember struct {
	AbilityBoardId int64

	ArousalLevel int64

	CharacterId int64

	DuplicatedCount int64

	EquipItemIds *string

	Exp int64

	Level int64

	LevelBreak int64

	ManagedCharacterId int64

	NamedExp int64

	NamedLevel int64

	SkillLevel1 int64

	SkillLevel2 int64

	SkillLevel3 int64

	WeaponId int64

	WeaponLevel int64

	WeaponSkillExp int64

	WeaponSkillLevel int64
}
