package model

type PlayerAbilityBoardsArrayObject struct {
	AbilityBoardId int64

	EquipItemIds []int64

	ManagedAbilityBoardId int64

	ManagedCharacterId int64
}
