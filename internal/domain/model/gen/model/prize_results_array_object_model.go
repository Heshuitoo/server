package model

type PrizeResultsArrayObject struct {
	ResetTarget int64

	RewardAmount int64

	RewardId int64

	RewardType int64
}
