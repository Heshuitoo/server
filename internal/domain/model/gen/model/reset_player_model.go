package model

type ResetPlayerResponse struct {
	NewAchievementCount int64  `json:"newAchievementCount,omitempty"`
	ResultCode          int32  `json:"resultCode,omitempty"`
	ResultMessage       string `json:"resultMessage,omitempty"`
	ServerTime          string `json:"serverTime,omitempty"`
	ServerVersion       int32  `json:"serverVersion,omitempty"`
}
