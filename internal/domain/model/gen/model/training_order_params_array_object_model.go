package model

type TrainingOrderParamsArrayObject struct {
	ManagedCharacterIds []int64

	SlotId int64

	TrainingId int64
}
