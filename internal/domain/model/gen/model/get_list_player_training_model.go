package model

type GetListPlayerTrainingResponse struct {
	SlotInfo []SlotInfoArrayObject

	TrainingInfo []TrainingInfoArrayObject
}
