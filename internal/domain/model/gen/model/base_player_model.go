package model

type BasePlayer struct {
	Age                  int64
	CharacterLimit       int64
	CharacterWeaponCount int64
	Comment              *string
	ContinuousDays       int64
	CreatedAt            string
	CurrentAchievementId int64
	FacilityLimit        int64
	FacilityLimitCount   int64
	FriendLimit          int64
	Gold                 int64
	Id                   int64
	IpAddr               string
	ItemLimit            int64
	Kirara               int64
	KiraraLimit          int64
	LastLoginAt          string
	LastPartyAdded       string
	Level                int64
	LevelExp             int64
	LimitedGem           int64
	LoginCount           int64
	LoginDays            int64
	LotteryTicket        int64
	MyCode               string
	Name                 string
	PartyCost            int64
	RecastTime           int64
	RecastTimeMax        int64
	RoomObjectLimit      int64
	RoomObjectLimitCount int64
	Stamina              int64
	StaminaMax           int64
	StaminaUpdatedAt     string
	State                int64

	SupportLimit     int64
	TotalExp         int64
	UnlimitedGem     int64
	UserAgent        string
	WeaponLimit      int64
	WeaponLimitCount int64
}
