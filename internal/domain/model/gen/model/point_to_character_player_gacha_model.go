package model

type PointToCharacterPlayerGachaResponse struct {
	AfterDrawPoint int64

	ArousalResults []interface{}

	BeforeDrawPoint int64

	GachaResults []GachaResultsArrayObject

	ItemSummary []ItemSummaryArrayObject

	ManagedCharacters []ManagedCharactersArrayObject

	ManagedNamedTypes []ManagedNamedTypesArrayObject

	OfferTitleTypes []OfferTitleTypesArrayObject
}
