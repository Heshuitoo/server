package model

type OpenUpPlayerTownFacilityResponse struct {
	ManagedRoom *string

	ManagedRoomObjects []interface{}

	Player BasePlayer
}
