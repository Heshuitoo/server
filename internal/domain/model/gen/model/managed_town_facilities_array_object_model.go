package model

type ManagedTownFacilitiesArrayObject struct {
	ActionTime int64

	BuildPointIndex int64

	BuildTime int64

	FacilityId int64

	Level int64

	ManagedTownFacilityId int64

	OpenState int64
}
