package model

type BannersArrayObject struct {
	Category int64

	DispEndAt string

	EndAt string

	GachaId int64

	Id int64

	ImgId string

	OffsetX float32

	OffsetY float32

	PickupCharacterIds *string

	StartAt string
}
