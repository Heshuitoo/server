package model

type QuestNpcsArrayObject struct {
	CharacterId int64

	CharacterLevel int64

	CharacterLimitBreak int64

	CharacterSkillLevel int64

	Id int64

	WeaponId int64

	WeaponLevel int64

	WeaponSkillLevel int64
}
