package model

type EvolutionPlayerCharacterResponse struct {
	Gold int64

	ItemSummary []ItemSummaryArrayObject

	ManagedCharacter EvolutionPlayerCharacterResponseManagedCharacter
}
