package model

type SalePlayerRoomObjectResponse struct {
	ManagedRoomObjects []ManagedRoomObjectsArrayObject

	Player BasePlayer
}
