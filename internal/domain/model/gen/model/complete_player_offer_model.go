package model

type CompletePlayerOfferResponse struct {
	ItemSummary []ItemSummaryArrayObject

	ManagedMasterOrbs []ManagedMasterOrbsArrayObject

	OfferTitleTypes []OfferTitleTypesArrayObject

	// Offers []OffersArrayObject

	Player BasePlayer
}
