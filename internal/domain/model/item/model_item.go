package model_item

import (
	"gitlab.com/kirafan/sparkle/server/internal/domain/value"
	value_item "gitlab.com/kirafan/sparkle/server/internal/domain/value/item"
)

type Item struct {
	ItemId       uint `gorm:"primary_key"`
	ItemSortId   uint
	Name         string
	Description  string
	Category     value_item.ItemCategory
	Type         value_item.ItemType
	Rare         value_item.ItemRare
	SaleAmount   int32
	TypeArg1     int32
	TypeArg2     int32
	TypeArg3     int32
	TypeArg4     int32
	Appeal       value.BoolLikeUInt8
	IsEventBonus bool
}
