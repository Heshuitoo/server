package model_user

type ManagedMasterOrb struct {
	ManagedMasterOrbId uint `gorm:"primarykey"`
	// Foreign key
	UserId uint
	// Sometimes it can be -1 (why)
	Level int8
	// Default value is 0
	Exp         uint32
	MasterOrbId uint8
}
