package model_user

import "gorm.io/gorm"

// Cleared quests ( not adv Id since it conflicts to type)
type ClearedAdvId struct {
	gorm.Model
	UserId uint
	User   User
	AdvId  uint64
}
