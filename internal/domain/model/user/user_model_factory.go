package model_user

import (
	"fmt"
	"time"

	"github.com/google/uuid"
	value_character "gitlab.com/kirafan/sparkle/server/internal/domain/value/character"
	value_town_facility "gitlab.com/kirafan/sparkle/server/internal/domain/value/town_facility"
	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
	"gitlab.com/kirafan/sparkle/server/pkg/calc"
)

func NewInitialOfferTitleTypes() []OfferTitleType {
	var newOfferTitleTypes []OfferTitleType
	for i := 0; i < 38; i++ {
		newOfferTitleTypes = append(newOfferTitleTypes, OfferTitleType{
			TitleType:      value_character.TitleType(i),
			ContentRoomFlg: 1,
			Category:       -1,
			OfferIndex:     -1,
			OfferPoint:     -1,
			OfferMaxPoint:  0,
			State:          0,
			Shown:          0,
			SubState:       0,
			SubState1:      0,
			SubState2:      0,
			SubState3:      0,
		})
	}
	// Id:34 Manga-time content room was not implemented in app
	newOfferTitleTypes[34].ContentRoomFlg = 0
	return newOfferTitleTypes
}

func newInitialManagedMasterOrbs() []ManagedMasterOrb {
	var newManagedMasterOrbs []ManagedMasterOrb
	for i := 1; i < 6; i++ {
		newManagedMasterOrbs = append(newManagedMasterOrbs, ManagedMasterOrb{
			Level:       -1,
			Exp:         0,
			MasterOrbId: uint8(i),
		})
	}
	return newManagedMasterOrbs
}

func newInitialManagedBattleParties() []ManagedBattleParty {
	var newManagedBattleParties []ManagedBattleParty
	for i := 1; i < 16; i++ {
		newManagedBattleParties = append(newManagedBattleParties, ManagedBattleParty{
			Name:                "パーティー" + fmt.Sprint(i),
			CostLimit:           0,
			ManagedCharacterId1: -1,
			ManagedCharacterId2: -1,
			ManagedCharacterId3: -1,
			ManagedCharacterId4: -1,
			ManagedCharacterId5: -1,
			ManagedWeaponId1:    -1,
			ManagedWeaponId2:    -1,
			ManagedWeaponId3:    -1,
			ManagedWeaponId4:    -1,
			ManagedWeaponId5:    -1,
			MasterOrbId:         1,
		})
	}
	return newManagedBattleParties
}

func newInitialSupportCharacters() []SupportCharacter {
	var newSupportCharacters []SupportCharacter
	for i := 1; i < 4; i++ {
		newSupportCharacters = append(newSupportCharacters, SupportCharacter{
			Name: "サポートパーティー" + fmt.Sprint(i),
			ManagedCharacterIds: []SupportCharacterManagedCharacterId{
				{ManagedCharacterId: -1},
				{ManagedCharacterId: -1},
				{ManagedCharacterId: -1},
				{ManagedCharacterId: -1},
				{ManagedCharacterId: -1},
				{ManagedCharacterId: -1},
				{ManagedCharacterId: -1},
				{ManagedCharacterId: -1},
			},
			ManagedWeaponIds: []SupportCharacterManagedWeaponId{
				{ManagedWeaponId: -1},
				{ManagedWeaponId: -1},
				{ManagedWeaponId: -1},
				{ManagedWeaponId: -1},
				{ManagedWeaponId: -1},
				{ManagedWeaponId: -1},
				{ManagedWeaponId: -1},
				{ManagedWeaponId: -1},
			},
		})
	}
	// Support party 1 is active on create
	newSupportCharacters[0].Active = 1
	return newSupportCharacters
}

func newInitialManagedFacilities() []ManagedTownFacility {
	var newTownFacilities []ManagedTownFacility
	var facilityIds = []uint32{0, 1200, 1202, 1203, 1204, 1205}
	var buildPointIndexes = []value_town_facility.BuildPointIndex{0, 268436656, 268436658, 268436659, 268436660, 268436661}
	for i := range facilityIds {
		newTownFacilities = append(newTownFacilities, ManagedTownFacility{
			FacilityId:      uint32(facilityIds[i]),
			BuildPointIndex: buildPointIndexes[i],
			Level:           1,
			OpenState:       1,
			ActionTime:      0,
			BuildTime:       0,
		})
	}
	return newTownFacilities
}

func newInitialStepCode() value_user.StepCode {
	return value_user.StepCodeAccountCreated
}

func newInitialUserGachas() []UserGacha {
	now := time.Now()
	return []UserGacha{
		{
			CreatedAt:          now,
			UpdatedAt:          now,
			DeletedAt:          nil,
			Gem10CurrentStep:   1,
			PlayerDrawPoint:    0,
			Gem10Daily:         0,
			Gem10Total:         0,
			Gem1Daily:          0,
			Gem1Total:          0,
			UGem1Daily:         0,
			UGem1Total:         0,
			ItemDaily:          0,
			ItemTotal:          0,
			Gem1FreeDrawCount:  0,
			Gem10FreeDrawCount: 0,
			GachaId:            1,
		},
	}
}

// TODO: Refactor and retrieve mission and presents as param
func NewUser(deviceUUId string, name string) User {
	now := time.Now()
	return User{
		CreatedAt: now,
		UpdatedAt: now,
		DeletedAt: nil,
		// NOTE: Actual server had access token and session id as different field, but this server uses same field
		// Maybe no one cares security
		UUId:                     deviceUUId,
		Session:                  uuid.New().String(),
		Age:                      value_user.AgeUnknown,
		CharacterLimit:           0,
		CharacterWeaponCount:     0,
		Comment:                  nil,
		ContinuousDays:           1,
		CurrentAchievementId:     0,
		FacilityLimit:            10,
		FacilityLimitCount:       0,
		FriendLimit:              20,
		Gold:                     0,
		IpAddr:                   "1.1.1.1",
		ItemLimit:                0,
		Kirara:                   0,
		KiraraLimit:              10000,
		LastLoginAt:              now,
		LastPartyAdded:           nil,
		Level:                    1,
		LevelExp:                 0,
		UnlimitedGem:             0,
		LimitedGem:               0,
		LoginCount:               0,
		LoginDays:                0,
		LotteryTicket:            0,
		MyCode:                   calc.GetSparkleRandomString(),
		Name:                     name,
		PartyCost:                50,
		RecastTime:               0,
		RecastTimeMax:            300,
		RoomObjectLimit:          100,
		RoomObjectLimitCount:     0,
		Stamina:                  10,
		StaminaMax:               10,
		StaminaUpdatedAt:         now,
		State:                    1,
		SupportLimit:             6,
		TotalExp:                 0,
		UserAgent:                "app/0.0.0; iOS 12.1.2; iPhone6S; iPhone8,1",
		WeaponLimit:              10,
		WeaponLimitCount:         0,
		ItemSummary:              []ItemSummary{},
		SupportCharacters:        newInitialSupportCharacters(),
		ManagedBattleParties:     newInitialManagedBattleParties(),
		ManagedCharacters:        []ManagedCharacter{},
		ManagedNamedTypes:        []ManagedNamedType{},
		ManagedFieldPartyMembers: []ManagedFieldPartyMember{},
		ManagedTowns:             []ManagedTown{},
		ManagedFacilities:        newInitialManagedFacilities(),
		ManagedRoomObjects:       NewInitialManagedRoomObjects(),
		ManagedWeapons:           []ManagedWeapon{},
		// Below field must be specified after the room object primary keys defined...
		ManagedRooms:         []ManagedRoom{},
		ManagedMasterOrbs:    newInitialManagedMasterOrbs(),
		ManagedAbilityBoards: []ManagedAbilityBoard{},
		FavoriteMembers:      []FavoriteMember{},
		OfferTitleTypes:      NewInitialOfferTitleTypes(),
		AdvIds:               []ClearedAdvId{},
		TipIds:               []ShownTipId{},
		FlagUi:               1,
		FlagPush:             1,
		FlagStamina:          1,
		IsNewProduct:         0,
		IsCloseInfo:          0,
		TrainingCount:        0,
		PresentCount:         0,
		FriendProposedCount:  0,
		NewAchievementCount:  0,
		// Below value is used for new account tutorial flow (eTutorialSeq)
		StepCode:         newInitialStepCode(),
		LastMemberAdded:  now,
		LatestQuestLogID: -1,
		// Main story last screen ids
		LastOpenedPart1ChapterId:      -1,
		LastOpenedPart2ChapterId:      -1,
		LastPlayedPart1ChapterQuestId: -1,
		LastPlayedPart2ChapterQuestId: -1,
		QuestLogs:                     []QuestLog{},
		QuestAccesses:                 []QuestAccess{},
		Missions:                      []UserMission{},
		Presents:                      []UserPresent{},
		Gachas:                        newInitialUserGachas(),
		LoginBonuses:                  []UserLoginBonus{},
		LastLoginBonus:                nil,
		MoveCode:                      "",
		MoveDeadline:                  now,
		MovePasswordSalt:              "",
	}
}

// Create new user with specify session id (only for testing)
func NewUserWithSpecifySession(deviceUUId string, sessionUUId string, name string) User {
	user := NewUser(deviceUUId, name)
	user.Session = sessionUUId
	return user
}
