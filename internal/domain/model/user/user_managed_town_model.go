package model_user

type ManagedTown struct {
	ManagedTownId uint `gorm:"primarykey"`
	// Foreign key
	UserId   uint
	GridData string
}
