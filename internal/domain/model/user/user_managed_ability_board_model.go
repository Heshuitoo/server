package model_user

import "gorm.io/gorm"

type ManagedAbilityBoard struct {
	ManagedAbilityBoardId uint `gorm:"primarykey"`
	// Foreign Key
	UserId uint
	// TODO: find what does this field mean
	AbilityBoardId uint32
	// Foreign Key (TODO: Add the key to migration)
	ManagedCharacterId int
	// TODO: make value object for this field
	EquipItemIds []ManagedAbilityBoardEquipItemId
}

type ManagedAbilityBoardEquipItemId struct {
	gorm.Model
	// Foreign Key (TODO: Add the key to migration)
	ManagedAbilityBoardId uint
	// TODO: make value object for this field
	ItemId uint32
}
