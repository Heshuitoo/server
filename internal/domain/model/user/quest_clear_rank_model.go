package model_user

import (
	value_quest "gitlab.com/kirafan/sparkle/server/internal/domain/value/quest"
)

type QuestClearRank struct {
	QuestId   uint
	ClearRank value_quest.ClearRank
}
