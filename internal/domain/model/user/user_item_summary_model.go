package model_user

import "time"

type ItemSummary struct {
	ItemSummaryId uint `gorm:"primarykey"`
	CreatedAt     time.Time
	UpdatedAt     time.Time
	Amount        uint32
	// ItemId
	Id uint32
	// Foreign key
	UserId uint
}
