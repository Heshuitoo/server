package model_user

import (
	"errors"
	"time"

	model_level_table "gitlab.com/kirafan/sparkle/server/internal/domain/model/level_table"
	"gitlab.com/kirafan/sparkle/server/internal/domain/value"
	value_character "gitlab.com/kirafan/sparkle/server/internal/domain/value/character"
	value_login_bonus "gitlab.com/kirafan/sparkle/server/internal/domain/value/login_bonus"
	value_mission "gitlab.com/kirafan/sparkle/server/internal/domain/value/mission"
	value_present "gitlab.com/kirafan/sparkle/server/internal/domain/value/present"
	value_quest "gitlab.com/kirafan/sparkle/server/internal/domain/value/quest"
	value_room "gitlab.com/kirafan/sparkle/server/internal/domain/value/room"
	value_town_facility "gitlab.com/kirafan/sparkle/server/internal/domain/value/town_facility"
	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
	value_weapon "gitlab.com/kirafan/sparkle/server/internal/domain/value/weapon"
	"gitlab.com/kirafan/sparkle/server/pkg/auth"
	"gitlab.com/kirafan/sparkle/server/pkg/calc"
	"gitlab.com/kirafan/sparkle/server/pkg/parser"
)

type QuestAccess struct {
	Id uint `gorm:"primarykey"`
	// foreignKey
	UserId    uint
	QuestId   uint
	CreatedAt time.Time
	UpdatedAt time.Time
}

type User struct {
	Id        uint `gorm:"primarykey"`
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt *time.Time
	/** Login user key */
	UUId string `gorm:"type:varchar(36);unique_index"`
	/** Login permanent token (treat as session also) */
	Session              string `gorm:"type:varchar(36);unique_index"`
	Age                  value_user.Age
	CharacterLimit       uint16
	CharacterWeaponCount uint16
	Comment              *string
	ContinuousDays       uint16
	CurrentAchievementId uint64
	FacilityLimit        uint16
	FacilityLimitCount   uint16
	FriendLimit          uint16
	Gold                 uint64
	IpAddr               string
	ItemLimit            uint32
	Kirara               uint32
	KiraraLimit          uint32
	LastLoginAt          time.Time
	LastPartyAdded       *time.Time
	Level                uint16
	LevelExp             uint64
	TotalExp             uint64
	UnlimitedGem         uint32
	LimitedGem           uint32
	LoginCount           uint32
	LoginDays            uint16
	LotteryTicket        uint32
	MyCode               string `gorm:"type:varchar(10);unique_index"`
	Name                 string
	PartyCost            uint16
	RecastTime           uint16
	RecastTimeMax        uint16
	RoomObjectLimit      uint16
	RoomObjectLimitCount uint16
	Stamina              uint32
	StaminaMax           uint32
	StaminaUpdatedAt     time.Time
	SupportLimit         uint8
	UserAgent            string
	WeaponLimit          uint16
	WeaponLimitCount     uint16
	// Unused field
	State uint8
	// User inventories
	ItemSummary              []ItemSummary             `gorm:"PRELOAD:false"`
	SupportCharacters        []SupportCharacter        `gorm:"PRELOAD:false"`
	ManagedBattleParties     []ManagedBattleParty      `gorm:"PRELOAD:false"`
	ManagedCharacters        []ManagedCharacter        `gorm:"foreignKey:PlayerId;PRELOAD:false"`
	ManagedNamedTypes        []ManagedNamedType        `gorm:"PRELOAD:false"`
	ManagedFieldPartyMembers []ManagedFieldPartyMember `gorm:"PRELOAD:false"`
	ManagedTowns             []ManagedTown             `gorm:"PRELOAD:false"`
	ManagedFacilities        []ManagedTownFacility     `gorm:"PRELOAD:false"`
	ManagedRoomObjects       []ManagedRoomObject       `gorm:"PRELOAD:false"`
	ManagedWeapons           []ManagedWeapon           `gorm:"PRELOAD:false"`
	ManagedRooms             []ManagedRoom             `gorm:"PRELOAD:false"`
	ManagedMasterOrbs        []ManagedMasterOrb        `gorm:"PRELOAD:false"`
	ManagedAbilityBoards     []ManagedAbilityBoard     `gorm:"PRELOAD:false"`
	FavoriteMembers          []FavoriteMember          `gorm:"PRELOAD:false"`
	OfferTitleTypes          []OfferTitleType          `gorm:"PRELOAD:false"`
	// Simple value array
	AdvIds []ClearedAdvId `gorm:"PRELOAD:false"`
	TipIds []ShownTipId   `gorm:"PRELOAD:false"`
	// App notify info
	FlagPush    value.BoolLikeUInt8
	FlagUi      value.BoolLikeUInt8
	FlagStamina value.BoolLikeUInt8
	PushToken   string
	// UI notify
	IsNewProduct uint8
	IsCloseInfo  uint8
	// TODO: make these fields refer other tables instead column
	TrainingCount       uint8
	PresentCount        uint8
	FriendProposedCount uint8
	NewAchievementCount uint8
	// New user tutorial flow
	StepCode value_user.StepCode
	// On-going quest info
	LatestQuestLogID int `gorm:"DEFAULT:-1"`
	// Maybe new character (non-duplicated) added time?
	// TODO: Find how this value used in the game
	LastMemberAdded time.Time
	// (moved from quest/get_all)
	LastOpenedPart1ChapterId      int8
	LastOpenedPart2ChapterId      int8
	LastPlayedPart1ChapterQuestId int32
	LastPlayedPart2ChapterQuestId int32
	QuestLogs                     []QuestLog    `gorm:"PRELOAD:false"`
	QuestAccesses                 []QuestAccess `gorm:"PRELOAD:false"`
	// (moved from mission/get_all)
	Missions []UserMission `gorm:"PRELOAD:false"`
	// (moved from present/get_all)
	Presents []UserPresent `gorm:"PRELOAD:false"`
	// (moved from gacha/get_all)
	Gachas []UserGacha `gorm:"PRELOAD:false"`
	// (moved from achievement/get_all)
	Achievements []UserAchievement `gorm:"PRELOAD:false"`
	// (moved from login_bonus/get)
	LoginBonuses   []UserLoginBonus `gorm:"PRELOAD:false"`
	LastLoginBonus *string
	// (moved from player/move)
	MoveCode         string
	MoveDeadline     time.Time
	MovePasswordSalt string
}

// Error codes

var ErrUserTutorialTipAlreadyExists = errors.New("tutorial tip already exists")
var ErrUserClearedAdvAlreadyExists = errors.New("cleared adv already exists")
var ErrUserNamedTypeAlreadyExists = errors.New("named type already exists")
var ErrUserNamedTypeNotFound = errors.New("named type not found")
var ErrUserCharacterNotFound = errors.New("specified character was not found")
var ErrUserQuestAccessAlreadyExists = errors.New("quest access already exists")
var ErrUserGachaAlreadyExists = errors.New("gacha already exists")
var ErrUserPresentNotFound = errors.New("present not found")
var ErrUserPresentInvalidCharacterId = errors.New("present objectId(characterId) is not valid")
var ErrUserWeaponLimitExceed = errors.New("weapon limit reached")
var ErrUserRoomObjectLimitExceed = errors.New("room object limit reached")
var ErrUserKiraraLimitExceed = errors.New("kirara limit reached")
var ErrUserPresentNotImplemented = errors.New("not implemented present type")
var ErrUserManagedFacilityNotFound = errors.New("managed facility not found")
var ErrUserManagedWeaponNotFound = errors.New("managed weapon not found")
var ErrUserInvalidStepCode = errors.New("invalid step code")
var ErrUserRoomNotFound = errors.New("invalid room specified or not found")
var ErrUserRoomObjectNotFound = errors.New("invalid room object specified or not found")
var ErrUserWeaponSpecialWeaponAlreadyExists = errors.New("specified special weapon already exists")

// Tutorial feature

func (u *User) AddTutorialTip(tip uint32) error {
	for i := range u.TipIds {
		if u.TipIds[i].TipId == tip {
			return ErrUserTutorialTipAlreadyExists
		}
	}
	newTip := ShownTipId{
		UserId: u.Id,
		TipId:  tip,
	}
	u.TipIds = append(u.TipIds, newTip)
	return nil
}

func (u *User) UpdateStepCode(step value_user.StepCode) error {
	if step == value_user.StepCodeUnused0 || step == value_user.StepCodeUnused4 {
		return ErrUserInvalidStepCode
	}
	if step != value_user.StepCodeTutorialDone && u.StepCode == value_user.StepCodeTutorialDone {
		return ErrUserInvalidStepCode
	}
	if step < u.StepCode && step != value_user.StepCodeTutorialDone {
		return ErrUserInvalidStepCode
	}
	u.StepCode = step
	return nil
}

// Quest feature

func (u *User) HasClearedAdv(advId uint64) bool {
	for i := range u.AdvIds {
		if u.AdvIds[i].AdvId == advId {
			return true
		}
	}
	return false
}

func (u *User) AddClearedAdv(advId uint64) error {
	for _, v := range u.AdvIds {
		if v.AdvId == advId {
			return ErrUserClearedAdvAlreadyExists
		}
	}
	newAdv := ClearedAdvId{
		UserId: u.Id,
		AdvId:  advId,
	}
	u.AdvIds = append(u.AdvIds, newAdv)
	return nil
}

func (u *User) AddQuestLog(questId uint, clearRank value_quest.ClearRank) {
	for i := range u.QuestLogs {
		if u.QuestLogs[i].QuestId == questId {
			u.QuestLogs[i].ClearRank = clearRank
			return
		}
	}
	newQuestLog := QuestLog{
		UserId:    u.Id,
		QuestId:   questId,
		ClearRank: clearRank,
		IsRead:    1,
	}
	u.QuestLogs = append(u.QuestLogs, newQuestLog)
}

func (u *User) AddQuestAccess(questId uint) error {
	for _, v := range u.QuestAccesses {
		if v.QuestId == questId {
			return ErrUserQuestAccessAlreadyExists
		}
	}
	newQuestAccess := QuestAccess{
		UserId:  u.Id,
		QuestId: questId,
	}
	u.QuestAccesses = append(u.QuestAccesses, newQuestAccess)
	return nil
}

// Weapon feature

func (u *User) ExtendManagedWeaponLimit() {
	// TODO: Move this static value to config
	u.WeaponLimit += 10
	u.WeaponLimitCount++
}

func (u *User) GetManagedWeaponByWeaponId(weaponId value_weapon.WeaponId) (*ManagedWeapon, error) {
	for i := range u.ManagedWeapons {
		if u.ManagedWeapons[i].WeaponId == weaponId {
			return &u.ManagedWeapons[i], nil
		}
	}
	return nil, ErrUserManagedWeaponNotFound
}

func (u *User) GetManagedWeapon(managedWeaponId value_user.ManagedWeaponId) (*ManagedWeapon, error) {
	for i := range u.ManagedWeapons {
		if u.ManagedWeapons[i].ManagedWeaponId == managedWeaponId {
			return &u.ManagedWeapons[i], nil
		}
	}
	return nil, ErrUserManagedWeaponNotFound
}

func (u *User) HasWeapon(weaponId value_weapon.WeaponId) bool {
	for _, weapon := range u.ManagedWeapons {
		if weapon.WeaponId == weaponId {
			return true
		}
	}
	return false
}

func (u *User) AddWeapon(weaponId value_weapon.WeaponId, isSpecialWeapon bool) error {
	weaponCount := len(u.ManagedWeapons)
	if uint16(weaponCount)+1 > u.WeaponLimit && !isSpecialWeapon {
		return ErrUserWeaponLimitExceed
	}
	// NOTE: Special weapon can not be duplicated
	if u.HasWeapon(weaponId) && isSpecialWeapon {
		return ErrUserWeaponSpecialWeaponAlreadyExists
	}
	newWeapon := newManagedWeapon(u.Id, weaponId)
	u.ManagedWeapons = append(u.ManagedWeapons, newWeapon)
	return nil
}

func (u *User) RemoveWeapon(managedWeaponId value_user.ManagedWeaponId) bool {
	for index := 0; index < len(u.ManagedWeapons); index++ {
		if u.ManagedWeapons[index].ManagedWeaponId != managedWeaponId {
			continue
		}
		u.ManagedWeapons = append(u.ManagedWeapons[:index], u.ManagedWeapons[index+1:]...)
		return true
	}
	return false
}

func (u *User) AddWeaponSkillExps(weaponExps map[uint64]parser.SkillResult) {
	for i := range u.ManagedWeapons {
		for j := range weaponExps {
			managedWeaponId := value_user.NewManagedWeaponId(j)
			if u.ManagedWeapons[i].ManagedWeaponId == managedWeaponId {
				u.ManagedWeapons[i].AddWeaponSkillExp(uint32(weaponExps[j].Skill1))
			}
		}
	}
}

// Support party features

func (u *User) GetSupportCharacter(managedSupportId value_user.ManagedSupportId) (*SupportCharacter, error) {
	for i := range u.SupportCharacters {
		if u.SupportCharacters[i].ManagedSupportId == managedSupportId {
			return &u.SupportCharacters[i], nil
		}
	}
	return nil, ErrUserManagedFacilityNotFound
}

// Inventory features

func (u *User) AddItem(itemId uint32, count uint32) {
	for i, item := range u.ItemSummary {
		if item.Id == itemId {
			u.ItemSummary[i].Amount += count
			return
		}
	}
	newItem := ItemSummary{
		UserId: u.Id,
		Id:     itemId,
		Amount: count,
	}
	u.ItemSummary = append(u.ItemSummary, newItem)
}

func (u *User) AddGold(count uint64) {
	u.Gold += count
}

// User value features

func (u *User) AddRankExp(amount uint64) {
	u.TotalExp += amount
}

func (u *User) UpdateRank(newRank uint16) {
	u.Level = calc.Max(u.Level, newRank)
}

func (u *User) AddKirara(amount uint32) error {
	if u.Kirara+amount > u.KiraraLimit {
		return ErrUserKiraraLimitExceed
	}
	u.Kirara += amount
	return nil
}

func (u *User) AddLimitedGem(amount uint32) {
	u.LimitedGem += amount
}

func (u *User) AddUnlimitedGem(amount uint32) {
	u.UnlimitedGem += amount
}

func (u *User) AddCharacter(characterId value_character.CharacterId) bool {
	for i := range u.ManagedCharacters {
		if u.ManagedCharacters[i].CharacterId == characterId {
			u.ManagedCharacters[i].IncreaseDuplicatedCount()
			return true
		}
	}
	newCharacter := newManagedCharacter(u.Id, characterId)
	u.ManagedCharacters = append(u.ManagedCharacters, newCharacter)
	return false
}

func (u *User) GetCharacterIdByManagedCharacterId(managedCharacterId value_user.ManagedCharacterId) (*value_character.CharacterId, error) {
	for i := range u.ManagedCharacters {
		if u.ManagedCharacters[i].ManagedCharacterId == managedCharacterId {
			return &u.ManagedCharacters[i].CharacterId, nil
		}
	}
	return nil, ErrUserCharacterNotFound
}

func (u *User) GetManagedCharacter(managedCharacterId value_user.ManagedCharacterId) (*ManagedCharacter, error) {
	for i := range u.ManagedCharacters {
		if u.ManagedCharacters[i].ManagedCharacterId == managedCharacterId {
			return &u.ManagedCharacters[i], nil
		}
	}
	return nil, ErrUserManagedFacilityNotFound
}

func (u *User) AddCharacterExps(exp uint64, managedCharacterIds []uint64) {
	for i := range u.ManagedCharacters {
		if calc.Contains(managedCharacterIds, uint64(u.ManagedCharacters[i].ManagedCharacterId)) {
			u.ManagedCharacters[i].AddExp(exp)
		}
	}
}

func (u *User) AddCharacterSkillExps(characterExps map[uint64]parser.SkillResult) {
	for i := range u.ManagedCharacters {
		for j, characterExp := range characterExps {
			if j == uint64(u.ManagedCharacters[i].ManagedCharacterId) {
				u.ManagedCharacters[i].AddSkillExp(uint32(characterExp.Skill1), SkillType1)
				u.ManagedCharacters[i].AddSkillExp(uint32(characterExp.Skill2), SkillType2)
				u.ManagedCharacters[i].AddSkillExp(uint32(characterExp.Skill3), SkillType3)
			}
		}
	}
}

func (u *User) AddGacha(gachaId uint) error {
	for i := range u.Gachas {
		if u.Gachas[i].GachaId == gachaId {
			return ErrUserGachaAlreadyExists
		}
	}
	newGacha := NewUserGacha(u.Id, gachaId)
	u.Gachas = append(u.Gachas, newGacha)
	return nil
}

// Add friendship of character
func (u *User) AddNamedType(namedType uint16, titleType value_character.TitleType) error {
	for i := range u.ManagedNamedTypes {
		if u.ManagedNamedTypes[i].NamedType == namedType {
			return ErrUserNamedTypeAlreadyExists
		}
	}
	newManagedNamedType, err := newManagedNamedType(u.Id, namedType, titleType)
	if err != nil {
		return err
	}
	u.ManagedNamedTypes = append(u.ManagedNamedTypes, newManagedNamedType)
	return nil
}

func (u *User) AddNamedTypeExp(namedType uint16, exp uint32) error {
	for i := range u.ManagedNamedTypes {
		if u.ManagedNamedTypes[i].NamedType == namedType {
			u.ManagedNamedTypes[i].AddExp(exp)
			return nil
		}
	}
	return ErrUserNamedTypeNotFound
}

func (u *User) AddNamedTypeExps(exp uint32, managedNamedTypeIds []uint64) {
	for i := range u.ManagedNamedTypes {
		if calc.Contains(managedNamedTypeIds, uint64(u.ManagedNamedTypes[i].ManagedNamedTypeId)) {
			u.ManagedNamedTypes[i].AddExp(exp)
		}
	}
}

// Town feature
func (u *User) UpdateTown(gridData string) {
	if len(u.ManagedTowns) == 0 {
		u.ManagedTowns = []ManagedTown{
			{
				UserId:   u.Id,
				GridData: gridData,
			},
		}
	} else {
		u.ManagedTowns[0].GridData = gridData
	}
}

// Town facility feature

func (u *User) ExtendManagedFacilityLimit() {
	// TODO: Move this static value to config
	u.FacilityLimit += 10
	u.FacilityLimitCount++
}

func (u *User) GetManagedFacility(managedTownFacilityId uint32) (*ManagedTownFacility, error) {
	for _, facility := range u.ManagedFacilities {
		if uint32(facility.ManagedTownFacilityId) == managedTownFacilityId {
			return &facility, nil
		}
	}
	return nil, ErrUserManagedFacilityNotFound
}

func (u *User) AddManagedFacility(facilityId uint32, buildPointIndex int32, level uint8, actionTime uint64, buildTime uint64, openState uint8) error {
	newManagedFacility, err := newManagedTownFacility(
		u.Id,
		facilityId,
		buildPointIndex,
		level,
		actionTime,
		buildTime,
		openState,
	)
	if err != nil {
		return err
	}
	u.ManagedFacilities = append(u.ManagedFacilities, *newManagedFacility)
	return nil
}

func (u *User) RemoveManagedFacility(managedTownFacilityId uint32) error {
	for i := range u.ManagedFacilities {
		if uint32(u.ManagedFacilities[i].ManagedTownFacilityId) == managedTownFacilityId {
			u.ManagedFacilities = append(u.ManagedFacilities[:i], u.ManagedFacilities[i+1:]...)
			return nil
		}
	}
	return ErrUserManagedFacilityNotFound
}

func (u *User) LevelUpManagedFacility(
	managedTownFacilityId uint32,
	newLevel uint8,
	actionTime uint64,
	openState value_town_facility.TownFacilityOpenState,
) error {
	for i := range u.ManagedFacilities {
		if uint32(u.ManagedFacilities[i].ManagedTownFacilityId) == managedTownFacilityId {
			u.ManagedFacilities[i].Level = newLevel
			u.ManagedFacilities[i].ActionTime = actionTime
			u.ManagedFacilities[i].OpenState = openState
			return nil
		}
	}
	return ErrUserManagedFacilityNotFound
}

func (u *User) OpenUpManagedFacility(
	managedTownFacilityId uint32,
	newLevel uint8,
	buildTime uint64,
	openState value_town_facility.TownFacilityOpenState,
) error {
	for i := range u.ManagedFacilities {
		if uint32(u.ManagedFacilities[i].ManagedTownFacilityId) == managedTownFacilityId {
			u.ManagedFacilities[i].Level = newLevel
			u.ManagedFacilities[i].BuildTime = buildTime
			u.ManagedFacilities[i].OpenState = openState
			return nil
		}
	}
	return ErrUserManagedFacilityNotFound
}

func (u *User) UpdateManagedFacilityActionTime(managedTownFacilityId uint32, actionTime uint64) error {
	for i := range u.ManagedFacilities {
		if uint32(u.ManagedFacilities[i].ManagedTownFacilityId) == managedTownFacilityId {
			u.ManagedFacilities[i].ActionTime = actionTime
			return nil
		}
	}
	return ErrUserManagedFacilityNotFound
}

func (u *User) MoveManagedTownFacility(
	managedTownFacilityId uint32,
	buildPointIndex value_town_facility.BuildPointIndex,
	buildTime uint64,
	openState value_town_facility.TownFacilityOpenState,
) error {
	for i := range u.ManagedFacilities {
		if uint32(u.ManagedFacilities[i].ManagedTownFacilityId) == managedTownFacilityId {
			u.ManagedFacilities[i].BuildPointIndex = buildPointIndex
			u.ManagedFacilities[i].BuildTime = buildTime
			u.ManagedFacilities[i].OpenState = openState
			return nil
		}
	}
	return ErrUserManagedFacilityNotFound
}

// Mission feature

func (u *User) ProgressMission(managedMissionId uint, rate uint) error {
	var targetMission *UserMission
	for _, mission := range u.Missions {
		if mission.ManagedMissionId == managedMissionId {
			targetMission = &mission
			break
		}
	}
	if targetMission != nil {
		targetMission.Rate = rate
	}
	return nil
}

func (u *User) CompleteMission(managedMissionId uint) error {
	var targetMission *UserMission
	for _, mission := range u.Missions {
		if mission.ManagedMissionId == managedMissionId {
			targetMission = &mission
			break
		}
	}
	if targetMission != nil {
		targetMission.State = value_mission.MissionStateCleared
	}
	return nil
}

func (u *User) ReceiveLoginBonuses() *[]UserLoginBonus {
	// Validate login bonuses count
	if len(u.LoginBonuses) == 0 {
		return nil
	}
	// Validate last login bonus received date
	lastUpdate := u.LastLoginAt.Day()
	today := time.Now().Day()
	if lastUpdate == today {
		return nil
	}

	receivedBonuses := []UserLoginBonus{}
	newUserLoginBonuses := []UserLoginBonus{}
	for i := range u.LoginBonuses {
		// Receive today's login bonus
		day := u.LoginBonuses[i].LoginBonusDayIndex
		bonuses := u.LoginBonuses[i].LoginBonus.BonusDays[day].BonusItems
		for _, bonus := range bonuses {
			if bonus.Amount == 0 {
				continue
			}
			switch bonus.Type {
			case value_login_bonus.LoginBonusDayPresentTypeItem:
				u.AddItem(uint32(bonus.ObjId), uint32(bonus.Amount))
			case value_login_bonus.LoginBonusDayPresentTypeLimitedGem:
				u.AddLimitedGem(uint32(bonus.Amount))
			default:
				continue
			}
		}
		receivedBonuses = append(receivedBonuses, u.LoginBonuses[i])
		// Increase login bonus day index and reset or remove the login bonus
		u.LoginBonuses[i].LoginBonusDayIndex++
		currentDayIndex := int(u.LoginBonuses[i].LoginBonusDayIndex)
		bonusDayLength := len(u.LoginBonuses[i].LoginBonus.BonusDays)
		if currentDayIndex >= bonusDayLength {
			if u.LoginBonuses[i].LoginBonus.ImageId != value_login_bonus.LoginBonusImageNormal {
				continue
			}
			u.LoginBonuses[i].LoginBonusDayIndex = 0
		}
		newUserLoginBonuses = append(newUserLoginBonuses, u.LoginBonuses[i])
	}
	u.LoginBonuses = newUserLoginBonuses
	u.LastLoginAt = time.Now()
	return &receivedBonuses
}

func (u *User) GetManagedPresent(managedPresentId uint) (*UserPresent, error) {
	for i := range u.Presents {
		if u.Presents[i].ManagedPresentId == managedPresentId && u.Presents[i].ReceivedAt == nil {
			return &u.Presents[i], nil
		}
	}
	return nil, ErrUserPresentNotFound
}

func (u *User) UpdateManagedPresentReceivedAt(managedPresentId uint) error {
	for i := range u.Presents {
		if u.Presents[i].ManagedPresentId == managedPresentId {
			now := time.Now()
			u.Presents[i].ReceivedAt = &now
			return nil
		}
	}
	return ErrUserPresentNotFound
}

func (u *User) ReceivePresent(managedPresentId uint) (*UserPresent, error) {
	targetPresent, err := u.GetManagedPresent(managedPresentId)
	if err != nil {
		return nil, ErrUserPresentNotFound
	}

	switch targetPresent.Type {
	case value_present.PresentTypeItem:
		u.AddItem(uint32(targetPresent.ObjectId), uint32(targetPresent.Amount))
	case value_present.PresentTypeCharacter:
		characterId, err := value_character.NewCharacterId(uint32(targetPresent.ObjectId))
		if err != nil {
			return nil, ErrUserPresentInvalidCharacterId
		}
		u.AddCharacter(characterId)
	case value_present.PresentTypeWeapon:
		weaponId := value_weapon.NewWeaponId(targetPresent.ObjectId)
		if err := u.AddWeapon(weaponId, false); err != nil {
			return nil, err
		}
	case value_present.PresentTypeKiraraPoint:
		if err := u.AddKirara(uint32(targetPresent.Amount)); err != nil {
			return nil, err
		}
	case value_present.PresentTypeLimitedGem:
		u.LimitedGem += uint32(targetPresent.Amount)
	case value_present.PresentTypeUnlimitedGem:
		u.UnlimitedGem += uint32(targetPresent.Amount)
	case value_present.PresentTypeGold:
		u.Gold += uint64(targetPresent.Amount)
	case value_present.PresentTypeNone:
		fallthrough
	case value_present.PresentTypeTownFacility:
		fallthrough
	case value_present.PresentTypeRoomObject:
		fallthrough
	case value_present.PresentTypeMasterOrb:
		fallthrough
	case value_present.PresentTypePackageItem:
		fallthrough
	case value_present.PresentTypeAllowRoomObject:
		fallthrough
	case value_present.PresentTypeMasterOrbLvUp:
		fallthrough
	case value_present.PresentTypeAchievement:
		return nil, ErrUserPresentNotImplemented
	}

	u.UpdateManagedPresentReceivedAt(managedPresentId)

	return targetPresent, nil
}

// Item utility features

func (u *User) HasItem(itemId uint32) bool {
	for _, item := range u.ItemSummary {
		if item.Id == itemId {
			return true
		}
	}
	return false
}

func (u *User) HasItemMoreThan(itemId uint32, count uint32) bool {
	for _, item := range u.ItemSummary {
		if item.Id == itemId {
			return item.Amount >= count
		}
	}
	return false
}

func (u *User) ConsumeItem(itemId uint32, count uint32) bool {
	for index := 0; index < len(u.ItemSummary); index++ {
		if u.ItemSummary[index].Id != itemId {
			continue
		}
		if u.ItemSummary[index].Amount < count {
			return false
		}
		u.ItemSummary[index].Amount -= count
		if u.ItemSummary[index].Amount == 0 {
			u.ItemSummary = append(u.ItemSummary[:index], u.ItemSummary[index+1:]...)
		}
		return true
	}
	return false
}

// Consume features
func (u *User) ConsumeGem(count uint32) bool {
	if (u.UnlimitedGem + u.LimitedGem) < count {
		return false
	}
	if u.LimitedGem >= count {
		u.LimitedGem -= count
		return true
	}
	if u.LimitedGem > 0 {
		count -= u.LimitedGem
		u.LimitedGem = 0
	}
	u.UnlimitedGem -= count
	return true
}

func (u *User) ConsumeUnlimitedGem(count uint32) bool {
	if u.UnlimitedGem < count {
		return false
	}
	u.UnlimitedGem -= count
	return true
}

func (u *User) ConsumeLimitedGem(count uint32) bool {
	if u.LimitedGem < count {
		return false
	}
	u.LimitedGem -= count
	return true
}

// Stamina features

func (u *User) RefreshStamina() {
	lastUpdatedAt := u.StaminaUpdatedAt
	currentTime := time.Now()
	if u.Stamina >= u.StaminaMax {
		u.StaminaUpdatedAt = currentTime
		return
	}
	// Diff in seconds
	diff := currentTime.Sub(lastUpdatedAt).Seconds()
	increaseAmount := uint32(diff) / uint32(u.RecastTimeMax)
	if increaseAmount > 0 {
		u.Stamina = calc.Min(u.Stamina+increaseAmount, u.StaminaMax)
	}
	// Calculate remaining time until next stamina recovery
	if u.RecastTime != 0 && int32(diff) != 0 {
		if int32(u.RecastTimeMax)-int32(diff)%int32(u.RecastTime) > 0 {
			u.RecastTime = u.RecastTimeMax - uint16(diff)%u.RecastTime
		} else {
			u.RecastTime = 0
		}
	}
	u.StaminaUpdatedAt = currentTime
}

// Heal user's stamina 100% of max stamina
func (u *User) AddStaminaGold() {
	u.Stamina += u.StaminaMax
}

// Heal user's stamina 50% of max stamina
func (u *User) AddStaminaSilver() {
	u.Stamina += u.StaminaMax / 2
}

// Heal user's stamina 10% of max stamina
func (u *User) AddStaminaBronze() {
	u.Stamina += u.StaminaMax / 10
}

func (u *User) ConsumeStamina(count uint32) bool {
	if u.Stamina < count {
		return false
	}
	u.Stamina -= count
	return true
}

func (u *User) ConsumeKirara(count uint32) bool {
	if u.Kirara < count {
		return false
	}
	u.Kirara -= count
	return true
}

func (u *User) ConsumeGold(count uint64) bool {
	if u.Gold < count {
		return false
	}
	u.Gold -= count
	return true
}

func (u *User) ConsumeTownFacilityCosts(levelFacility *model_level_table.LevelTableTownFacility) bool {
	consumed := u.ConsumeGold(uint64(levelFacility.GoldAmountBuy))
	if !consumed {
		return false
	}
	consumed = u.ConsumeKirara(levelFacility.KiraraPointAmountBuy)
	return consumed
}

// Room features

func (u *User) AddRoomObject(roomObjectId uint32) error {
	roomObjectCount := len(u.ManagedRoomObjects)
	if uint16(roomObjectCount) >= u.RoomObjectLimit {
		return ErrUserRoomObjectLimitExceed
	}
	newRoomObject := ManagedRoomObject{
		UserId:       u.Id,
		RoomObjectId: roomObjectId,
	}
	u.ManagedRoomObjects = append(u.ManagedRoomObjects, newRoomObject)
	return nil
}

func (u *User) HasRoomObject(managedRoomObjectId int, roomObjectId uint32) bool {
	for i := range u.ManagedRoomObjects {
		if u.ManagedRoomObjects[i].ManagedRoomObjectId == managedRoomObjectId &&
			u.ManagedRoomObjects[i].RoomObjectId == roomObjectId {
			return true
		}
	}
	return false
}

func (u *User) GetRoomObject(managedRoomObjectId uint) (*ManagedRoomObject, error) {
	for i := range u.ManagedRoomObjects {
		if u.ManagedRoomObjects[i].ManagedRoomObjectId == int(managedRoomObjectId) {
			return &u.ManagedRoomObjects[i], nil
		}
	}
	return nil, ErrUserRoomNotFound
}

func (u *User) RemoveRoomObject(managedRoomObjectId int, roomObjectId uint32) error {
	for i := range u.ManagedRoomObjects {
		if u.ManagedRoomObjects[i].ManagedRoomObjectId == managedRoomObjectId &&
			u.ManagedRoomObjects[i].RoomObjectId == roomObjectId {
			u.ManagedRoomObjects = append(u.ManagedRoomObjects[:i], u.ManagedRoomObjects[i+1:]...)
			return nil
		}
	}
	return ErrUserRoomObjectNotFound
}

func (u *User) HasManagedRoom(managedRoomId uint) bool {
	for i := range u.ManagedRooms {
		if u.ManagedRooms[i].ManagedRoomId == managedRoomId {
			return true
		}
	}
	return false
}

func (u *User) GetManagedRoom(managedRoomId uint) (*ManagedRoom, error) {
	for i := range u.ManagedRooms {
		if u.ManagedRooms[i].ManagedRoomId == managedRoomId {
			return &u.ManagedRooms[i], nil
		}
	}
	return nil, ErrUserRoomNotFound
}

func (u *User) GetManagedRoomByFloorId(floorId value_room.FloorId) (*ManagedRoom, error) {
	for i := range u.ManagedRooms {
		if u.ManagedRooms[i].FloorId == floorId {
			return &u.ManagedRooms[i], nil
		}
	}
	return nil, ErrUserRoomNotFound
}

func (u *User) UpdateRoom(managedRoomId uint, arrangeData []ManagedRoomArrangeData) error {
	if len(u.ManagedRooms) == 0 {
		return ErrUserRoomNotFound
	}
	if !u.HasManagedRoom(managedRoomId) {
		return ErrUserRoomNotFound
	}
	for i := range arrangeData {
		if !u.HasRoomObject(arrangeData[i].ManagedRoomObjectId, arrangeData[i].RoomObjectId) {
			return ErrUserRoomObjectNotFound
		}
	}
	for i := range u.ManagedRooms {
		if u.ManagedRooms[i].ManagedRoomId == managedRoomId {
			u.ManagedRooms[i].ArrangeData = arrangeData
		}
	}
	return nil
}

func (u *User) ExtendManagedRoomObjectLimit() {
	// TODO: Move this static value to config
	u.RoomObjectLimit += 10
	u.RoomObjectLimitCount++
}

func (u *User) SetShownPlayerContentRoom(titleType value_character.TitleType) bool {
	for i := range u.OfferTitleTypes {
		if u.OfferTitleTypes[i].TitleType == titleType {
			u.OfferTitleTypes[i].SetShown()
			return true
		}
	}
	return false
}

func (u *User) IncreaseLoginDays() {
	u.LoginDays++
}

func (u *User) UpdateMoveCode(password string) (string, error) {
	jst, err := time.LoadLocation("Asia/Tokyo")
	if err != nil {
		panic(err)
	}
	hash, err := auth.GeneratePasswordHash(password)
	if err != nil {
		return "", errors.New("failed to generate password hash")
	}
	u.MoveCode = calc.GetSparkleRandomString()
	u.MovePasswordSalt = hash
	u.MoveDeadline = time.Date(2023, 8, 28, 0, 00, 00, 0, jst)
	return u.MoveCode, nil
}

func (u *User) IsMovePasswordValid(password string) bool {
	if err := auth.ValidatePassword(u.MovePasswordSalt, password); err != nil {
		return false
	}
	return true
}
