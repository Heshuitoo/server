package model_user

func NewInitialManagedRoomObjects() []ManagedRoomObject {
	initialManagedRoomArranges := GetInitialManagedRoomArranges()

	newManagedRoomObjects := make([]ManagedRoomObject, len(initialManagedRoomArranges))
	for i := range newManagedRoomObjects {
		newManagedRoomObjects[i] = ManagedRoomObject{
			RoomObjectId: initialManagedRoomArranges[i].RoomObjectId,
		}
	}
	return newManagedRoomObjects
}
