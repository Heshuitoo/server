package model_chest

type PrizeResult struct {
	ResetTarget  int64 `json:"resetTarget"`
	RewardType   int64 `json:"rewardType"`
	RewardId     int64 `json:"rewardId"`
	RewardAmount int64 `json:"rewardAmount"`
}
