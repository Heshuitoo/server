package model_login_bonus

import (
	value_login_bonus "gitlab.com/kirafan/sparkle/server/internal/domain/value/login_bonus"
)

type LoginBonusDay struct {
	BonusDayId value_login_bonus.LoginBonusDayIndex `gorm:"primaryKey"`
	// foreign key
	BonusId uint
	IconId  value_login_bonus.LoginBonusDayIconId
	// It must be 3 fields
	BonusItems []LoginBonusDayBonusItem `gorm:"foreignKey:BonusDayId"`
}
