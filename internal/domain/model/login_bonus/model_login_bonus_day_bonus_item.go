package model_login_bonus

import (
	value_login_bonus "gitlab.com/kirafan/sparkle/server/internal/domain/value/login_bonus"
)

type LoginBonusDayBonusItem struct {
	BonusDayBonusItemId uint `gorm:"primaryKey"`
	// foreign key
	BonusDayId value_login_bonus.LoginBonusDayIndex
	Type       value_login_bonus.LoginBonusDayBonusItemPresentType
	ObjId      int32
	Amount     uint32
	// messageMakeId is dead parameter
}
