package model_exp_table

type ExpTableFriendship struct {
	Level            uint32 `gorm:"primaryKey"`
	NextExp          uint32
	TotalExp         uint32
	BoostPercentHp   uint8
	BoostPercentAtk  uint8
	BoostPercentMgc  uint8
	BoostPercentDef  uint8
	BoostPercentMDef uint8
	BoostPercentSpd  uint8
	BoostPercentLuck uint8
}
